# compiler
CC = g++
#CC = icc

# HOST or MIC
PLATFORM = host
#PLATFORM = mic

#BLAS = none
#BLAS = atlas_std
BLAS = openblas
#BLAS = mkl

#LAPACKDIR = $(HOME)/lapack-3.3.1

#USE_LP = yes
USE_LP = no

ifeq ($(CC), icc)
ifeq ($(BLAS), mkl)
BASIC_OPT = -pthread -std=c++11 -Wno-deprecated -mkl
endif
else
#BASIC_OPT = -pthread -std=c++11 -Wno-deprecated
BASIC_OPT = -pthread -std=c++17
endif


ifneq ($(BLAS), none)

ifdef LAPACKDIR
OPTIONS = $(BASIC_OPT) -DCXSC_USE_BLAS -DCXSC_USE_LAPACK
else
OPTIONS = $(BASIC_OPT) -DCXSC_USE_BLAS
endif

else
OPTIONS = $(BASIC_OPT)
endif

ifeq ($(PLATFORM), host)

ifeq ($(CC), g++)
OPTIM_OPT = -O3 -march=native
else
OPTIM_OPT = -O3 -xHost -ip
endif

else
#OPTIM_OPT = -O3 -mmic -D__MIC__ -axMIC-AVX512 -ip
#OPTIM_OPT = -O3 -mmic -D__MIC__ -ip -fma -simd
OPTIM_OPT = -O3 -mmic -D__MIC__ -ip -simd
endif


#ADHC_DIR = $(HOME)/IntervalMethods/adhc/
#ADHC_DIR = $(HOME)/IntervalMethods/adhc/c++11/
#ADHC_DIR = $(HOME)/IntervalMethods/adhc/c++11/simple/
ADHC_DIR = $(HOME)/IntervalMethods/adhc/new/

#LOKI_DIR = $(HOME)/Loki/loki-0.1.7

# XSC directory
ifeq ($(PLATFORM), host)
#XSC_DIR = $(HOME)/cxsc-2_5_4
XSC_DIR = $(HOME)/survive-cxsc
else
XSC_DIR = $(HOME)/cxsc-2_5_4/mic/
endif


# header files directories
INCL_XSC = -I$(XSC_DIR)/include/


ifeq ($(BLAS), openblas)
INCL_BLAS = -I$(HOME)/OpenBLAS/include/
LIBS_BLAS = -lopenblas
LIBS_BLAS_DIR = -L$(HOME)/OpenBLAS/lib/
else ifeq ($(BLAS), mkl)
INCL_BLAS = -I/opt/intel/mkl/include/
LIBS_BLAS = -lmkl_core -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_tbb_thread -liomp5 -lm
LIBS_BLAS_DIR = -L/opt/intel/mkl/lib/intel64/
else
INCL_BLAS = 
LIBS_BLAS = -latlas -lcblas -lf77blas -latlas -lgfortran
LIBS_BLAS_DIR = -L/usr/local/atlas/lib
endif


ifeq ($(USE_LP), yes)
INCL = $(INCL_XSC) $(INCL_BLAS) -I$(HOME)/glpk/include/
else
INCL = $(INCL_XSC) $(INCL_BLAS)
endif


INCL_ADHC = -I$(ADHC_DIR)

# libraries
LIBS_XSC = -lcxsc -lm
LIBS_TBB = -ltbb -ltbbmalloc

ifeq ($(USE_LP), yes)
LIBS_GLPK = -lglpk
LIBS_GLPK_DIR = -L$(HOME)/glpk/lib
endif

ifdef LAPACKDIR
LIBS = $(LIBS_XSC) $(LIBS_TBB) $(LIBS_GLPK) $(LIBS_BLAS) -lgfortran -llapack
else
#LIBS = $(LIBS_XSC) $(LIBS_TBB) $(LIBS_GLPK) $(LIBS_BLAS) -lgfortran
LIBS = $(LIBS_XSC) $(LIBS_TBB) $(LIBS_GLPK) $(LIBS_BLAS)
endif


# libraries directory
ifdef LAPACKDIR
#LIBS_DIR_ = -L$(XSC_DIR)/lib/ $(LIBS_GLPK_DIR) $(LIBS_BLAS_DIR) -L$(HOME)/lapack-3.3.1
LIBS_DIR_ = -L$(XSC_DIR)/lib/ $(LIBS_GLPK_DIR) $(LIBS_BLAS_DIR)
else
LIBS_DIR_ = -L$(XSC_DIR)/lib/ $(LIBS_GLPK_DIR) $(LIBS_BLAS_DIR)
endif

LIBS_DIR = $(LIBS_DIR_)

# Hungarian (or another matching finding) algorithm
#MATCHING_DIR = hungarian
#MATCHING_OBJ = $(MATCHING_DIR)/matching.o $(MATCHING_DIR)/munkres.o #$(MATCHING_DIR)/matrix.o
#MATCHING_SRC = $(MATCHING_DIR)/hung.cpp $(MATCHING_DIR)/hung.hpp $(MATCHING_DIR)/munkres.cpp $(MATCHING_DIR)/matrix.cpp $(MATCHING_DIR)/munkres.h $(MATCHING_DIR)/matrix.h
#MATCHING_SRC = matching.hpp $(MATCHING_DIR)/hung.cpp $(MATCHING_DIR)/munkres.cpp $(MATCHING_DIR)/matrix.cpp $(MATCHING_DIR)/munkres.h $(MATCHING_DIR)/matrix.h


all: example broyden broy-trid almost puma rhein transistor hippopede n-R_planar n-R_planar-notrig diff_geom box3 bratu kin1 alexandre-under brent ext_freudenstein franek more-cosnard kinematics1 neural-3-layer hopfield stationary-hopf autoencoder

example:example.o solver.o lists.o LP.o lptau.o borsuk.o high_prec_procedures.o
	$(CC) $(OPTIONS) $(OPTIM_OPT) -o example example.o LP.o lptau.o solver.o lists.o borsuk.o high_prec_procedures.o $(LIBS_DIR) $(LIBS)

broyden:broyden.o solver.o lists.o LP.o lptau.o borsuk.o high_prec_procedures.o
	$(CC) $(OPTIONS) $(OPTIM_OPT) -o broyden broyden.o LP.o lptau.o solver.o lists.o borsuk.o high_prec_procedures.o $(LIBS_DIR) $(LIBS)

broy-trid:broy-trid.o solver.o lists.o LP.o lptau.o borsuk.o high_prec_procedures.o
	$(CC) $(OPTIONS) $(OPTIM_OPT) -o broy-trid broy-trid.o LP.o lptau.o solver.o lists.o borsuk.o high_prec_procedures.o $(LIBS_DIR) $(LIBS)

almost:almost.o solver.o lists.o LP.o lptau.o borsuk.o high_prec_procedures.o
	$(CC) $(OPTIONS) $(OPTIM_OPT) -o almost almost.o LP.o lptau.o solver.o lists.o borsuk.o high_prec_procedures.o $(LIBS_DIR) $(LIBS)

puma:puma.o solver.o lists.o LP.o lptau.o borsuk.o high_prec_procedures.o
	$(CC) $(OPTIONS) $(OPTIM_OPT) -o puma puma.o LP.o lptau.o solver.o lists.o borsuk.o high_prec_procedures.o $(LIBS_DIR) $(LIBS)

rhein:rhein.o solver.o lists.o LP.o lptau.o borsuk.o high_prec_procedures.o
	$(CC) $(OPTIONS) $(OPTIM_OPT) -o rhein rhein.o LP.o lptau.o solver.o lists.o borsuk.o high_prec_procedures.o $(LIBS_DIR) $(LIBS)

transistor:transistor.o solver.o lists.o LP.o lptau.o borsuk.o high_prec_procedures.o
	$(CC) $(OPTIONS) $(OPTIM_OPT) -o transistor transistor.o LP.o lptau.o solver.o lists.o borsuk.o high_prec_procedures.o $(LIBS_DIR) $(LIBS)

hippopede:hippopede.o solver.o lists.o LP.o lptau.o borsuk.o high_prec_procedures.o
	$(CC) $(OPTIONS) $(OPTIM_OPT) -o hippopede hippopede.o LP.o lptau.o solver.o lists.o borsuk.o high_prec_procedures.o $(LIBS_DIR) $(LIBS)

#manif:manif.o solver.o lists.o my_hess_ari.o LP.o lptau.o
#	$(CC) $(OPTIONS) $(OPTIM_OPT) -o manif manif.o my_hess_ari.o LP.o lptau.o solver.o lists.o $(LIBS_DIR) $(LIBS)

n-R_planar: n-R_planar.o solver.o lists.o LP.o lptau.o borsuk.o high_prec_procedures.o
	$(CC) $(OPTIONS) $(OPTIM_OPT) -o n-R_planar n-R_planar.o LP.o lptau.o solver.o lists.o borsuk.o high_prec_procedures.o $(LIBS_DIR) $(LIBS)

n-R_planar-notrig: n-R_planar-notrig.o solver.o lists.o LP.o lptau.o borsuk.o high_prec_procedures.o
	$(CC) $(OPTIONS) $(OPTIM_OPT) -o n-R_planar-notrig n-R_planar-notrig.o LP.o lptau.o solver.o lists.o borsuk.o high_prec_procedures.o $(LIBS_DIR) $(LIBS)

diff_geom:diff_geom.o solver.o lists.o LP.o lptau.o borsuk.o high_prec_procedures.o
	$(CC) $(OPTIONS) $(OPTIM_OPT) -o diff_geom diff_geom.o LP.o lptau.o solver.o lists.o borsuk.o high_prec_procedures.o $(LIBS_DIR) $(LIBS)

#example-ZLS:example-ZLS.o solver.o lists.o my_hess_ari.o LP.o lptau.o
#	$(CC) $(OPTIONS) $(OPTIM_OPT) -o example-ZLS example-ZLS.o my_hess_ari.o LP.o lptau.o solver.o lists.o $(LIBS_DIR) $(LIBS)

box3:box3.o solver.o lists.o LP.o lptau.o borsuk.o high_prec_procedures.o
	$(CC) $(OPTIONS) $(OPTIM_OPT) -o box3 box3.o LP.o lptau.o solver.o lists.o borsuk.o high_prec_procedures.o $(LIBS_DIR) $(LIBS)

bratu:bratu.o solver.o lists.o LP.o lptau.o borsuk.o high_prec_procedures.o
	$(CC) $(OPTIONS) $(OPTIM_OPT) -o bratu bratu.o LP.o lptau.o solver.o lists.o borsuk.o high_prec_procedures.o $(LIBS_DIR) $(LIBS)

kin1:kin1.o solver.o lists.o LP.o lptau.o borsuk.o high_prec_procedures.o
	$(CC) $(OPTIONS) $(OPTIM_OPT) -o kin1 kin1.o LP.o lptau.o solver.o lists.o borsuk.o high_prec_procedures.o $(LIBS_DIR) $(LIBS)

alexandre-under:alexandre-under.o solver.o lists.o LP.o lptau.o borsuk.o high_prec_procedures.o
	$(CC) $(OPTIONS) $(OPTIM_OPT) -o alexandre-under alexandre-under.o LP.o lptau.o solver.o lists.o borsuk.o high_prec_procedures.o $(LIBS_DIR) $(LIBS)

brent:brent.o solver.o lists.o LP.o lptau.o borsuk.o high_prec_procedures.o
	$(CC) $(OPTIONS) $(OPTIM_OPT) -o brent brent.o LP.o lptau.o solver.o lists.o borsuk.o high_prec_procedures.o $(LIBS_DIR) $(LIBS)

ext_freudenstein:ext_freudenstein.o solver.o lists.o LP.o lptau.o borsuk.o high_prec_procedures.o
	$(CC) $(OPTIONS) $(OPTIM_OPT) -o ext_freudenstein ext_freudenstein.o LP.o lptau.o solver.o lists.o borsuk.o high_prec_procedures.o $(LIBS_DIR) $(LIBS)

#varshovi:varshovi.o solver.o lists.o my_hess_ari.o LP.o lptau.o
#	$(CC) $(OPTIONS) $(OPTIM_OPT) -o varshovi varshovi.o my_hess_ari.o LP.o lptau.o solver.o lists.o $(LIBS_DIR) $(LIBS)

franek:franek.o solver.o lists.o LP.o lptau.o borsuk.o high_prec_procedures.o
	$(CC) $(OPTIONS) $(OPTIM_OPT) -o franek franek.o LP.o lptau.o solver.o lists.o borsuk.o high_prec_procedures.o $(LIBS_DIR) $(LIBS)

more-cosnard:more-cosnard.o solver.o lists.o LP.o lptau.o borsuk.o high_prec_procedures.o
	$(CC) $(OPTIONS) $(OPTIM_OPT) -o more-cosnard more-cosnard.o LP.o lptau.o solver.o lists.o borsuk.o high_prec_procedures.o $(LIBS_DIR) $(LIBS)

kinematics1:kinematics1.o solver.o lists.o LP.o lptau.o borsuk.o high_prec_procedures.o
	$(CC) $(OPTIONS) $(OPTIM_OPT) -o kinematics1 kinematics1.o LP.o lptau.o solver.o lists.o borsuk.o high_prec_procedures.o $(LIBS_DIR) $(LIBS)

neural-3-layer:neural-3-layer.o solver.o lists.o LP.o lptau.o borsuk.o high_prec_procedures.o
	$(CC) $(OPTIONS) $(OPTIM_OPT) -o neural-3-layer neural-3-layer.o LP.o lptau.o solver.o lists.o borsuk.o high_prec_procedures.o $(LIBS_DIR) $(LIBS)

hopfield:hopfield.o solver.o lists.o LP.o lptau.o borsuk.o high_prec_procedures.o
	$(CC) $(OPTIONS) $(OPTIM_OPT) -o hopfield hopfield.o LP.o lptau.o solver.o lists.o borsuk.o high_prec_procedures.o $(LIBS_DIR) $(LIBS)
	#$(CC) $(OPTIONS) $(OPTIM_OPT) -o hopfield hopfield.o LP.o lptau.o solver.o lists.o $(LIBS_DIR) $(LIBS)

stationary-hopf:stationary-hopf.o solver.o lists.o LP.o lptau.o borsuk.o high_prec_procedures.o
	$(CC) $(OPTIONS) $(OPTIM_OPT) -o stationary-hopf stationary-hopf.o LP.o lptau.o solver.o lists.o borsuk.o high_prec_procedures.o $(LIBS_DIR) $(LIBS)

autoencoder:autoencoder.o solver.o lists.o LP.o lptau.o borsuk.o high_prec_procedures.o
	$(CC) $(OPTIONS) $(OPTIM_OPT) -o autoencoder autoencoder.o LP.o lptau.o solver.o lists.o borsuk.o high_prec_procedures.o $(LIBS_DIR) $(LIBS)

#my_hess_ari.o:my_hess_ari.cpp my_hess_ari.hpp
#	$(CC) $(OPTIONS) $(OPTIM_OPT) -c my_hess_ari.cpp $(INCL)

LP.o:LP.cpp LP.hpp 
	$(CC) $(OPTIONS) $(OPTIM_OPT) -c LP.cpp $(INCL)

lptau.o:lptau.cc
	$(CC) $(OPTIONS) $(OPTIM_OPT) -c lptau.cc

lists.o:lists.cpp lists.hpp
	$(CC) $(OPTIONS) $(OPTIM_OPT) -c lists.cpp $(INCL)

borsuk.o:borsuk.cpp borsuk.hpp problem_desc.hpp
ifeq ($(BLAS), mkl)
	$(CC) $(OPTIONS) $(OPTIM_OPT) -DMKL -c borsuk.cpp $(INCL)
else
	$(CC) $(OPTIONS) $(OPTIM_OPT) -c borsuk.cpp $(INCL)
endif

high_prec_procedures.o:high_prec_procedures.cpp high_prec_procedures.hpp
	$(CC) $(OPTIONS) $(OPTIM_OPT) -c high_prec_procedures.cpp $(INCL) $(INCL_ADHC)

#problem_desc_impl.o:problem_desc_impl.cpp problem_desc_impl.hpp problem_desc.hpp
#	$(CC) $(OPTIONS) $(OPTIM_OPT) -c problem_desc_impl.cpp $(INCL)

solver.o:solver.cpp solver.hpp problem_desc.hpp statistics.hpp threaded_aux.hpp bisection.hpp lists.hpp LP.hpp tolerable.hpp bc.hpp bound-consistency.hpp hc.hpp quadratic_eq.hpp hansen_quadratic_eq.hpp borsuk.hpp top_deg.hpp box_graph.hpp
	$(CC) $(OPTIONS) $(OPTIM_OPT) -c solver.cpp $(INCL) $(INCL_ADHC)

example.o:example.cpp solver.hpp problem_desc_impl.hpp problem_desc.hpp lists.hpp num_threads.hpp
	$(CC) $(OPTIONS) $(OPTIM_OPT) -c example.cpp $(INCL) $(INCL_ADHC) 

broyden.o:broyden.cpp solver.hpp problem_desc_impl.hpp problem_desc.hpp lists.hpp num_threads.hpp
	$(CC) $(OPTIONS) $(OPTIM_OPT) -c broyden.cpp $(INCL) $(INCL_ADHC)

broy-trid.o:broy-trid.cpp solver.hpp problem_desc_impl.hpp problem_desc.hpp lists.hpp num_threads.hpp
	$(CC) $(OPTIONS) $(OPTIM_OPT) -c broy-trid.cpp $(INCL) $(INCL_ADHC)

almost.o:almost.cpp solver.hpp problem_desc_impl.hpp problem_desc.hpp lists.hpp num_threads.hpp
	$(CC) $(OPTIONS) $(OPTIM_OPT) -c almost.cpp $(INCL) $(INCL_ADHC)

puma.o:puma.cpp solver.hpp problem_desc_impl.hpp problem_desc.hpp lists.hpp num_threads.hpp
	$(CC) $(OPTIONS) $(OPTIM_OPT) -c puma.cpp $(INCL) $(INCL_ADHC)

rhein.o:rhein.cpp solver.hpp problem_desc_impl.hpp problem_desc.hpp lists.hpp num_threads.hpp
	$(CC) $(OPTIONS) $(OPTIM_OPT) -c rhein.cpp $(INCL) $(INCL_ADHC)

transistor.o:transistor.cpp solver.hpp problem_desc_impl.hpp problem_desc.hpp lists.hpp num_threads.hpp
	$(CC) $(OPTIONS) $(OPTIM_OPT) -c transistor.cpp $(INCL) $(INCL_ADHC)

hippopede.o:hippopede.cpp solver.hpp problem_desc_impl.hpp problem_desc.hpp lists.hpp num_threads.hpp
	$(CC) $(OPTIONS) $(OPTIM_OPT) -c hippopede.cpp $(INCL) $(INCL_ADHC)

#manif.o:manif.cpp solver.hpp problem_desc_impl.hpp problem_desc.hpp lists.hpp my_hess_ari.hpp num_threads.hpp
#	$(CC) $(OPTIONS) $(OPTIM_OPT) -c manif.cpp $(INCL) $(INCL_ADHC)

n-R_planar.o:n-R_planar.cpp solver.hpp problem_desc_impl.hpp problem_desc.hpp lists.hpp num_threads.hpp
	$(CC) $(OPTIONS) $(OPTIM_OPT) -c n-R_planar.cpp $(INCL) $(INCL_ADHC)

n-R_planar-notrig.o:n-R_planar-notrig.cpp solver.hpp problem_desc_impl.hpp problem_desc.hpp lists.hpp num_threads.hpp
	$(CC) $(OPTIONS) $(OPTIM_OPT) -c n-R_planar-notrig.cpp $(INCL) $(INCL_ADHC)

diff_geom.o:diff_geom.cpp solver.hpp problem_desc_impl.hpp problem_desc.hpp lists.hpp num_threads.hpp
	$(CC) $(OPTIONS) $(OPTIM_OPT) -c diff_geom.cpp $(INCL) $(INCL_ADHC)

#example-ZLS.o:example-ZLS.cpp solver.hpp problem_desc_impl.hpp problem_desc.hpp lists.hpp my_hess_ari.hpp num_threads.hpp
#	$(CC) $(OPTIONS) $(OPTIM_OPT) -c example-ZLS.cpp $(INCL) $(INCL_ADHC)

box3.o:box3.cpp solver.hpp problem_desc_impl.hpp problem_desc.hpp lists.hpp num_threads.hpp
	$(CC) $(OPTIONS) $(OPTIM_OPT) -c box3.cpp $(INCL) $(INCL_ADHC)

bratu.o:bratu.cpp solver.hpp problem_desc_impl.hpp problem_desc.hpp lists.hpp num_threads.hpp
	$(CC) $(OPTIONS) $(OPTIM_OPT) -c bratu.cpp $(INCL) $(INCL_ADHC)

kin1.o:kin1.cpp solver.hpp problem_desc_impl.hpp problem_desc.hpp lists.hpp num_threads.hpp
	$(CC) $(OPTIONS) $(OPTIM_OPT) -c kin1.cpp $(INCL) $(INCL_ADHC)

alexandre-under.o:alexandre-under.cpp solver.hpp problem_desc_impl.hpp problem_desc.hpp lists.hpp num_threads.hpp
	$(CC) $(OPTIONS) $(OPTIM_OPT) -c alexandre-under.cpp $(INCL) $(INCL_ADHC)

brent.o:brent.cpp solver.hpp problem_desc_impl.hpp problem_desc.hpp lists.hpp num_threads.hpp
	$(CC) $(OPTIONS) $(OPTIM_OPT) -c brent.cpp $(INCL) $(INCL_ADHC)

ext_freudenstein.o:ext_freudenstein.cpp solver.hpp problem_desc_impl.hpp problem_desc.hpp lists.hpp num_threads.hpp
	$(CC) $(OPTIONS) $(OPTIM_OPT) -c ext_freudenstein.cpp $(INCL) $(INCL_ADHC)

#varshovi.o:varshovi.cpp solver.hpp problem_desc_impl.hpp problem_desc.hpp lists.hpp my_hess_ari.hpp num_threads.hpp
#	$(CC) $(OPTIONS) $(OPTIM_OPT) -c varshovi.cpp $(INCL) $(INCL_ADHC)

franek.o:franek.cpp solver.hpp problem_desc_impl.hpp problem_desc.hpp lists.hpp num_threads.hpp
	$(CC) $(OPTIONS) $(OPTIM_OPT) -c franek.cpp $(INCL) $(INCL_ADHC)

more-cosnard.o:more-cosnard.cpp solver.hpp problem_desc_impl.hpp problem_desc.hpp lists.hpp num_threads.hpp
	$(CC) $(OPTIONS) $(OPTIM_OPT) -c more-cosnard.cpp $(INCL) $(INCL_ADHC)

kinematics1.o:kinematics1.cpp solver.hpp problem_desc_impl.hpp problem_desc.hpp lists.hpp num_threads.hpp
	$(CC) $(OPTIONS) $(OPTIM_OPT) -c kinematics1.cpp $(INCL) $(INCL_ADHC)

neural-3-layer.o: neural-3-layer.cpp solver.hpp problem_desc_impl.hpp problem_desc.hpp lists.hpp num_threads.hpp
	$(CC) $(OPTIONS) $(OPTIM_OPT) -c neural-3-layer.cpp $(INCL) $(INCL_ADHC)

hopfield.o: hopfield.cpp solver.hpp problem_desc_impl.hpp problem_desc.hpp lists.hpp num_threads.hpp
	$(CC) $(OPTIONS) $(OPTIM_OPT) -c hopfield.cpp $(INCL) $(INCL_ADHC)

stationary-hopf.o: stationary-hopf.cpp solver.hpp problem_desc_impl.hpp problem_desc.hpp lists.hpp num_threads.hpp
	$(CC) $(OPTIONS) $(OPTIM_OPT) -c stationary-hopf.cpp $(INCL) $(INCL_ADHC)

autoencoder.o: autoencoder.cpp solver.hpp problem_desc_impl.hpp problem_desc.hpp lists.hpp num_threads.hpp
	$(CC) $(OPTIONS) $(OPTIM_OPT) -c autoencoder.cpp $(INCL) $(INCL_ADHC)


.PHONY: clean clean_ex rebuild all
clean:
	rm example example.o broyden broyden.o broy-trid broy-trid.o almost almost.o puma puma.o rhein rhein.o hippopede hippopede.o n-R_planar n-R_planar.o n-R_planar-notrig n-R_planar-notrig.o transistor transistor.o diff_geom diff_geom.o box3 box3.o bratu bratu.o kin1 kin1.o alexandre-under alexandre-under.o brent brent.o ext_freudenstein ext_freudenstein.o franek franek.o more-cosnard more-cosnard.o kinematics1 kinematics1.o neural-3-layer neural-3-layer.o hopfield hopfield.o stationary-hopf stationary-hopf.o autoencoder.o LP.o solver.o lists.o lptau.o borsuk.o high_prec_procedures.o

clean_ex:
	rm example.o broyden.o broy-trid.o almost.o puma.o rhein.o hippopede.o n-R_planar.o n-R_planar-notrig.o transistor.o diff_geom.o box3.o bratu.o kin1.o alexandre-under.o brent.o ext_freudenstein.o franek.o more-cosnard.o kinematics1.o neural-3-layer.o hopfield.o stationary-hopf.o autoencoder.o

rebuild: clean all

