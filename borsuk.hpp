#if !defined __BORSUK_HPP__
#define __BORSUK_HPP__

#include "problem_desc.hpp"

#include <imatrix.hpp>

int compute_precond_for_borsuk (const cxsc::ivector &y1, const cxsc::ivector &y2, cxsc::imatrix &Y);

int check_borsuk (const problem_desc &p, const cxsc::ivector &x);

int check_borsuk_parallel (const problem_desc &p, const cxsc::ivector &x);

void narrowly_compute_all_f (const problem_desc &p, const cxsc::ivector &x, cxsc::ivector &y);

/*int check_borsuk_subdiv (const problem_desc &p, const cxsc::ivector &x, const cxsc::rmatrix &Y, const intvector &var_num) {
	//cout << "var_num = " << var_num << "\n";
	std::vector< std::pair<cxsc::ivector, cxsc::ivector> > subfacets;
	subfacets.clear();
	for (int it = 1; it <= p.num_fun; ++it) {
		const int i = var_num[it];
		cxsc::ivector x1{x};
		SetInf(x1[i], Sup(x1[i]));
		cxsc::ivector x2{x};
		SetSup(x2[i], Inf(x2[i]));
		//cout << "for i = " << i << " : " << check_borsuk_for_i(p, x1, x2) << "\n";
		if (!check_borsuk_for_i(p, x1, x2, Y)) return 0;
	}
	return 1;
}*/

#endif

