#if !defined __BOX_GRAPH_HPP__
#define __BOX_GRAPH_HPP__

#include <ivector.hpp>

#include <tbb/spin_mutex.h>
#include <tbb/parallel_invoke.h>
#include <tbb/parallel_for.h>
//#include <tbb/parallel_for_each.h>
#include <tbb/blocked_range.h>
#include <tbb/concurrent_vector.h>
#include <tbb/enumerable_thread_specific.h>

#include <vector>
#include <iomanip>
#include <atomic>

#include "borsuk.hpp"

struct box_node;

struct box_graph;


using vert_set = tbb::concurrent_vector<box_node>;


enum class AlgVersion {find_cycle, compute_top_deg, compute_top_deg_underdetermined};

struct box_node {
	cxsc::ivector x;
	int orientation;
	cxsc::intvector sv;
	std::vector<vert_set::iterator> neighb;
	tbb::spin_mutex mut;
	tbb::enumerable_thread_specific<bool> marked;

	box_node(const cxsc::ivector &x_, const int orient_, const cxsc::intvector &sv_) : x(x_), orientation(orient_), sv(sv_) {
		neighb.clear();
	};

	box_node(const box_node &b) : x(b.x), orientation(b.orientation), sv(b.sv), neighb(b.neighb) /* mutex is NOT copied!!! */ {};

	box_node(box_node &&) = default;

	box_node & operator = (box_node &&b) {return b;}; 
};


int intersect_d_boxes (cxsc::ivector &x, const cxsc::ivector &y) {
	int count{0};
	for (int i = 1; i <= VecLen(x); ++i) {
		if (Disjoint(x[i], y[i])) return -1;
		x[i] &= y[i];
		if (Inf(x[i]) < Sup(x[i])) ++count;
	}
	return count;
}


int num_of_nondeg_components (const cxsc::ivector &x, const int limit) {
	int count = 0;
	for (int i = 1; i <= limit; ++i) count += (Sup(x[i]) > Inf(x[i]))?1:0;
	return count;
}


cxsc::intvector numbers_of_nondeg_components (const cxsc::ivector &x) {
	int count = 0;
	cxsc::intvector result(VecLen(x));
	for (int limit = 1; limit <= VecLen(x); ++limit) {
		//for (int i = 1; i <= limit; ++i) count += (Sup(x[i]) > Inf(x[i]))?1:0;
		count += (Sup(x[limit]) > Inf(x[limit]))?1:0;
		result[limit] = count;
	}
	return result;
}


cxsc::intvector numbers_of_nondeg_components (const cxsc::ivector &x, const cxsc::intvector &which) {
	int count = 0;
	cxsc::intvector result(VecLen(x));
	for (int limit = 1; limit <= VecLen(x); ++limit) {
		if (which[limit]) continue;
		for (int i = 1; i <= limit; ++i) count += (!which[i] && Sup(x[i]) > Inf(x[i]))?1:0;
		result[limit] = count;
	}
	return result;
}


struct box_graph {
	vert_set V;

	int create_edges() {
		if (V.empty()) return -1;
		tbb::parallel_for(0, static_cast<int>(V.size()), 1, 
		//tbb::parallel_for_each(V.begin(), V.end(), 
			//[&](box_node &b) {
			[&](int i) {
				vert_set::iterator current = V.begin() + i;
				//tbb::spin_mutex::scoped_lock lock1(current->mut);
				scoped_lock_of<tbb::spin_mutex> lock1{current->mut};
				for (vert_set::iterator it = current + 1; it != V.end(); ++it) {
					//tbb::spin_mutex::scoped_lock lock2(it->mut);
					scoped_lock_of<tbb::spin_mutex> lock2{it->mut};
					if (!Disjoint(current->x, it->x)) {
						current->neighb.push_back(it);
						it->neighb.push_back(current);
					}
				}
			}
		);
		return 0;
	};

	bool create_sufficient_subboxes (const cxsc::ivector &x, const int orientation, const problem_desc &p, const real &eps) {
		//tbb::spin_mutex::scoped_lock lock(it->mut);
		cxsc::ivector y(p.num_fun);
		narrowly_compute_all_f(p, x, y);
		bool sufficient{false};
		cxsc::intvector sv(p.num_fun);
		for (int i = 1; i <= p.num_fun; ++i) {
			if (Inf(y[i]) > 0.0) {
				sv[i] = 1;
				sufficient = true;
			}
			else if (Sup(y[i]) < 0.0) {
				sv[i] = -1;
				sufficient = true;
			}
			else {
				sv[i] = 0;
			}
		}
		if (sufficient) {
			V.push_back(box_node(x, orientation, sv));
			return true;
		}
		int k;
		if (norm(x, k) <= eps) {
			//V.push_back(box_node(x, orientation, sv));
			//std::cout << "not sufficient: x = " << x << "\nsv = " << sv << "\n";
			//std::cout << "not sufficient\n";
			return false;
		}
		cxsc::real c = (Inf(x[k]) + Sup(x[k]))*0.5;
		cxsc::ivector x1{x};
		SetSup(x1[k], c);
		bool result = create_sufficient_subboxes(x1, orientation, p, eps);
		if (!result) return false;
		x1[k] = cxsc::interval(c, Sup(x[k]));
		result = create_sufficient_subboxes(x1, orientation, p, eps);
		return result;
	};

	bool create_sufficient_subboxes (const cxsc::ivector &x, const cxsc::intvector &which,
					 const int orientation, const problem_desc &p, const real &eps) {
		cxsc::ivector y(p.num_fun);
		narrowly_compute_all_f(p, x, y);
		bool sufficient{false};
		cxsc::intvector sv(p.num_fun);
		for (int i = 1; i <= p.num_fun; ++i) {
			if (Inf(y[i]) > 0.0) {
				sv[i] = 1;
				sufficient = true;
			}
			else if (Sup(y[i]) < 0.0) {
				sv[i] = -1;
				sufficient = true;
			}
			else {
				sv[i] = 0;
			}
		}
		if (sufficient) {
			V.push_back(box_node(x, orientation, sv));
			return true;
		}
		int k;
		if (norm_subvec(x, which, k) <= eps) {
			return false;
		}
		cxsc::real c = (Inf(x[k]) + Sup(x[k]))*0.5;
		cxsc::ivector x1{x};
		SetSup(x1[k], c);
		bool result = create_sufficient_subboxes(x1, which, orientation, p, eps);
		if (!result) return false;
		x1[k] = cxsc::interval(c, Sup(x[k]));
		result = create_sufficient_subboxes(x1, which, orientation, p, eps);
		return result;
	};

	bool create_subboxes_sufficient_for_2D_cycle (const cxsc::ivector &x, const problem_desc &p, const real &eps) {
		cxsc::ivector y(p.num_fun);
		narrowly_compute_all_f(p, x, y);
		bool one_sufficient{false}, all_sufficient{true};
		cxsc::intvector sv(p.num_fun);
		for (int i = 1; i <= p.num_fun; ++i) {
			if (Inf(y[i]) > 0.0) {
				sv[i] = 1;
				one_sufficient = true;
			}
			else if (Sup(y[i]) < 0.0) {
				sv[i] = -1;
				one_sufficient = true;
			}
			else {
				sv[i] = 0;
				all_sufficient = false;
			}
		}
		if (all_sufficient) {
			V.push_back(box_node(x, 1, sv));
			return true;
		}
		int k;
		if (norm(x, k) <= eps) {
			if (one_sufficient) V.push_back(box_node(x, 1, sv));
			return false;
		}
		cxsc::real c = (Inf(x[k]) + Sup(x[k]))*0.5;
		cxsc::ivector x1{x};
		SetSup(x1[k], c);
		bool result1 = create_subboxes_sufficient_for_2D_cycle(x1, p, eps);
		x1[k] = cxsc::interval(c, Sup(x[k]));
		bool result2 = create_subboxes_sufficient_for_2D_cycle(x1, p, eps);
		return result1 & result2;
	};

	template<AlgVersion ver>
	bool create_from_boundary (const cxsc::ivector &x, const cxsc::intvector &which,
				   const int orientation, const problem_desc &p, const real &eps) {
		std::atomic<bool> result;
		result = true;
		cxsc::intvector nondeg_components;
		if (ver == AlgVersion::compute_top_deg_underdetermined) {
			nondeg_components = numbers_of_nondeg_components(x, which);
		}
		else {
			nondeg_components = numbers_of_nondeg_components(x);
		}
		//std::cout << "numbers_of_nondeg: " << nondeg_components << "\n";
		tbb::parallel_for(1, VecLen(x) + 1, 1,
			[&, orientation](int i) {
				if (ver == AlgVersion::compute_top_deg_underdetermined) {
					if (which[i]) return;
				}
				//tbb::parallel_invoke([=, &x, &p, &eps, &result] {
				//tbb::parallel_invoke([&x, &p, &eps, &result, this, orientation, i] {
				//tbb::parallel_invoke([&x, &p, &eps, &result, =] {
				tbb::parallel_invoke([&, orientation, i] {
				//tbb::parallel_invoke([&] {
					cxsc::ivector x_face{x};
					SetSup(x_face[i], Inf(x_face[i]));
					bool res;
					int my_orientation{orientation};
					if (nondeg_components[i] % 2 != 0) my_orientation = -my_orientation;
					//std::cout << "create x_face: " << x_face << ", orient = " << my_orientation << "\n";
					if (ver == AlgVersion::compute_top_deg) {
						res = create_sufficient_subboxes (x_face, my_orientation, p, eps);
					}
					else if (ver == AlgVersion::compute_top_deg_underdetermined) {
						res = create_sufficient_subboxes (x_face, which, my_orientation, p, eps);
					}
					else if (ver == AlgVersion::find_cycle) {
						res = create_subboxes_sufficient_for_2D_cycle (x_face, /*my_orientation,*/ p, eps);
					}
					else {
						std::terminate();
					}
					if (!res) result = false;
				},
				[&, orientation, i] {
				//[&] {
					cxsc::ivector x_face{x};
					SetInf(x_face[i], Sup(x[i]));
					bool res;
					int my_orientation{orientation};
					if (nondeg_components[i] % 2 == 0) my_orientation = -my_orientation;
					//std::cout << "create x_face: " << x_face << ", orient = " << my_orientation << "\n";
					if (ver == AlgVersion::compute_top_deg) {
						res = create_sufficient_subboxes (x_face, my_orientation, p, eps);
					}
					else if (ver == AlgVersion::compute_top_deg_underdetermined) {
						res = create_sufficient_subboxes (x_face, which, my_orientation, p, eps);
					}
					else if (ver == AlgVersion::find_cycle) {
						res = create_subboxes_sufficient_for_2D_cycle (x_face, /*my_orientation,*/ p, eps);
					}
					else {
						std::terminate();
					}
					if (!res) result = false;
				}
				);
			}
		);
		if (ver == AlgVersion::find_cycle) {
			create_edges();
			return result;
		}
		else {
			if (!result) return false;
			create_edges();
			return result;
		}
		//----if (!result) return false;
		//create_edges();
		//return result;
		//----return true;
	};

	void create_faces_list (const vert_set::iterator it, const int l, const cxsc::intvector &which) {
		const int d = VecLen(it->sv) - 1;
		cxsc::intvector nondeg_components = numbers_of_nondeg_components(it->x, which);
		tbb::parallel_for(1, VecLen(it->x) + 1, 1,
			[&, d, l](int i) {
				if (which[i]) return;
				/*{
					std::stringstream ss;
				}*/
				tbb::parallel_invoke([&, i, l] {
					if (Inf(it->x[i]) >= Sup(it->x[i])) return;
					cxsc::ivector x_face{it->x};
					SetSup(x_face[i], Inf(x_face[i]));
					int my_orientation{it->orientation};
					if (nondeg_components[i] % 2 != 0) my_orientation = -my_orientation;
					cxsc::ivector cur_x_face;
					for (vert_set::iterator neighb_it : it->neighb) {
						if (neighb_it->sv[l] == it->sv[l]) continue;
						cur_x_face = x_face;
						if (intersect_d_boxes (cur_x_face, neighb_it->x) < d - 1) continue;
						intvector sv(d);
						//if (neighb_it->sv[l] == -it->sv[l]) std::cout << "\tCos nie tak !\n";
						//std::cout << "\tneighb_it->sv = " << neighb_it->sv << "\n";
						for (int ii = 1; ii < l; ++ii) {
							sv[ii] = neighb_it->sv[ii];
							if (sv[ii] == 0) sv[ii] = it->sv[ii];
						}
						//sv[l] = neighb_it->sv[d];
						//for (int ii = l + 1; ii < d; ++ii) sv[ii] = neighb_it->sv[ii];
						for (int ii = l + 1, ii2 = l; ii <= d + 1; ++ii, ++ii2) {
							sv[ii2] = neighb_it->sv[ii];
							if (sv[ii2] == 0) sv[ii2] = it->sv[ii];
						}
						//std::cout << "\tsv = " << sv << "\n";
						V.push_back(box_node(cur_x_face, my_orientation, sv));
					}
				},
				[&, i, l] {
					if (Inf(it->x[i]) >= Sup(it->x[i])) return;
					cxsc::ivector x_face{it->x};
					SetInf(x_face[i], Sup(x_face[i]));
					int my_orientation{it->orientation};
					if (nondeg_components[i] % 2 == 0) my_orientation = -my_orientation;
					cxsc::ivector cur_x_face;
					for (vert_set::iterator neighb_it : it->neighb) {
						if (neighb_it->sv[l] == it->sv[l]) continue;
						cur_x_face = x_face;
						if (intersect_d_boxes (cur_x_face, neighb_it->x) < d - 1) continue;
						intvector sv(d);
						//if (neighb_it->sv[l] == -it->sv[l]) std::cout << "\tCos nie tak !\n";
						//std::cout << "\tneighb_it->sv = " << neighb_it->sv << "\n";
						for (int ii = 1; ii < l; ++ii) {
							sv[ii] = neighb_it->sv[ii];
							if (sv[ii] == 0) sv[ii] = it->sv[ii];
						}
						//sv[l] = neighb_it->sv[d];
						//for (int ii = l + 1; ii < d; ++ii) sv[ii] = neighb_it->sv[ii];
						for (int ii = l + 1, ii2 = l; ii <= d + 1; ++ii, ++ii2) {
							sv[ii2] = neighb_it->sv[ii];
							if (sv[ii2] == 0) sv[ii2] = it->sv[ii];
						}
						V.push_back(box_node(cur_x_face, my_orientation, sv));
					}
				}
				);
			}
		);
	};

	int merge_neighbors_with_equal_sv(vert_set::iterator &it) {
	//int merge_neighbors_with_equal_sv(box_node &b) {
		int count = 0;
		//std::cout << "merge for box with sv = " << it->sv << ", neighbors: " << it->neighb.size() << "\n";
		std::vector<vert_set::iterator> new_neighbors;
		new_neighbors.clear();
		for (vert_set::iterator & neighb_it : it->neighb) {
			//std::cout << "\tneighbor with sv = " << neighb_it->sv << "\n";
			if (neighb_it->sv == it->sv) {
				++count;
				it->x |= neighb_it->x;
				for (vert_set::iterator & iter : neighb_it->neighb) {
					if (iter != it) {
						if (iter == neighb_it) std::cout << "oops!\n";
						new_neighbors.push_back(iter);
						std::replace(iter->neighb.begin(), iter->neighb.end(), neighb_it, it);
					}
					//else std::cout << "\t\tit\n";
				}
				//std::cout << "neighb_it->sv = " << neighb_it->sv << "\n";
				neighb_it->neighb.clear();
			}
		}
		for (auto iter = new_neighbors.begin(); iter != new_neighbors.end(); ++iter) {
			it->neighb.push_back(*iter);
		}
		auto iter = std::remove_if(it->neighb.begin(),
					   it->neighb.end(),
					   [](const vert_set::iterator my_it){return my_it->neighb.empty();});
		it->neighb.erase(iter, it->neighb.end());
		std::sort(it->neighb.begin(), it->neighb.end());
		it->neighb.erase(std::unique(it->neighb.begin(), it->neighb.end()), it->neighb.end());
		return count;
	};

	void merge_nodes_with_equal_sv() {
		for (vert_set::iterator it = V.begin(); it != V.end(); ++it) {
		//for (box_node &b : V) {
			//std::cout << "considering it->sv = " << it->sv << "\n";
			int result;
			do {
				result = merge_neighbors_with_equal_sv(it);
				//std::cout << "result = " << result << "\n";
			} while (result > 0);
		}
	};

	void compute_lists_of_selected_boxes(std::vector< std::vector<vert_set::iterator> > &lists) {
		// for all  i = 1, ..., n and s in {-1, +1}
		const int n = VecLen(V[0].sv);
		lists.clear();
		lists.resize(n*2);
		for (int i1 = 0; i1 < n*2; ++i1) lists[i1].clear();
		//for (const box_node &b : V)
		for (int ind = 0; ind < V.size(); ++ind) {
			tbb::concurrent_vector<box_node>::iterator it = V.begin() + ind;
			for (int i = 1; i <= n; ++i) {
				const int i1 = i - 1;
				if (it->sv[i] == 1) lists[i1].push_back(it);
				else if (it->sv[i] == -1) lists[n + i1].push_back(it);
			}
		}
		//for (int i1 = 0; i1 < n*2; ++i1) std::cout << "list size: " << lists[i1].size() << "\n";
	};

	int find_cycle_with_nonzero_degree(const vert_set::iterator it) {
		//std::cout << "find_cycle, x = " << it->x << "\n";
		const int n = VecLen(it->sv);
		if (n != 2) return -1;
		for (box_node &b : V) b.marked.local() = false;
		return find_cycle_with_nonzero_degree_recur(it, it);
	};

	bool is_next_element_of_cycle (const vert_set::iterator current, const vert_set::iterator next) {
		if (current->sv[1] == 1 && next->sv[2] == -1) return true;
		if (current->sv[2] == -1 && next->sv[1] == -1) return true;
		if (current->sv[1] == -1 && next->sv[2] == 1) return true;
		if (current->sv[2] == 1 && next->sv[1] == 1) return true;
		return false;
	};

	bool is_the_same_element_of_cycle (const vert_set::iterator current, const vert_set::iterator next) {
		if (current->sv[1] == 1 && next->sv[1] == 1) return true;
		if (current->sv[2] == -1 && next->sv[2] == -1) return true;
		if (current->sv[1] == -1 && next->sv[1] == -1) return true;
		if (current->sv[1] == 1 && next->sv[1] == 1) return true;
		return false;
	};

	int find_cycle_with_nonzero_degree_recur(const vert_set::iterator it, const vert_set::iterator first) {
		//std::cout << "find_cycle_with_nonzero_degree_recur, x = " << it->x << ", sv = " << it->sv << "\n";
		//std::cout << "first = " << first->x << "\n";
		it->marked.local() = true;
		for (auto next_iter : it->neighb) {
			if (!(is_next_element_of_cycle(it, next_iter)||(is_the_same_element_of_cycle(it, next_iter) && it != first))) {
			       	continue;
			}
			if (next_iter == first) {
				//std::cout << "Now found: x = " << next_iter->x << ", sv = " << next_iter->sv << "\n";
				return 1;
			}
			if (next_iter->marked.local()) continue;
			//else continue;
			int result = find_cycle_with_nonzero_degree_recur(next_iter, first);
			if (result == 1) {
				//std::cout << "Found: x = " << next_iter->x << ", sv = " << next_iter->sv << "\n";
				return 1;
			}
			else next_iter->marked.local() = false;
		}
		return -1;
	};

	/*int deg () {
		//std::cout << "deg(" << d << ")\n";
		//std::cout << "V: " << V.size() << "\n";
		if (V.empty()) return 0;
		const int n = VecLen(V[0].sv);
		const int d = n - 1;
		if (d == 0) {
			int sum = 0;
			for (const box_node &b : V) {
				if (b.sv[1] == 1) sum += b.orientation;
			}
			return sum;
		}
		//const int n = d + 1;//VecLen(V[0].x);
		std::vector< std::vector<vert_set::iterator> > lists;
		compute_lists_of_selected_boxes(lists);
		//std::cout << "Lists computed: ";
		//for (int i1 = 0; i1 < n*2; ++i1) cout << lists[i1].size() << ", ";
		//std::cout << "\n";
		std::vector< std::vector<vert_set::iterator> >::iterator chosen = std::min_element(lists.begin(), lists.end(), [](const std::vector<vert_set::iterator> &a, const std::vector<vert_set::iterator> &b){return a.size() < b.size();});
		int s = 1;
		int l = chosen - lists.begin() + 1;
		if (l > n) {
			l -= n;
			s = -1;
		}
		const int orientation = (l % 2 == 0)?-1:1;
		box_graph g_new;
		for (auto iter : *chosen) {
			g_new.create_faces_list(iter, l);
		}
		g_new.create_edges();
		return s*orientation*g_new.deg();
	};

	int compute_topol_degree() {
		return deg();
	};*/

	void show_boxes() const {
		for (const box_node &b : V) std::cout << "x = " << b.x << "\n";
	};

	void show_nodes() const {
		std::stringstream ss;

		for (const box_node &b : V) {
			ss << "Node:\n";
			ss << "x = " << b.x << "\n";
			ss << "orientation = " << b.orientation << "\n";
			ss << "sv = " << b.sv << "\n";
			ss << "==============\n";
		}
		ss << "Total nodes: " << V.size() << "\n";
		std::cout << ss.str();
	};

	void show_info() const {
		std::stringstream ss;

		int sufficient_count{0};
		for (const box_node &b : V) {
			for (int i = 1; i <= VecLen(b.sv); ++i) {
				if (b.sv[i] != 0) {
					++sufficient_count;
					break;
				}
			}
			//cout << "Neighb: " << b.neighb.size() << "\n";
			//ss << "Node:\n";
			//ss << "x = " << b.x << "\n";
			//ss << "orientation = " << b.orientation << "\n";
			//ss << "sv = " << b.sv << "\n";
			//ss << "==============\n";
		}
		ss << "Total nodes: " << V.size() << ", sufficient:" << sufficient_count << "\n";
		std::cout << ss.str();
	};
};


int compute_topol_degree(box_graph &g, const intvector &which) {
	if (g.V.empty()) return 0;
	int n = VecLen(g.V[0].sv);
	int coeff = 1;
	//g.show_nodes();
	//g.show_info();
	for (int d = n - 1; d > 0; --d, --n) {
		/*{
			std::stringstream ss;
			ss << "d = " << d << ", V:" << g.V.size() << "\n";
			std::cout << ss.str();
		}*/
		std::vector< std::vector<vert_set::iterator> > lists;
		g.compute_lists_of_selected_boxes(lists);
		std::vector< std::vector<vert_set::iterator> >::iterator chosen = std::min_element(lists.begin(), lists.end(), [](const std::vector<vert_set::iterator> &a, const std::vector<vert_set::iterator> &b){return a.size() < b.size();});
		int s = 1;
		int l = chosen - lists.begin() + 1;
		if (l > n) {
			l -= n;
			s = -1;
		}
		//std::cout << "l = " << l << "\n";
		//std::cout << "size: " << chosen->size() << "\n";
		const int orientation = (l % 2 == 0)?-1:1;
		box_graph g_new;
		g_new.V.clear();
		for (auto iter : *chosen) {
			//std::cout << "create_faces_list, x = " << iter->x << "\nsv = " << iter->sv << "\n";
			//for (auto ni : iter->neighb) std::cout << "\tneighb: x = " << ni->x << "\n\tsv = " << ni->sv << "\n";
			g_new.create_faces_list(iter, l, which);
		}
		//g_new.show_nodes();
		//g_new.show_info();
		if (g_new.V.empty()) return 0;
		g_new.create_edges();
		coeff *= s*orientation;
		g = std::move(g_new);
	}
	/*{
		std::stringstream ss;
		ss << "... and d = 0, V:" << g.V.size() << "\n";
		std::cout << ss.str();
	}*/
	int sum = 0;
	for (const box_node &b : g.V) {
		//std::cout << "(" << b.sv[1] << ", " << b.orientation << ",\n" << b.x << "),\n";
		//std::cout << "(" << b.sv[1] << ", " << b.orientation << "), ";
		if (b.sv[1] == 1) sum += b.orientation;
		//sum += b.orientation*b.sv[1];
		if (b.sv[1] == 0) std::cout << "ojej!\n";
	}
	//sum /= 2;
	//std::cout << "\n";
	/*{
		std::stringstream ss;
		ss << "sum = " << sum << ", coeff = " << coeff << "\n";
		std::cout << ss.str();
	}*/
	return coeff*sum;
	//return sum;
}

#endif

