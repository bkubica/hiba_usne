
#if !defined __THREADED_AUX_HPP__
#define __THREADED_AUX_HPP__

#include <atomic>

enum thread_privacy_t {thread_shared, thread_private};

//auxiliary type - to be used using the alias below (possibly_atomic - without the underscore at the end)

template <typename T, thread_privacy_t priv>
struct possibly_atomic_ {
	using value_type = T;
};

template <typename T> struct possibly_atomic_<T, thread_shared> {
	using value_type = std::atomic<T>;
};

// the alias to use the above template type (possibly_atomic_) conveniently

template <typename T, thread_privacy_t priv>
using possibly_atomic = typename possibly_atomic_<T, priv>::value_type;

//***********************************************************

#include <tbb/spin_mutex.h>
#include <tbb/spin_rw_mutex.h>
#include <tbb/null_mutex.h>
#include <tbb/parallel_invoke.h>

// only DISTR_MUTEX defined -- a distributed spin_rw_mutex
// only NON_RW_MUTEX defined -- a single spin_mutex
// both undefined - a single spin_rw_mutex
// DISTR_MUTEX and NON_RW_MUTEX defined -- an error
// (yeah, complicated, a bit...)

#define DISTR_MUTEX
//#define NON_RW_MUTEX

#if !defined NON_RW_MUTEX
using mutex_bound_cons_type = tbb::spin_rw_mutex;
#elif !defined DISTR_MUTEX
using mutex_bound_cons_type = tbb::spin_mutex;
#endif
//using mutex_bound_cons_type = tbb::null_mutex;

#if defined DISTR_MUTEX
union distr_mutex {
	mutex_bound_cons_type mut;
	char pad[64];

	distr_mutex() : mut() {};

	~distr_mutex() {
		mut.~mutex_bound_cons_type();
	}
};
#endif

//***********************************************************

#endif

