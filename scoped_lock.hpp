#if !defined __SCOPED_LOCK_HPP__
#define __SCOPED_LOCK_HPP__

//#include <typelist.hpp>

//using std_mutex_types = TL::TypeList<std::mutex, std::recursive_mutex, std::timed_mutex, std::timed_recursive_mutex>;

//using tbb_mutex_types = TL::TypeList<tbb::mutex, tbb::recursive_mutex, tbb::spin_mutex, tbb::queueing_mutex>;

//*******************************************

/*template<typename mutex_type, bool is_tbb_type>
struct aux_scoped_lock_of {
	using type = std::lock_guard<mutex_type>;
};

template<typename mutex_type>
struct aux_scoped_lock_of<mutex_type, true> {
	using type = typename mutex_type::scoped_lock;
};*/

//*******************************************

template<typename T>
struct TypeSink {
	using type = void;
};

template<typename T>
using TypeSinkT = typename TypeSink<T>::type;

//*******************************************

template<typename mutex_type, typename T = void>
struct scoped_lock_of_ {
	using type = std::lock_guard<mutex_type>;
};

//struct scoped_lock_of<mutex_type, TL::IndefOf<tbb_mutex_types, mutex_type> != -1> {
//	using type = typename aux_scoped_lock_of<mutex_type, TL::IndefOf<tbb_mutex_types, mutex_type> != -1>::type;
	//using type = std::lock_guard<mutex_type>;
//};

template<typename mutex_type>
struct scoped_lock_of_< mutex_type, TypeSinkT<typename mutex_type::scoped_lock> > {
	using type = typename mutex_type::scoped_lock;
};

//*******************************************

template<typename mutex_type>
using scoped_lock_of = typename scoped_lock_of_<mutex_type>::type;

#endif

