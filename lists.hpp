
#if !defined __LISTS_HPP__
#define __LISTS_HPP__

#include <ivector.hpp>
#include <intvector.hpp>
#include <imatrix.hpp>
#include <intmatrix.hpp>
#include <xscclass.hpp>

#include "tbb/concurrent_vector.h"

#include <fstream>

using namespace cxsc;
using namespace std;

struct elem {
	ivector x;
	//elem *next;
	intvector satisfied; // 0: the constr. can be both satisfied or not, -1: it cannot be satisfied, +1: always satisfied

	//elem (const ivector &x_) : x(x_) {}

	elem (const ivector &x_, const intvector &satisfied_) : x(x_), satisfied(satisfied_) {}

	elem (const elem &e) : x(e.x), satisfied(e.satisfied) {}

	elem (elem &&e) : x(std::move(e.x)), satisfied(std::move(e.satisfied)) {}

	void show () {
		cout << "x = " << x << "\n";
		cout << "satisfied = " << satisfied << "\n";
	}
};


struct elem_guaranteed : public elem {
	intvector which; // 1 if forall x in x[i] a solution exists

	//elem_guaranteed (const ivector &x, const intvector &which_) : elem(x), which(which_) {}

	elem_guaranteed (const ivector &x_, const intvector &satisfied_, const intvector &which_) : elem(x_, satisfied_), which(which_) {}

	elem_guaranteed (const elem_guaranteed &e) : elem(e.x, e.satisfied), which(e.which) {}

	elem_guaranteed (elem_guaranteed &&e) : elem(static_cast<elem>(e)), which(e.which) {}

	void show () {
		cout << "x = " << x << "\n";
		cout << "satisfied = " << satisfied << "\n";
		cout << "which = " << which << endl;
	}
};

bool sufficiently_reduced (const ivector &x_new, const ivector &x_old);

real Lebesgue (const ivector &);


template <typename T>
void save_cairo (const tbb::concurrent_vector<T> &vec, const ivector &x0, const char *name);


template<typename T>
real Lebesgue (const tbb::concurrent_vector<T> &vec);

template<typename T>
interval ILebesgue (const tbb::concurrent_vector<T> &vec);


template<typename T>
real Lebesgue_parallel (const tbb::concurrent_vector<T> &vec);

template<typename T>
interval ILebesgue_parallel (const tbb::concurrent_vector<T> &vec);


template<typename T>
real MaxDiam_parallel (const tbb::concurrent_vector<T> &vec);

#endif

