
#include "solver.hpp"
#include "problem_desc_impl.hpp"

#include <string>

#define N 2
#define M_eq 0
#define M_ineq 2

// N - variables, M - equations

template<int level, SparsityLevel sparse_mode, int n, class T = cxsc::interval>
struct circle2 {
	adhc_ari<level, sparse_mode, n, T> operator() (const adhc_vector<level, sparse_mode, n, T> &x, const int i=0) const {
		adhc_ari<level, sparse_mode, n, T> result;
		//if (i == 1) return sqr(x[1]) - 1.0;
		//else return x[2];
		result = (sqr(x[1]) + sqr(x[2]) - 4.0)*(sqr(x[1]) + sqr(x[2]) - 1.0);
		//result = (power(x[1], 4) + power(x[2], 4) - 4.0)*(power(x[1], 6) + power(x[2], 6) - 1.0);
		//result = sqr(x[1]) + sqr(x[2]) - 4.0;
		return result;
	}
};


template<int level, SparsityLevel sparse_mode, int n, class T = cxsc::interval>
struct inequality_example {
	adhc_ari<level, sparse_mode, n, T> operator() (const adhc_vector<level, sparse_mode, n, T> &x, const int i=0) const {
		adhc_ari<level, sparse_mode, n, T> result;
		//result = x[1] - x[2];
		if (i == 1) result = sqr(x[1]) + sqr(x[2]) - 4.0;
		else result = sqr(x[1]) - x[2];
		//result = sqr(x[1]) - x[2];
		return result;
	}
};

//#define SPARSITY SparsityLevel::dense
//#define SPARSITY sparse
#define SPARSITY SparsityLevel::highly_sparse


int main() {
#include "num_threads.hpp"
	opdotprec = 1;
	ivector x(N);
	x = interval (-3.0, 5.0);
	//x[2] = 0.0;
	//x = interval (1.25, 1.75);
	//x = interval (1.37, 1.419); // albo 1.45; ciekawe roznice: 1.415-1.420
	//x = interval (-100.0, 101.0);
	real eps = 1e-5;//1e-6;
	//eps = 1.0;
	eps = 1e-3;
	//eps = 1e-1;
	//eps = 1e-7;

	problem_desc_impl<SPARSITY, N, M_eq, circle2, M_ineq, inequality_example> p {x};
	p.rhs[1] += interval(-0.1, 0.1);
	//p.rhs[2] += interval(-1.0, 1.0);
	p.rhs[2] += interval(0.0, Infinity);
	//p.rhs[1] += interval(-Infinity, 0.0);
	solver s (num_threads);
	cout << "x0 = " << x << endl;
	create_func creat = Herbort_create_pair_list;
	//create_func creat = Goulard_create_pair_list;
	//create_func creat = GE_create_pair_list;
	//create_func creat = Goulard_GE_create_pair_list;
	s.branch_and_bound (p, eps, creat);
	s.show_results();

	char name_pos[20], name_verif[20];
	strcpy (name_pos, "pos.txt");
	strcpy (name_verif, "ver.txt");
	save_cairo (s.sol, p.x0, name_pos);
	save_cairo (s.guar_sol, p.x0, name_verif);

//#define SHOW
#if defined SHOW
	cout << "Possible: " << endl;
	for (auto &x : s.sol) {
		x.show();
	}
	cout << endl << "Guaranteed: " << endl;
	for (auto &x : s.guar_sol) {
		x.show();
	}
	std::cout << "========\n";
#endif
	s.stat_show();
	return 0;
}

