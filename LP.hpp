
#if !defined __LP_HPP__
#define __LP_HPP__

#include <ivector.hpp>
#include <imatrix.hpp>
#include <rvector.hpp>
#include <rmatrix.hpp>
#include <mutex>

//#include "tbb/mutex.h"

using namespace cxsc;
using namespace std;

int LP_narrowing (ivector &x, ivector &y, imatrix &J, ivector &xc, ivector &yc, imatrix &Jc);

//using LP_mutex = tbb::mutex;
using LP_mutex = std::mutex;

#endif

