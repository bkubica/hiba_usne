int hc_cons_serial(const problem_desc &p, const real &eps, elem &e, intvector &which) {
	which = 1;
	for (int i = 1; i <= p.num_fun + p.num_ineq; ++i) p.hc_initial_evaluate (e.x, i);
	bool modified;
	do {
		modified = false;
		int i = 1;
		for (; i <= p.num_fun; ++i) {
			//std::cout << "i = " << i << '\n';
			int result = p.hc_narrow (e.x, i, which, cxsc::interval(0.0)); ++stat->num_hc_narrow;
			//std::cout << "result = " << result << '\n';
			if (result == -1) {
				//++stat->num_del_bc;
				++stat->num_del_hc;
				return -1;
                	}
			if (result == 1) modified = true;
		}
		for (int ii = 1; ii <= p.num_ineq; ++ii, ++i) {
			if (e.satisfied[ii] != 0) continue;
			int result = p.hc_narrow (e.x, i, which, p.rhs[ii]); ++stat->num_hc_narrow;
			if (result == -1) {
				//++stat->num_del_bc;
				++stat->num_del_hc;
				return -1;
			}
			if (result == 1) modified = true;
		}
	} while (modified);
	return 0;
}


int hc_bc_cons_serial(const problem_desc &p, const real &eps, elem &e, intvector &which) {
	which = 1;
	for (int i = 1; i <= p.num_fun + p.num_ineq; ++i) {
		//if (use_hc_narrow[i]) p.hc_initial_evaluate (x, i);
		if (true) p.hc_initial_evaluate (e.x, i);
	}
	bool modified;
	do {
		modified = false;
		int i = 1;
		for (; i <= p.num_fun; ++i) {
			int result;
			//if (use_hc_narrow[i]) {
			if (true) {
				result = p.hc_narrow (e.x, i, which, cxsc::interval(0.0)); ++stat->num_hc_narrow;
				if (result == -1) {
					++stat->num_del_hc;
					return -1;
				}
				else if (result == 1) modified = true;
			}
			//for (int j = 1; j <= VecLen(x); ++j) {
			for (int j = i, count = 0; count < VecLen(e.x); j = (j == VecLen(e.x))?1:(j + 1), ++count) {
				if (num_terms[i][j] >= 2) {
					result = bc3revise<thread_private>(p, e.x, cxsc::interval(0.0), i, j, eps, which, modified);
					++stat->num_bc3rev;
					if (result == -1) {
						++stat->num_del_bc;
						return -1;
					}
					else if (result == 1) modified = true;
				}
			}
		}
		for (int ii = 1; ii <= p.num_ineq; ++ii, ++i) {
			if (e.satisfied[ii] != 0) continue;
			int result = p.hc_narrow (e.x, i, which, p.rhs[ii]); ++stat->num_hc_narrow;
			if (result == -1) {
				//++stat->num_del_bc;
				++stat->num_del_hc;
				return -1;
			}
			if (result == 1) modified = true;
		}
	} while (modified);
	return 0;
}

