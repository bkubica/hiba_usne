
#include "borsuk.hpp"

#include <xi_ari.hpp>
//#include <matinv_aprx.hpp>
//#include <matinv.hpp>
#include <tbb/blocked_range.h>
#include <tbb/parallel_reduce.h>

#if defined MKL
#include <mkl_lapacke.h>
#else
#include <lapacke.h>
#endif

//#define PRECOND_BORSUK

cxsc::rmatrix compute_Q_g (const int n) {
	cout << "compute Q_g\n";
	if (n < 2) {
		cxsc::rmatrix result(n, n);
		result = Id(result);
		return result;
	}
	double a_[n*n], tau_[n*2];
	a_[0] = 100.0;
	a_[1] = -100.0;
	a_[2] = a_[3] = 1.0;
	for (int ind = 4; ind < n*2; ++ind) a_[ind] = 0.0;
	/*{
		int ind = 4;
		for (int i = 2; i <= n - 1; ++i) {
			a_[ind] = 100.0;
			a_[ind + 1] = -100.0;
			ind += n;
		}
		a_[ind] = a_[ind + 1] = 1.0;
	}*/
	/*{
		a_[0] = 100.0; a_[1] = 1.0;
		a_[n] = -100.0; a_[n + 1] = 1.0;
	}*/
	const int lda = 2;//????????
	int error = LAPACKE_dgeqrf(LAPACK_ROW_MAJOR, n, 2, a_, lda, tau_);
	cout << "error dgeqrf Qg = " + std::to_string(error) + "\n";
	error = LAPACKE_dorgqr(LAPACK_ROW_MAJOR, n, n, 2, a_, n, tau_);
	cout << "error dorgqr Qg = " + std::to_string(error) + "\n";
	cxsc::rmatrix result(n, n);
	int ind = 0;
	for (int i = 1; i <= n; ++i)
		for (int j = 1; j <= n; ++j)
			result[i][j] = a_[ind++];
	result[1][1] = 100.0; result[1][2] = -100.0;
	result[2][1] = 1.0; result[2][2] = 1.0;
	for (int i = 3; i <= n; ++i) result[i][1] = result[i][2] = 0.0;
	return result;
}


int compute_precond_for_borsuk (const cxsc::ivector &y1, const cxsc::ivector &y2, cxsc::rmatrix &Y) {
	const int n = VecLen(y1);
	Y = cxsc::rmatrix(n, n);
	Y = Id(Y);
	if (n < 2) return -2;
	//static cxsc::rmatrix Q_g = compute_Q_g(n);
	//std::terminate();
	//return -1;
	//cout << "y1 = " << y1 << ", y2 = " << y2 << "\n";
	cxsc::rvector y1c = (Inf(y1) + Sup(y1))*0.5;
	cxsc::rvector y2c = (Inf(y2) + Sup(y2))*0.5;
	double a_[n*n], tau_[n*2];
	int ind = 0;
	for (int i = 1; i <= n; ++i) {
		a_[ind++] = _double(y1c[i]);
		a_[ind++] = _double(y2c[i]);
	}
	const int lda = 2;//????????
	int error = LAPACKE_dgeqrf(LAPACK_ROW_MAJOR, n, 2, a_, lda, tau_);
	//cout << "error dgeqrf = " + std::to_string(error) + "\n";
	if (error != 0) return -1;
	error = LAPACKE_dorgqr(LAPACK_ROW_MAJOR, n, n, 2, a_, n, tau_);
	//cout << "error dorgqr = " + std::to_string(error) + "\n";
	if (error != 0) return -1;
	ind = 0;
	for (int i = 1; i <= n; ++i) {
		a_[ind] = _double(y1c[i]);
		a_[ind + 1] =_double(y2c[i]);
		ind += n;
	}
	/*cxsc::rmatrix debug_Q_f(n, n);
	{
		ind = 0;
		for (int i = 1; i <= n; ++i)
			for (int j = 1; j <= n; ++j)
				debug_Q_f[i][j] = a_[ind++];
	}*/
	int ipiv[n + 1];
	error = LAPACKE_dgetrf(LAPACK_ROW_MAJOR, n, n, a_, n, ipiv);
	//cout << "error = " + std::to_string(error) + "\n";
	if (error != 0) return -1;
	//cout << "hej\n";
	error = LAPACKE_dgetri(LAPACK_ROW_MAJOR, n, a_, n, ipiv);
	if (error != 0) return -1;
	//cout << "plum\n";
	cxsc::rmatrix inv_Q_f(n, n);
	//cxsc::rmatrix Q_f(n, n);
	ind = 0;
	for (int i = 1; i <= n; ++i)
		for (int j = 1; j <= n; ++j)
			inv_Q_f[i][j] = a_[ind++];
	//cout << "inv_Q_f = " << inv_Q_f << "\n";
	static cxsc::rmatrix Q_g = compute_Q_g(n); // computing a constant, shared between threads
						   // proper only for C++11 and higher
	//cout << "Q_g = " << Q_g << "\n";
	/*Q_g[1][1] = 100.0; Q_g[1][2] = -100.0; //Q_f[1][1] = y1c[1]; Q_f[1][2] = y2c[1];
	Q_g[2][1] = 1.0; Q_g[2][2] = 1.0; //Q_f[2][1] = y1c[2]; Q_f[2][2] = y2c[2];
	for (int i = 3; i <= n; ++i) {
		Q_g[i][1] = Q_g[i][2] = 0.0;
		//Q_f[i][1] = y1c[i];
		//Q_f[i][2] = y2c[i];
	}*/
	//return -1;
	//MatInvAprx (Q_f, aux, error);
	//MatInv (Q_f, aux, error);
	//return -1;
	Y = Q_g*inv_Q_f;
	//cout << Q_g << " = Q_g == " << Y*debug_Q_f << "\n";
	//std::terminate();
	return 0;
}


void narrowly_compute_all_f (const problem_desc &p, const cxsc::ivector &x, cxsc::ivector &y) {
	p.compute_all_f(x, y);
	cxsc::imatrix Jac(p.num_fun, VecLen(x));
	p.compute_all_grad_f(x, y, Jac);
	cxsc::ivector xc { (Inf(x) + Sup(x))*0.5 };
	cxsc::ivector yc;
	p.compute_all_f(xc, yc);
	y &= yc + Jac*(x - xc);
}


int check_borsuk_for_i (const problem_desc &p, const cxsc::ivector &x1, const cxsc::ivector &x2) {
	cxsc::ivector y1(p.num_fun), y2(p.num_fun);
	narrowly_compute_all_f(p, x1, y1);
	narrowly_compute_all_f(p, x2, y2);
	//p.compute_all_f(x1, y1);
	//p.compute_all_f(x2, y2);
#if defined PRECOND_BORSUK
	cxsc::rmatrix Y;
	int error = compute_precond_for_borsuk(y1, y2, Y);
	y1 = Y*y1;
	y2 = Y*y2;
#endif
	//cout << "x1 = " << x1 << "\nx2 = " << x2 << "\n";
	//cout << "y1 = " << y1 << "\ny2 = " << y2 << "\n";
	cxsc::interval result (0.0, Infinity);
	for (int j = 1; j <= p.num_fun; ++j) {
		if (0.0 <= y2[j]) {
			xinterval division = y1[j] % y2[j];
			cxsc::ivector vec = result & division;
			if (vec[1] == EmptyIntval()) return 1;
			if (vec[2] == EmptyIntval()) result = vec[1];
			else result = vec[1] | vec[2];
			//return 0;
		}
		else {
			//cout << "y2 nonzero: j = " << j << "\n";
			cxsc::interval division = y1[j]/y2[j];
			if (Disjoint(result, division)) return 1;
			result &= division;
		}
		//cout << "result(" << j << ") = " << result << "\n";
	}
	return (Sup(result) <= 0.0);
	//return 0;
}


int check_borsuk (const problem_desc &p, const cxsc::ivector &x) {
	//cout << "var_num = " << var_num << "\n";
	int count {0};
	for (int i = 1; i <= VecLen(x); ++i) {
		cxsc::ivector x1{x};
		SetInf(x1[i], Sup(x1[i]));
		cxsc::ivector x2{x};
		SetSup(x2[i], Inf(x2[i]));
		//cout << "for i = " << i << " : " << check_borsuk_for_i(p, x1, x2) << "\n";
		count += check_borsuk_for_i(p, x1, x2);
	}
	return (count >= p.num_fun);
}


int check_borsuk_parallel (const problem_desc &p, const cxsc::ivector &x) {
	int initial_value {0};
	int cnt = tbb::parallel_reduce (tbb::blocked_range<int>(1, VecLen(x) + 1), initial_value,
        		[&](const tbb::blocked_range<int> &r, int count)->int {
				for (int i = r.begin(), i_end = r.end(); i != i_end; ++i) {
					cxsc::ivector x1{x};
					SetInf(x1[i], Sup(x1[i]));
					cxsc::ivector x2{x};
					SetSup(x2[i], Inf(x2[i]));
					count += check_borsuk_for_i(p, x1, x2);
				}	
           			return count;
        		},
			[](const int c1, const int c2)->int {
				return c1 + c2;
			}
	);
	return (cnt >= p.num_fun);
}


/*int check_borsuk_subdiv (const problem_desc &p, const cxsc::ivector &x, const cxsc::rmatrix &Y, const intvector &var_num) {
	//cout << "var_num = " << var_num << "\n";
	std::vector< std::pair<cxsc::ivector, cxsc::ivector> > subfacets;
	subfacets.clear();
	for (int it = 1; it <= p.num_fun; ++it) {
		const int i = var_num[it];
		cxsc::ivector x1{x};
		SetInf(x1[i], Sup(x1[i]));
		cxsc::ivector x2{x};
		SetSup(x2[i], Inf(x2[i]));
		//cout << "for i = " << i << " : " << check_borsuk_for_i(p, x1, x2) << "\n";
		if (!check_borsuk_for_i(p, x1, x2, Y)) return 0;
	}
	return 1;
}*/

