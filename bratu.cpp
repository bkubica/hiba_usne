
#include "solver.hpp"
#include "problem_desc_impl.hpp"

#include <string>

#define N 30
#define M N

// N - variables, M - equations

template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> eq (const adhc_vector<level, sparse_mode, n, T> &x, const int num) {
	adhc_ari<level, sparse_mode, n, T> result;
	result = exp(x[num]) * (1.0/sqr(N + 1.0)) - 2*x[num];
	if (num > 1) result += x[num - 1];
	if (num < N) result += x[num + 1];
	//result += (num > 1)?x[num - 1]:0.0;
	//result += (num < N)?x[num + 1]:0.0;
	/*result = x[num]*(2.0 + 5.0*sqr(x[num]));
	int j = max(1, num - 5);
	int j_max = min(N, num + 1);
	while (j <= j_max) {
		if (j != num) result += x[j]*(1.0 + x[j]);
		j++;
	}*/
	return result;
}


template<int level, SparsityLevel sparse_mode, int n, class T>
struct bratu {
	adhc_ari<level, sparse_mode, n, T> operator() (const adhc_vector<level, sparse_mode, n, T> &x, const int i=0) const {
		return eq(x, i);
	};
};


//#define SPARSITY SparsityLevel::dense
//#define SPARSITY sparse
#define SPARSITY SparsityLevel::highly_sparse


int main() {
#include "num_threads.hpp"
	opdotprec = 1;
	ivector x(N);
	x = interval (-1e8, 20.0);
	real eps = /*1e-2;// */1e-6;//1e-4;//1e-1;
	//eps = 1e-3;
	//eps = 1.0;
	//eps = 1000.0;
	//bratu p (x, M);
	problem_desc_impl<SPARSITY, N, M, bratu> p(x);
	solver s (num_threads);
	cout << "x0 = " << x << endl;
	create_func creat = Herbort_create_pair_list;
	//create_func creat = Goulard_create_pair_list;
	//create_func creat = GE_create_pair_list;
	//create_func creat = Goulard_GE_create_pair_list;
	s.branch_and_bound (p, eps, creat);
//#define SHOW
#if defined SHOW
	cout << "Possible: " << endl;
	for (auto &x : s.sol) {
		x.show();
	}
	cout << endl << "Guaranteed: " << endl;
	for (auto &x : s.guar_sol) {
		x.show();
	}
	std::cout << "========\n";
#endif
	s.show_results();
	s.stat_show();
	return 0;
}

