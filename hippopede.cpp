
#include "solver.hpp"
#include "problem_desc_impl.hpp"

#include <string>

#define N 3
#define M 2

// N - variables, M - equations

template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> eq1 (const adhc_vector<level, sparse_mode, n, T> &x) {
	adhc_ari<level, sparse_mode, n, T> result;
	result = sqr(x[1]) + sqr(x[2]) - x[3];
	return result;
}

template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> eq2 (const adhc_vector<level, sparse_mode, n, T> &x) {
	adhc_ari<level, sparse_mode, n, T> result;
	result = sqr(x[2]) + sqr(x[3]) - 1.1*x[3];
	return result;
}


template<int level, SparsityLevel sparse_mode, int n, class T>
struct hippopede {
	adhc_ari<level, sparse_mode, n, T> operator() (const adhc_vector<level, sparse_mode, n, T> &x, const int i) const {
		return (i==1)?eq1(x):eq2(x);
	};
};


//#define SPARSITY SparsityLevel::dense
//#define SPARSITY sparse
#define SPARSITY SparsityLevel::highly_sparse


int main() {
#include "num_threads.hpp"
	opdotprec = 1;
	ivector x(N);
	x[1] = interval (-1.5, 1.5);
	x[2] = interval (-1.0, 1.0);
	x[3] = interval (0.0, 4.0);
	real eps = 1e-7;
	//eps = 1e-4;
	problem_desc_impl<SPARSITY, N, M, hippopede> p(x);
	solver s (num_threads);
	cout << "x0 = " << x << endl;
	create_func creat = Herbort_create_pair_list;
	//create_func creat = Goulard_create_pair_list;
	//create_func creat = GE_create_pair_list;
	//create_func creat = Goulard_GE_create_pair_list;
	s.branch_and_bound (p, eps, creat);
	s.show_results();
	s.stat_show();
	return 0;
}

