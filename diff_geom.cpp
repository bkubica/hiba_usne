
#include "solver.hpp"
#include "problem_desc_impl.hpp"

#include <string>

#define N 2
#define M 1

// N - variables, M - equations

template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> lemniscate_Bern (const adhc_vector<level, sparse_mode, n, T> &x) {
#define a 1.0
	adhc_ari<level, sparse_mode, n, T> result;
	//result = sqr(sqr(x[1]) + sqr(x[2]) + sqr(a)) - 4.0*sqr(a)*sqr(x[1]) - power(a, 4);
	result = sqr(sqr(x[1]) + sqr(x[2])) - 2.0*sqr(a)*(sqr(x[1]) - sqr(x[2]));
	return result;
#undef a
}

template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> adjacent_circles (const adhc_vector<level, sparse_mode, n, T> &x) {
	adhc_ari<level, sparse_mode, n, T> result;
	//result = (sqr(x[1] - 1.0) + sqr(x[2]) - 1.0)*(sqr(x[1] + 1.0) + sqr(x[2]) - 1.0);
	result = sqr(sqr(x[1]) + sqr(x[2])) - 4.0*sqr(x[1]);
	return result;
}


template<int level, SparsityLevel sparse_mode, int n, class T>
struct diff_geom {
	adhc_ari<level, sparse_mode, n, T> operator() (const adhc_vector<level, sparse_mode, n, T> &x, const int i) const {
		return lemniscate_Bern(x);
		//return adjacent_circles(x);
	};
};


#define SPARSITY SparsityLevel::highly_sparse


int main() {
#include "num_threads.hpp"
	opdotprec = 1;
	ivector x(N);
	x[1] = interval (-2.0, 2.0);
	x[2] = interval (-2.0, 2.0);
	real eps = 1e-5;
	//eps = 1e-7;
	problem_desc_impl<SPARSITY, N, M, diff_geom> p(x);
	solver s (num_threads);
	cout << "x0 = " << x << endl;
	create_func creat = Herbort_create_pair_list;
	//create_func creat = Goulard_create_pair_list;
	//create_func creat = GE_create_pair_list;
	//create_func creat = Goulard_GE_create_pair_list;
	s.branch_and_bound (p, eps, creat);
//#define SHOW
#if defined SHOW
	cout << "Possible: " << endl;
	for (auto &x : s.sol) {
		x.show();
	}
	cout << endl << "Guaranteed: " << endl;
	for (auto &x : s.guar_sol) {
		x.show();
	}
	std::cout << "========\n";
#endif
	s.show_results();
	s.stat_show();
	return 0;
}

