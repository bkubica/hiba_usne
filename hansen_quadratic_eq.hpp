int solve_quadratic_eq_real_coef (const real &a_, const real &b_, const real &c_, interval &x1, interval &x2) {
	//cout << "solve_quadratic_eq_real_coef\n";
	//cout << " a = " << a_ << ", b = " << b_ << ", c = " << c_ << endl;
	if (a_ == 0.0) {
		// the linear case?
		x1 = -interval(c_)/interval(b_);
		//cout << "linear: x1 = " << x1 << endl;
		return 1;
	}
	//++stat->num_quadr;
	interval a(a_), b(b_), c(c_);
	interval delta = sqr(b) - 4*a*c;
	if (Sup(delta) < 0.0) {
		//cout << "no solutions: delta negative, delta = " << delta << endl;
		//++stat->num_del_quadr_delta_neg;
		return 0;
	}
	//if (0.0 <= delta) {
	//if (0.0 <= b || 0.0 <= c) {
	if (0.0 <= c) {
		//cout << "Houston, we have a problem!\n";
		return -1;
	}
	/*if (0.0 <= delta) {
		//cout << "Delta contains zero!\n";
		return -1;
	}*/
	interval sqrt_delta = sqrt(delta & interval(0.0, Infinity));
	interval aux;
	if (Inf(b) > 0.0) {
		aux = -(b + sqrt_delta);
		//x1 = -(b + sqrt_delta)/a/2;
		//x2 = c/a/x1;
	}
	else {
		aux = (-b + sqrt_delta);
		//x2 = (-b + sqrt_delta)/a/2;
		//x1 = c/a/x2;
	}
	x1 = aux/a/2;
	x2 = 2*c/aux;
	//cout << "x1 = " << x1 << endl;
	//cout << "x2 = " << x2 << endl;
	return 2;
}


bool compare_intervals(const interval &x, const interval &y) {
	return (Inf(x) <= Inf(y));
}


int use_quadratic_hansen (const problem_desc &p, int i, ivector &x, ivector &x2, ivector &x3, intvector &which) {
	//cout << "use_quadratic_hansen\n";
	interval y, y0;
	ivector g, g0;
	imatrix H;
	ivector xc = ivector(Inf(x) + Sup(x))/2;
	p.compute_grad_f (xc, i, y0, g0); ++stat->num_grad_f;
	p.compute_hess_f (x, i, y, g, H); ++stat->num_hess_f;
	//cout << "H = " << H << endl;
	//cout << "g0 = " << g0 << endl;
	//cout << "y0 = " << y0 << endl;
	int n = VecLen(x);
	//x2 = x;
	ivector v = x - xc;
	which = 1;
	for (int j = 1; j <= n; ++j) {
		//cout << "j = " << j << endl;
		interval a = H[j][j]/2;
		interval b = g0[j], c = y0;
		for (int ii = 1; ii <= n; ++ii) {
			if (ii != j) {
				b += H[ii][j]*v[ii];
				c += H[ii][ii]*sqr(v[ii])/2 + g0[ii]*v[ii];
				for (int jj = ii + 1; jj <= n; ++jj) {
					c += H[ii][jj]*v[ii]*v[jj];
				}
			}
		}
		//cout << "a = " << a << endl;
		//cout << "b = " << b << endl;
		//cout << "c = " << c << endl;
		if (Inf(a) == 0.0 && Sup(a) == 0.0) continue;
		if (Sup(a) <= 0.0) {
			a = -a;
			b = -b;
			c = -c;
		}
		++stat->num_quadr;
		interval x1_, x2_;
		tbb::concurrent_vector<interval> solutions;
		// f1
		int num_sol = solve_quadratic_eq_real_coef(Inf(a), Inf(b), Inf(c), x1_, x2_);
		if (num_sol > 0) {
			if (Inf(x1_) > 0.0) solutions.push_back(x1_);
			if (num_sol > 1 && Inf(x2_) > 0.0) solutions.push_back(x2_);
		}
		else if (num_sol == -1) continue;
		// f2
		num_sol = solve_quadratic_eq_real_coef(Inf(a), Sup(b), Inf(c), x1_, x2_);
		if (num_sol > 0) {
			if (Sup(x1_) < 0.0) solutions.push_back(x1_);
			if (num_sol > 1 && Sup(x2_) < 0.0) solutions.push_back(x2_);
		}
		else if (num_sol == -1) continue;
		// f3
		num_sol = solve_quadratic_eq_real_coef(Sup(a), Sup(b), Sup(c), x1_, x2_);
		if (num_sol > 0) {
			if (Inf(x1_) > 0.0) solutions.push_back(x1_);
			if (num_sol > 1 && Inf(x2_) > 0.0) solutions.push_back(x2_);
		}
		else if (num_sol == -1) continue;
		// f4
		num_sol = solve_quadratic_eq_real_coef(Sup(a), Inf(b), Sup(c), x1_, x2_);
		if (num_sol > 0) {
			if (Sup(x1_) < 0.0) solutions.push_back(x1_);
			if (num_sol > 1 && Sup(x2_) < 0.0) solutions.push_back(x2_);
		}
		else if (num_sol == -1) continue;
		if ((Inf(a) < 0.0)||(Inf(a) == 0.0 && Sup(b) > 0.0)||(Inf(a) == 0.0 && Inf(b) == 0.0 && Inf(c) < 0.0 )) {
			solutions.push_back(interval(-Infinity));
		}
		if ((Inf(a) < 0.0)||(Inf(a) == 0.0 && Inf(b) < 0.0)||(Inf(a) == 0.0 && Inf(b) == 0.0 && Inf(c) < 0.0 )) {
			solutions.push_back(interval(Infinity));
		}
		interval v1, v2, v3;
		if (solutions.size() == 0) {
			++stat->num_del_quadr_delta_neg;
			return -1;
		}
		else if(solutions.size() == 2) {
			tbb::parallel_sort (solutions.begin(), solutions.end(), compare_intervals);
			v1 = interval(Inf(solutions[0]), Sup(solutions[1]));
			interval xx = xc[j] + v1;
			if (Disjoint(xx, x[j])) {
				++stat->num_del_quadr_disjoint;
				return -1;
			}
			/*if (in(xx, x[j])) {
				x[j] = xx;
				which[j] = 0;
				if (!(0.0 <= a))return 1;
			}
			else*/ x[j] &= xx;
		}
		else if (solutions.size() == 4) {
			//cout << "4 solutions: j = " << j << endl;
			tbb::parallel_sort (solutions.begin(), solutions.end(), compare_intervals);
			v1 = interval(Inf(solutions[0]), Sup(solutions[1]));
			v2 = interval(Inf(solutions[2]), Sup(solutions[3]));
			//cout << "v1 = " << v1 << endl;
			//cout << "v2 = " << v2 << endl;
			interval xx1 = xc[j] + v1;
			interval xx2 = xc[j] + v2;
			//cout << "xx1 = " << xx1 << endl;
			//cout << "xx2 = " << xx2 << endl;
			//cout << "x[j] = " << x[j] << endl;
			if (Disjoint(xx1, x[j])) {
				if(Disjoint(xx2, x[j])) {
					++stat->num_del_quadr_disjoint;
					return -1;
				}
				else if (in(xx2, x[j])) {
					x[j] = xx2;
					which[j] = 0;
					if (!(0.0 <= a))return 1;
				}
				else x[j] &= xx2;
			}
			else {
				if(Disjoint(xx2, x[j])) {
					if (in(xx1, x[j])) {
						x[j] = xx1;
						which[j] = 0;
						if (!(0.0 <= a))return 1;
					}
					else x[j] &= xx1;
				}
				else {
					if (Disjoint(xx1, xx2)) {
						x2 = x;
						x[j] &= xx1;
						x2[j] &= xx2;
						//cout << "quadratic bisected\n";
						//cout << " x = " << x << endl;
						//cout << " x2 = " << x2 << endl;
						++stat->num_bis_quadr; 
						return 3;
					}
					else x[j] &= (xx1 | xx2);
				}
			}
		}
		else if (solutions.size() == 6) {
			//cout << "6 solutions\n";
			//return 0;
			tbb::parallel_sort (solutions.begin(), solutions.end(), compare_intervals);
			v1 = interval(Inf(solutions[0]), Sup(solutions[1]));
			v2 = interval(Inf(solutions[2]), Sup(solutions[3]));
			v3 = interval(Inf(solutions[4]), Sup(solutions[5]));
			//cout << "v1 = " << v1 << endl;
			//cout << "v2 = " << v2 << endl;
			//cout << "v3 = " << v3 << endl;
			//x3 = x;
			interval xx1 = xc[j] + v1;
			interval xx2 = xc[j] + v2;
			interval xx3 = xc[j] + v3;
			/*if (Disjoint(xx1, x[j])) {
				if(Disjoint(xx2, x[j])) {
					if(Disjoint(xx3, x[j])) {
						++stat->num_del_quadr_disjoint;
						return -1;
					}
					else if (in(xx3, x[j])) {
						;
					}
				}
				else if (in(xx2, x[j])) {
					;
				}
			}*/
			tbb::concurrent_vector<interval> xs;
			xs.clear();
			if (!Disjoint(xx1, x[j])) xs.push_back(xx1);
			if (!Disjoint(xx2, x[j])) xs.push_back(xx2);
			if (!Disjoint(xx3, x[j])) xs.push_back(xx3);
			//cout << "before analysis:\n";
			//for (size_t iter = 0; iter < xs.size(); ++iter) cout << "xs[" << iter << "] = " << xs[iter] << endl;
			//cout << "after\n";
			if (xs.size() == 0) {
				++stat->num_del_quadr_disjoint;
				return -1;
			}
			if (xs.size() == 1) {
				if (in(xs[0], x[j])) {
					x[j] = xs[0];
					which[j] = 0;
					if (!(0.0 <= a))return 1;
				}
				else x[j] &= xs[0];
			}
			else {
				for (tbb::concurrent_vector<interval>::iterator iter = xs.begin(); iter != xs.end(); ++iter) {
					*iter &= x[j];
				}
				//for (size_t iter = 0; iter < xs.size(); ++iter) xs[iter] &= x[j];
				//for (size_t iter = 0; iter < xs.size(); ++iter) cout << "xs[" << iter << "] = " << xs[iter] << endl;
				if (xs.size() >= 2) {
					cout << "trisection!!!\n";
					x2 = x3 = x;
					x[j] = xs[0];
					x2[j] = xs[1];
					if (xs.size() == 3) {
						x3[j] = xs[2];
						++stat->num_bis_quadr;
						return 5;
					}
					else if (xs.size() > 3) {
						cout << "wrong number of solutions!!!\n";
						abort();
					}
					++stat->num_bis_quadr;
					return 3;
				}
				continue;
			}
			//return 5;
		}
		else {
			cout << "Wrong number of quadratic equation solutions: " << solutions.size() << endl << "Solutions: "; 
			for (tbb::concurrent_vector<interval>::const_iterator i = solutions.begin(); i != solutions.end(); ++i) cout << *i << endl;
			abort();
			return -1;
		}
		//cout << "currently, x = " << x << endl;
		//cout << "currently, x2 = " << x2 << endl;
	}
	return 0;
}

