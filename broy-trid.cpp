
#include "solver.hpp"
#include "problem_desc_impl.hpp"

#include <string>

#define N 50
#define M N

// N - variables, M - equations

template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> eq (const adhc_vector<level, sparse_mode, n, T> &x, const int num) {
	adhc_ari<level, sparse_mode, n, T> result;
	/*result = x[num]*(2.0 + 5.0*sqr(x[num]));
	int j = max(1, num - 5);
	int j_max = min(N, num + 1);
	while (j <= j_max) {
		//if (j != num) result += x[j]*(1.0 + x[j]);
		if (j != num) result += (sqr(x[j] + 0.5) - 0.25);
		++j;
	}*/
	if (num == 1) result = (3.0 - 2.0*x[1])*x[1] - 2.0*x[2] + 1.0;
	else if (num == N) result = (3.0 - 2.0*x[N])*x[N] - x[N - 1] + 1.0;
	else result = (3.0 - 2.0*x[num])*x[num] - x[num - 1] - 2.0*x[num + 1] + 1.0;
	return result;
}

template<int level, SparsityLevel sparse_mode, int n, class T>
struct broyden_trid {
	adhc_ari<level, sparse_mode, n, T> operator() (const adhc_vector<level, sparse_mode, n, T> &x, const int i=0) const {
		return eq(x, i);
	};
};


//#define SPARSITY SparsityLevel::dense
//#define SPARSITY sparse
#define SPARSITY SparsityLevel::highly_sparse
//#define SPARSITY SparsityLevel::another_sparse


int main() {
#include "num_threads.hpp"
	opdotprec = 1;
	ivector x(N);
	x = interval (-100.0, 101.0);
	//x = interval (-100.0, 100.0);
	//x = interval (-1.0, 1.0);
	real eps = /*1e-2;// */1e-6;//1e-4;//1e-1;
	//----eps = 1e-8;
	problem_desc_impl<SPARSITY, N, M, broyden_trid> p(x);
	solver s (num_threads);
	cout << "x0 = " << x << endl;
	create_func creat = Herbort_create_pair_list;
	//create_func creat = Goulard_create_pair_list;
	//create_func creat = GE_create_pair_list;
	//create_func creat = Goulard_GE_create_pair_list;
	s.branch_and_bound (p, eps, creat);
//#define SHOW
#if defined SHOW
	cout << "Possible: " << endl;
	for (auto &x : s.sol) {
		x.show();
	}
	cout << endl << "Guaranteed: " << endl;
	for (auto &x : s.guar_sol) {
		x.show();
	}
	std::cout << "========\n";
#endif
	s.show_results();
	s.stat_show();
	return 0;
}

