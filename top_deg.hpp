#if !defined __TOP_DEG_HPP__
#define __TOP_DEG_HPP__

#include "box_graph.hpp"


using sign_covering = std::vector< std::pair<cxsc::ivector, cxsc::intvector> >;


int check_system_of_two_equations (const problem_desc &p, const cxsc::ivector &x, const cxsc::real &eps) {
	//std::cout << "check_system_of_two_equations\n";
	box_graph g;
	cxsc::intvector dummy;
	bool result = g.create_from_boundary<AlgVersion::find_cycle> (x, dummy, 1, p, eps);
	//g.show_info();
	g.merge_nodes_with_equal_sv();
	//std::cout << "after merging:\n";
	//g.show_info();
	std::atomic<int> res;
	res = -1;
	//g.show_nodes();
	//for (auto it = g.V.begin(); it != g.V.end(); ++it) {
	tbb::parallel_for(0, static_cast<int>(g.V.size()), 1, 
		[&] (int i) {
			if (res == 1) return;
			auto it = g.V.begin() + i;
			if (g.find_cycle_with_nonzero_degree(it) == 1) {
				//cout << "For box x = " << x << "\n";
				//g.show_nodes();
				res = 1;
				//return 1;
			}
		}
	);
	return res;//-1;
}


int compute_topol_degree (const problem_desc &p, const cxsc::ivector &x, const cxsc::real &eps) {
	box_graph g;
	cxsc::intvector dummy(VecLen(x));
	dummy = 0;
	bool result = g.create_from_boundary<AlgVersion::compute_top_deg> (x, dummy, 1, p, eps);
	//g.show_nodes();
	//g.show_info();
	if (result) {
		//--std::cout << "OK\n";
		g.show_info();
		/*int td = compute_topol_degree(g);
		std::stringstream ss;
		ss << "top_deg = " << td << "\n";
		std::cout << ss.str();
		return td;*/
		return compute_topol_degree(g, dummy);
		//return g.compute_topol_degree();
	}
	//else if (diam(x[1]) > 1e-6) std::cout << "Sufficient covering not obtained\n";
	//else std::cout << "Sufficient covering not obtained\n";
	return 0;
}


int compute_topol_degree_underdet (const problem_desc &p, const cxsc::ivector &x, const intvector &which, const cxsc::real &eps) {
	/*{
		std::stringstream ss;
		ss << "which = " << which << "\n";
		std::cout << ss.str();
	}*/
	box_graph g;
	bool result = g.create_from_boundary<AlgVersion::compute_top_deg_underdetermined> (x, which, 1, p, eps/8);
	//--if (result) std::cout << "OK\n";
	//g.show_nodes();
	//--g.show_info();
	if (result) {
		return compute_topol_degree(g, which);
	}
	//else std::cout << "Sufficient covering not obtained\n";
	return 0;
}

#endif

