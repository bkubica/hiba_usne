
#include "solver.hpp"
#include "problem_desc_impl.hpp"

#include <string>

#define VER2

#define NUM_JOINTS 5

#if !defined VER2
#define N (2*NUM_JOINTS)
//#define M (NUM_JOINTS + 3)
#define M (NUM_JOINTS + 1)
#else
#define N (2*NUM_JOINTS - 2)
#define M (NUM_JOINTS + 1)
#endif

// N - variables, M - equations

#define x_fin 1.0
#define y_fin 1.0
#define phi_fin (M_PI/2)
#define l(i) 1.0


#define c(i) x[2*i - 1]
#define s(i) x[2*i]

template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> eq (const adhc_vector<level, sparse_mode, n, T> &x, const int num) {
	adhc_ari<level, sparse_mode, n, T> result;
	if (num <= NUM_JOINTS - 1) {
		result = sqr(s(num)) + sqr(c(num)) - 1.0;
	}
	else if (num == NUM_JOINTS + 1) {
		result = T(-x_fin);
		for (int i = 1; i <= NUM_JOINTS; ++i) result += l(i)*c(i);
	}
	else if (num == NUM_JOINTS /*+ 2*/) {
		result = T(-y_fin);
		for (int i = 1; i <= NUM_JOINTS; ++i) result += l(i)*s(i);
	}
	else if (num == NUM_JOINTS + 3) {
		//result = c(NUM_JOINTS);
		throw std::invalid_argument("n_r(x, " + std::to_string(num) + ")");
		//result = s(NUM_JOINTS)/c(NUM_JOINTS);
		//result = s(NUM_JOINTS) - 1.0;
	}
	else {
		//result = s(NUM_JOINTS) - 1.0;
		//exit(-1);
		throw std::invalid_argument("n_r(x, " + std::to_string(num) + ")");
	}
	return result;
}

template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> eq_v2 (const adhc_vector<level, sparse_mode, n, T> &x, const int num) {
	adhc_ari<level, sparse_mode, n, T> result;
	if (num < NUM_JOINTS) {
		result = sqr(s(num)) + sqr(c(num)) - 1.0;
	}
	else if (num == NUM_JOINTS) {
		result = T(-x_fin);
		for (int i = 1; i < NUM_JOINTS; ++i) result += l(i)*c(i);
		result += l(NUM_JOINTS);
	}
	else if (num == NUM_JOINTS + 1) {
		result = T(-y_fin);
		for (int i = 1; i < NUM_JOINTS; ++i) result += l(i)*s(i);
	}
	else {
		throw std::invalid_argument("n_r(x, " + std::to_string(num) + ")");
	}
	return result;
}
#undef s
#undef c

template<int level, SparsityLevel sparse_mode, int n, class T>
struct n_r {
	adhc_ari<level, sparse_mode, n, T> operator() (const adhc_vector<level, sparse_mode, n, T> &x, const int num) const {
#if !defined VER2
		return eq(x, num);
#else
		return eq_v2(x, num);
#endif
	};
};


//#define SPARSITY SparsityLevel::dense
//#define SPARSITY sparse
#define SPARSITY SparsityLevel::highly_sparse


int main() {
#include "num_threads.hpp"
	opdotprec = 1;
	ivector x(N);
	x = interval (-1.0, 1.0);
#if !defined VER2
	x[N - 1] = 0.0;
	x[N] = 1.0;
	//x[N] = interval (0.0, 1.0);
#endif
	real eps = 1e-2;
	eps = 1e-3;
	eps = 2e-2;
	eps = 1e-6;
	eps = 0.125;
	eps = 0.0625;   // 1/16
	eps = 0.03125;  // 1/32
	eps = 0.015625; // 1/64
	//eps = 1e-2;
	//eps = 1.0;
	//eps = 1000.0;
	problem_desc_impl<SPARSITY, N, M, n_r> p (x);
	solver s (num_threads);
	cout << "x0 = " << x << endl;
	create_func creat = Herbort_create_pair_list;
	//create_func creat = Goulard_create_pair_list;
	//create_func creat = GE_create_pair_list;
	//create_func creat = Goulard_GE_create_pair_list;
	s.branch_and_bound (p, eps, creat);
//#define SHOW
#if defined SHOW
	cout << "Possible: " << endl;
	for (auto &x : s.sol) {
		x.show();
	}
	cout << endl << "Guaranteed: " << endl;
	for (auto &x : s.guar_sol) {
		x.show();
	}
	std::cout << "========\n";
#endif
	s.show_results();
	s.stat_show();
	return 0;
}

