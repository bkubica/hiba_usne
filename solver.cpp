
#include <vector>
#include <cassert>

#include <xi_ari.hpp>

#include "solver.hpp"
#include "LP.hpp"
#include "statistics.hpp"
#include "scoped_lock.hpp"

#define APRX
//#define LP_PRECOND
#define INITIAL_SOBOL_EXCLUSION
#define EPSILON_INFLATION_INIT
#define SOBOL_FULL
#define COMPLEMENT_NEW

#define HIGH_PREC_NEWTON

#if defined APRX
#include <matinv_aprx.hpp>
#else
#include <matinv.hpp>
#endif

/*#if defined LP_PRECOND
#include <rev_simp.hpp>
#endif*/
#if defined HIGH_PREC_NEWTON
#include <l_interval.hpp>
#include <l_ivector.hpp>
#include <l_imatrix.hpp>
class itensor;
class sitensor;
class citensor;
class scitensor;
class l_itensor;
class rtensor;
class srtensor;
class ctensor;
class sctensor;
class l_ctensor;
#include <type_traits.hpp>
#include "high_prec_procedures.hpp"
#endif

#if defined INITIAL_SOBOL_EXCLUSION
#include "tolerable.hpp"
int complement (ivector &x, tbb::concurrent_vector<ivector> &list, tbb::concurrent_vector<ivector> &result);
int complement (ivector &x, ivector &y, tbb::concurrent_vector<ivector> &result);

double **sobol_points(unsigned N, unsigned D, char *dir_file);
#endif

#include <tbb/parallel_for.h>
#include <tbb/parallel_reduce.h>
#include <tbb/parallel_sort.h>
#include <tbb/task_scheduler_observer.h>
#include <tbb/task_arena.h>

#define BC3_INITIAL
//#define LIN_PROG
#define AUXILIARY_QUADRATIC
#define BOUND_CONS
#define NCMP
//#define RECOMPUTE_L1L2
//#define KRAWCZYK
//#define JACOBI
#define NOSUBM
//#define SWITCHING
#define SWITCHING_FULL
//#define NCMP_AND_GAUSS

#define QUADR_TYPE_HANSEN
//#define QUADR_TYPE_KUBICA

//#define LOW_LEVEL

//#define EPSILON_SWITCH 0.5
//#define EPSILON_SWITCH (1e-6*65536)

// bisection or trisection ?
#define N_SECTION 2

#define NONE 1
#define BORSUK_ONLY 2
#define PROPER 4

//#define ADVANCED_VERIF NONE
#define ADVANCED_VERIF BORSUK_ONLY
//#define ADVANCED_VERIF PROPER

statistics<thread_private/*shared*/> global_stat;

statistics<thread_private> *stats = nullptr;
//thread_local statistics<thread_shared> &stat = global_stat;
thread_local statistics<thread_private> *stat = nullptr;

#if defined LOW_LEVEL
#include <cxsc_blas.hpp>
#include <fenv.h>
#endif

#if (defined LIN_PROG||defined LP_PRECOND)
#include <glpk.h>
#endif

#if defined AUXILIARY_QUADRATIC

cxsc::intvector no_quadratic_term;

#if defined QUADR_TYPE_HANSEN
#include "hansen_quadratic_eq.hpp"
#else
#include "quadratic_eq.hpp"
#endif

#endif

cxsc::intmatrix no_term;
cxsc::intmatrix num_terms;
cxsc::intvector use_hc_narrow;

#if defined COMPLEMENT_NEW
//#include <tbb/mutex.h>
#include <tbb/spin_mutex.h>
#endif

LP_mutex LP_mut;

#if defined LOW_LEVEL
#include <limits>
#endif

#include <utility>

#include "bisection.hpp"
#include "bc.hpp"
#include "hc.hpp"
#include "bound-consistency.hpp"

#include "borsuk.hpp"
#include "top_deg.hpp"

//#define __MIC__
#if defined __MIC__
#include <sched.h>
#endif

struct stat_counter : public tbb::task_scheduler_observer {
	stat_counter() {
		observe (true);
	};

	void on_scheduler_entry(bool) {
		//std::cout << "thread stat initing\n";
		//thread_index = tbb::task_arena::current_thread_index();//threads_count.fetch_and_increment();
		stat = &stats[tbb::this_task_arena::current_thread_index()];
	};
};

//******************************

struct hc_initializer : public tbb::task_scheduler_observer {
	const problem_desc *ptr;
	hc_initializer(const problem_desc *ptr_) : ptr(ptr_) {
		observe (true);
	};

	void on_scheduler_entry(bool) {
		//std::cout << "thread hc initing\n";
		ptr->hc_init_thread();
		//ptr->tree->show();
		//std::cout << "\n";
	};
};

//******************************

class pinning_observer: public tbb::task_scheduler_observer {
    cpu_set_t *mask;
    int ncpus;

    const int pinning_step;
public:
    pinning_observer( int pinning_step=1 ) : pinning_step(pinning_step) {
        for ( ncpus = sizeof(cpu_set_t)/CHAR_BIT; ncpus < 16*1024 /* some reasonable limit */; ncpus <<= 1 ) {
	    //std::cout << "ncpus = " << ncpus << "\n";
            mask = CPU_ALLOC( ncpus );
	    //std::cout << "mask = " << mask << "\n";
            if ( !mask ) break;
            const size_t size = CPU_ALLOC_SIZE( ncpus );
            CPU_ZERO_S( size, mask );
            const int err = sched_getaffinity( 0, size, mask );
            if ( !err ) break;

            CPU_FREE( mask );
            mask = nullptr;
            if ( errno != EINVAL )  break;
        }
        if ( !mask )
            std::cout << "Warning: Failed to obtain process affinity mask. Thread affinitization is disabled.\n";
    }

void on_scheduler_entry( bool ) {
	if ( !mask ) return;

	const size_t size = CPU_ALLOC_SIZE( ncpus );
	const int num_cpus = CPU_COUNT_S( size, mask );
	int thr_idx = tbb::this_task_arena::current_thread_index();
	std::stringstream ss;
	ss.clear();
	//std::cout << "thr_idx = " << thr_idx << "\n" << "num_cpus = " << num_cpus << "\n";
	//std::cout << ss.str();
#if defined __MIC__
    thr_idx += 1; // To avoid logical thread zero for the master thread on Intel(R) Xeon Phi(tm)
#endif
    thr_idx %= num_cpus; // To limit unique number in [0; num_cpus-1] range

        // Place threads with specified step
        int cpu_idx = 0;
        for ( int i = 0, offset = 0; i<thr_idx; ++i ) {
            cpu_idx += pinning_step;
            if ( cpu_idx >= num_cpus )
                cpu_idx = ++offset;
        }

	//cpu_idx = 50 + tbb::this_task_arena::current_thread_index()%8;
	ss.clear();
	//ss << tbb::this_task_arena::current_thread_index() << ") cpu_idx = " << cpu_idx << '\n';
	std::cout << ss.str();
        // Find index of 'cpu_idx'-th bit equal to 1
        int mapped_idx = -1;
        while ( cpu_idx >= 0 ) {
            if ( CPU_ISSET_S( ++mapped_idx, size, mask ) )
                --cpu_idx;
        }

	//std::cout << tbb::this_task_arena::current_thread_index() << ") mapped_idx = " << mapped_idx << "\n";

        cpu_set_t *target_mask = CPU_ALLOC( ncpus );
        CPU_ZERO_S( size, target_mask );
        CPU_SET_S( mapped_idx, size, target_mask );
        const int err = sched_setaffinity( 0, size, target_mask );

        if ( err ) {
            std::cout << "Failed to set thread affinity!\n";
            exit( EXIT_FAILURE );
        }
	else {
            ss.clear();
	    ss << "Set thread affinity: Thread " << thr_idx << ": CPU " << mapped_idx << std::endl;
	    std::cerr << ss.str();
	}
	{
		cpu_set_t *check_mask = CPU_ALLOC(ncpus);
		int result = sched_getaffinity( 0, size, check_mask);
		if (result == -1) {
			perror ("sched_getaffinity");
			exit(-1);
		}
		for (int i = 0; i < num_cpus; ++i) {
			if (CPU_ISSET_S(i, size, check_mask)) {
				ss.clear();
				ss << tbb::this_task_arena::current_thread_index() << ") Processor " << i << " is set\n";
				std::cout << ss.str();
			}
		}
		CPU_FREE(check_mask);
	}
        CPU_FREE( target_mask );
    }

    ~pinning_observer() {
        if ( mask )
            CPU_FREE( mask );
    }
};

//******************************

class BBSolveBody {
	const problem_desc &pp;
	solver *solv_ptr;
	const real eps;
	const real eps_switch;
	bool use_bound;
	std::vector< std::pair<int, int> > &pairs_for_Ncmp;
	create_func creat;
public:
	BBSolveBody (const problem_desc &pp_, solver *solv_ptr_, const real eps_, const real eps_switch_, const bool use_bound_, std::vector< std::pair<int, int> > &pairs_for_Ncmp_, create_func creat_) : pp(pp_), solv_ptr(solv_ptr_), eps(eps_), eps_switch(eps_switch_), use_bound(use_bound_), pairs_for_Ncmp(pairs_for_Ncmp_), creat(creat_) {};

	void operator () (elem &current, tbb::feeder<elem> &feeder ) const {
		//cout << "\n\n===\ncurrent:";
		//current.show();
		//cout << "current x =" << current.x << endl;
		const problem_desc &p = pp;
		bool singul;
		const int num_variables = VecLen (p.x0);
		ivector /*x1(num_variables),*/ x2(num_variables), y(p.num_fun);
		ivector x3(num_variables);
		imatrix J;
#if defined SWITCHING || defined SWITCHING_FULL || defined NCMP_AND_GAUSS || !defined NCMP
		ivector x_sel(p.num_fun);
		rmatrix Y(p.num_fun, p.num_fun);
		intvector var_num(num_variables), eq_num(p.num_fun);
		var_num = -1;
		eq_num = -1;
		int error; // necessary ???
#if defined NOSUBM || defined JACOBI || defined KRAWCZYK
		ivector xc (num_variables), yc (p.num_fun);
		rmatrix YY (p.num_fun, p.num_fun);
#endif
#if defined LIN_PROG
#if !defined NOSUBM && !defined JACOBI && !defined KRAWCZYK
		ivector xc (num_variables), yc (p.num_fun);
#endif
		imatrix Jc;
#endif
#endif
		intvector which (num_variables), old_which (num_variables);
		ivector old_x(num_variables);
		int k, result;
		do {
			//cout << "x = " << current.x << endl;
			old_x = current.x;
			p.compute_all_grad_f (current.x, y, J); stat->num_grad_f += p.num_fun;
			//cout << "y = " << y << endl;
			//cout << "J = " << J << endl;
			if (!in(0, y)) {
				//cout << "no solution in \nx = " << current.x << endl;
				//my_delete_elem (current);
				return;
			}
			cxsc::ivector y_ineq(p.num_ineq);
			stat->num_ineq += p.compute_all_ineq (current.x, current.satisfied, y_ineq);
			//cout << "y_ineq = " << y_ineq << endl;
			int num_satisfied = compute_num_satisfied(current, y_ineq, p);
			if (num_satisfied == -1) {
				++stat->num_del_ineq;
				return;
			}
			singul = false;
#if defined SWITCHING_FULL
			//if (true) {
			//if (below_eps (current.x, 2.0) < num_variables) {
			//if (below_eps (current.x, 24.0/num_variables) < num_variables) {
			//----if (below_eps (current.x, 6.0/num_variables) < num_variables) {
			//if (below_eps (current.x, 4.0/num_variables) < num_variables) {
			//if (below_eps (current.x, 3.0/num_variables) < num_variables) {
			if (below_eps (current.x, 1.5/num_variables) < num_variables) {
			//if (below_eps (current.x, 0.5/num_variables) < num_variables) {
			//if (below_eps (current.x, 0.015625 /*1/64*/) < num_variables) {
			//if (below_eps (current.x, 0.03125 /*1/32*/) < num_variables) {
			//if (below_eps (current.x, 0.0625 /*1/16*/) < num_variables) {
				// at least 1 component larger or equal the given threshold
				//cout << "bc\n";
				which = 1;
				possibly_atomic<bool, thread_private> modified;
				//result = bc_cons_serial (p, eps, current, pairs_for_Ncmp, which); ++stat->num_bc3;
				//result = hc_cons_serial (p, eps, current, which); ++stat->num_hc;
				result = hc_bc_cons_serial (p, eps, current, which); ++stat->num_hc;
				if (result == -1) {
					//cout << "del by hc_bc\n";
					//++stat->num_del_bc;
					//++stat->num_del_hc;
					return;
				}
				//cout << "x = " << current.x << "\n";
			}
#if defined BOUND_CONS
			//if (below_eps(current.x, 1.0) <= num_variables - p.num_fun + 1) { 
			//if (below_eps(current.x, 0.5) <= num_variables - p.num_fun + 1) { 
			//if (below_eps(current.x, 0.25) <= num_variables - p.num_fun + 1) { 
			if (use_bound && (below_eps(current.x, 0.25) <= num_variables - max(1, p.num_fun/2))) { 
			//if (use_bound && (below_eps(current.x, 0.125) <= num_variables - max(1, p.num_fun/2))) { 
			//if (use_bound && (below_eps(current.x, 0.25) <= num_variables - 1)) { 
			//if (use_bound && (below_eps(current.x, 0.0625) <= num_variables - max(1, p.num_fun/2))) { 
				result = bound_cons(p, eps, current, pairs_for_Ncmp, which, 0.015625, 0.5); ++stat->num_bound_cons;
				//result = bound_cons(p, eps, current.x, pairs_for_Ncmp, which, 0.03125, 0.5); ++stat->num_bound_cons;
				if (result == -1) {
					++stat->num_del_bound_cons;
					//my_delete_elem (current);
					return;
				}
			}
#endif
			which = 1;
			//cout << "after bc\n\n";
			/*if (below_eps (current.x, 16/*32*eps_switch) <= (num_variables - p.num_fun)) {
				cout << "nothing\n";
				result = 0;
			}
			else if (below_eps (current.x, eps_switch) <= (num_variables - p.num_fun)) {*/
//#if defined NCMP_AND_GAUSS
//			if (true) {
//#else
//			if (below_eps (current.x, eps_switch) <= (num_variables - p.num_fun)) {
			if (below_eps (current.x, 2*eps_switch) <= (num_variables - p.num_fun)) {
			//if (false) {
//#endif
#endif
#if defined SWITCHING

#if defined NCMP_AND_GAUSS
			if (true) {
#else
			//if (below_eps (current.x, eps_switch) < num_variables) {
			if (below_eps (current.x, eps_switch) <= (num_variables - p.num_fun)) {
			//if (below_eps (current.x, EPSILON_SWITCH) <= (num_variables - p.num_fun)) {
			//if (below_eps (current.x, /*16384,32768,_65536_,131072*/1024*eps) <= (num_variables - p.num_fun)) {
			//if (below_eps (current.x, /*example, eps=1e-5*/8192*eps) <= (num_variables - p.num_fun)) {
			//if (below_eps (current.x, /*hippo, eps=1e-3*/2048*eps) <= (num_variables - p.num_fun)) {
			//if (below_eps (current.x, /*puma, eps=1e-4*/4096*eps) <= (num_variables - p.num_fun)) {
			//if (below_eps (current.x, /*broyden, eps=1e-6*/65536*eps) <= (num_variables - p.num_fun)) {
#endif

#endif
#if defined SWITCHING || defined SWITCHING_FULL
#if defined NCMP
#if defined RECOMPUTE_L1L2
				creat (J, pairs_for_Ncmp);
#endif
				result = use_Ncmp (p, current.x, J, pairs_for_Ncmp, x2, which, singul);
				/*if (norm (current.x, k) < eps) {
					// this is so that var_num was assigned a value for the veriffication tests...
					xc = (Inf(current.x) + Sup(current.x))/2;
					p.compute_all_f (xc, yc); stat->num_f += p.num_fun;
					++stat->num_precond;
					error = compute_preconditioner (J, current.x, Y, x_sel, var_num, eq_num);
				}*/
#elif defined LIN_PROG
				xc = (Inf(current.x) + Sup(current.x))/2;
				p.compute_all_f (xc, yc); stat->num_f += p.num_fun;
				Jc = (Inf(J) + Sup(J))/2;
				result = LP_narrowing (current.x, y, J, xc, yc, Jc);
				if (norm (current.x, k) < eps) {
					p.compute_all_f (current.x, y); stat->num_f += p.num_fun;
					if (!in(0, y)) {
						//my_delete_elem (current);
						return;
					}
				}
#endif
#if defined NCMP_AND_GAUSS
				//processing of the result of use_Ncmp (or LP_narrowing)
				if (result == -1) return;
				if (result == 3) {
					++stat->num_bis_newt;
					feeder.add (elem(std::move(x2), intvector(current.satisfied)));
					continue;
				}
#endif
			}
#if defined NCMP_AND_GAUSS
			{
				which = 1;
#else
			else {
#endif

#if defined NOSUBM || defined JACOBI || defined KRAWCZYK

				xc = (Inf(current.x) + Sup(current.x))/2;
				p.compute_all_f (xc, yc); stat->num_f += p.num_fun;
				//p.compute_all_ineq (xc, current.satisfied, yc); stat->num_f += p.num_fun;
#if defined LP_PRECOND
				error = compute_LP_CW_preconditioner (J, current.x, YY, var_num, eq_num);
				++stat->num_precond;
				//cout << "YY1 = " << YY << endl;
#else
				error = compute_preconditioner (J, current.x, Y, x_sel, var_num, eq_num);
				++stat->num_precond;
				//cout << "J = " << J << "\n";
				//cout << "Y = " << Y << "\n";
				//std::terminate();
				permute_preconditioner (Y, var_num, eq_num, YY);
#endif

#if defined NOSUBM
				result = use_nosubm_GS (p, current, J, yc, var_num, eq_num, YY, x2, which, singul);
#elif defined KRAWCZYK
				result = use_Krawczyk (p, current.x, J, yc, var_num, eq_num, YY, x2, which);
#else
				result = use_Jacobi (p, current.x, J, yc,var_num,eq_num,YY,x2,which,singul);
#endif


#else
				/*error = compute_preconditioner (J, current.x, Y, A_sel, x_sel,
									var_num, eq_num); ++stat->num_precond;
				result = use_GS (p, current.x, x_sel, A_sel, var_num, Y, x2, which);*/
				result = 0;
#endif
			}
#else // not switching

#if defined LIN_PROG
				xc = (Inf(current.x) + Sup(current.x))/2;
				//p.compute_all_grad_f (xc, yc, Jc); stat->num_grad_f += p.num_fun;
				p.compute_all_f (xc, yc); stat->num_f += p.num_fun;
				Jc = (Inf(J) + Sup(J))/2;
				{
					//LP_mutex::scoped_lock sc_lock(LP_mut);
					result = LP_narrowing (current.x, y, J, xc, yc, Jc);
				}
				if (norm (current.x, k) < eps) {
					p.compute_all_f (current.x, y);
					if (!in(0, y)) {
						//my_delete_elem (current);
						return;
					}
				}
#elif defined NCMP

#if defined RECOMPUTE_L1L2
			creat (J, pairs_for_Ncmp);
#endif
			result = use_Ncmp (p, current.x, J, pairs_for_Ncmp, x2, which, singul);
#else

#if (defined NOSUBM || defined JACOBI || defined KRAWCZYK)
			xc = (Inf(current.x) + Sup(current.x))/2;
			p.compute_all_f (xc, yc); stat->num_f += p.num_fun;
#if defined LP_PRECOND
			error = compute_LP_CW_preconditioner (J, current.x, YY, var_num, eq_num); ++stat->num_precond;
			//cout << "YY2 = " << YY << endl;
#else
			error = compute_preconditioner (J, current.x, Y, x_sel, var_num, eq_num); ++stat->num_precond;
			permute_preconditioner (Y, var_num, eq_num, YY);
#endif
			//YY = Y;
#if defined NOSUBM
			//result = use_nosubm_GS (p, current, J, yc, var_num, eq_num, YY, x2, which, singul, num_satisfied);
			result = use_nosubm_GS (p, current, J, yc, var_num, eq_num, YY, x2, which, singul);
#elif defined KRAWCZYK
			result = use_Krawczyk (p, current.x, J, yc, var_num, eq_num, YY, x2, which);
#else
			result = use_Jacobi (p, current.x, J, yc, var_num, eq_num, YY, x2, which, singul);
#endif

#else
			/*error = compute_preconditioner (J, current.x, Y, A_sel, x_sel, var_num, eq_num); ++stat->num_precond;
			result = use_GS (p, current.x, x_sel, A_sel, var_num, Y, x2, which);*/
			//cout << "a";
			result = 0;
#endif
#endif

#endif
			//cout << "result = " << result << endl;
			//cout << "teraz x = " << current.x << endl;
#if defined AUXILIARY_QUADRATIC
			int k;
			//if ((result == 0)&&(norm(current.x, k) >= 1.0/num_variables)) {
			//if ((result == 0)&&(below_eps (current.x, 0.5) <= (num_variables - p.num_fun))) {
			//if ((result == 0)&&(below_eps (current.x, 0.2) <= (num_variables - p.num_fun))) {
			//if ((result == 0)&&(below_eps (current.x, 0.1) <= (num_variables - p.num_fun))) {
			//if ((result == 0)&&(below_eps (current.x, 1.5/num_variables) <= (num_variables - p.num_fun))) {
			//---if ((result == 0)&&(below_eps (current.x, 2.5/(2 + num_variables)) <= (num_variables - p.num_fun))) {
			//----if ((result == 0)&&(singul||(below_eps (current.x, 2.5/(2 + num_variables)) <= (num_variables - p.num_fun)))) {
			//if ((result == 0)&&(below_eps (current.x, 2.4/num_variables) <= (num_variables - p.num_fun))) {
			//if ((result == 0)&&(below_eps (current.x, 0.2) >= p.num_fun)) {
			//int below = 0;
			//if ((result == 0)&&(below_eps (current.x, 1.5/num_variables) <= (num_variables - p.num_fun))&&(below_eps (current.x, 1.0) >= num_variables)) {
			//if ((result == 0)&&(below_eps (current.x, 1.5/num_variables) <= (num_variables - p.num_fun))&&(below_eps (current.x, 1.0) >= p.num_fun)) {
			//if ((result == 0)&&(below_eps (current.x, 2.5/(2 + num_variables)) <= (num_variables - p.num_fun))&&(below_eps (current.x, 2.0) >= /*num_variables*/p.num_fun)) {
			//----if ((result == 0)&&(below_eps (current.x, 2.5/(2 + num_variables)) <= (num_variables - p.num_fun))&&(below_eps (current.x, max(16.0/num_variables, 1.0)) >= /*num_variables*/p.num_fun)) {
			//----if ((result == 0)&&(singul||((below_eps (current.x, 2.5/(2 + num_variables)) <= (num_variables - p.num_fun))&&(below_eps (current.x, max(16.0/num_variables, 1.0)) >= /*num_variables*/p.num_fun)))) {
			if ((result == 0)&&(((singul||below_eps (current.x, 2.5/(2 + num_variables)) <= (num_variables - p.num_fun))&&(below_eps (current.x, max(16.0/num_variables, 1.0)) >= /*num_variables*/p.num_fun)))) {
			//-notbad-if ((result == 0)&&((singul)&&(below_eps (current.x, max(16.0/num_variables, 1.0)) >= /*num_variables*/p.num_fun)&&(below_eps(current.x, 0.25) >= (/*p.num_fun-1*/max(num_variables-3, 1))))) {
			//--now-optimal--if (singul&&(result == 0)&&(below_eps (current.x, max(16.0/num_variables, 1.0)) >= /*num_variables*/p.num_fun)&&/*(below_eps(current.x, 0.125) >= max(p.num_fun/4, 1))&&*/(below_eps(current.x, 0.25) >= max(/*max(num_variables-3,*/ p.num_fun*0.7/*)*/, 1))) {
			//-hopeless-if ((result == 0)&&((singul)&&(below_eps (current.x, max(16.0/num_variables, 1.0)) >= /*num_variables*/p.num_fun))) {
			//--++--++if ((result == 0)&&(((singul||below_eps (current.x, 10.0/(2 + num_variables)) <= (num_variables - p.num_fun))&&(below_eps (current.x, 8.0/num_variables) >= num_variables/*p.num_fun*/)))) {
			//if ((result == 0)&&((singul&&(below_eps (current.x, 5.0/(2 + num_variables)) <= (num_variables - p.num_fun)))||((below_eps (current.x, 2.5/(2 + num_variables)) <= (num_variables - p.num_fun))&&(below_eps (current.x, max(16.0/num_variables, 1.0)) >= /*num_variables*/p.num_fun)))) {
			//-if ((result == 0)&&((singul&&(below_eps (current.x, max(64.0/num_variables, 1.0)) >= p.num_fun))||((below_eps (current.x, 2.5/(2 + num_variables)) <= (num_variables - p.num_fun))&&(below_eps (current.x, max(16.0/num_variables, 1.0)) >= /*num_variables*/p.num_fun)))) {
			//-+-+if ((result == 0)&&((singul&&(below_eps (current.x, max(16.0/num_variables, 1.0)) >= max(p.num_fun/4, 1)))||((below_eps (current.x, 2.5/(2 + num_variables)) <= (num_variables - p.num_fun))&&(below_eps (current.x, max(16.0/num_variables, 1.0)) >= /*num_variables*/p.num_fun)))) {
			//if ((result == 0)&&(below_eps (current.x, 2.5/(2 + num_variables)) <= (num_variables - p.num_fun))&&(norm(current.x, k) <= 2.0)) {
			//if (result == 0) {
			//---if((result == 0)&&(!sufficiently_reduced(current.x, old_x))&&(singul||((below_eps (current.x, 2.5/(2 + num_variables)) <= (num_variables - p.num_fun))&&(below_eps (current.x, max(16.0/num_variables, 1.0)) >= /*num_variables*/p.num_fun)))) {
			//--if((result == 0)&&(!sufficiently_reduced(current.x, old_x))&&(singul||((below_eps (current.x, 2.5/(2 + num_variables)) <= (num_variables - p.num_fun))))) {
				//cout << "auxiliary quadratic for x = " << current.x << endl;
				int s = 0;
				for (int k = 1; k <= num_variables; ++k) s += which[k];
				if (/*p.num_fun/4 +*/ s >= num_variables /*- 2/*p.num_fun/4*/) {
					for (int k = 1; k <= p.num_fun; ++k) {
						if (no_quadratic_term[k]) continue;
						//if (no_quadratic_term[k]||(!which[k])) continue;
						/*bool makes_sense = false;
						for (int kk = 1; kk <= num_variables; ++kk) {
							if (diam(J[k][kk]) > 1.0 && diam(current.x[kk]) > 0.05)
						       	{
							       	makes_sense = true;
								kk = num_variables + 1;
							}
						}
						if (makes_sense) {
							result = use_quadratic (p, k, current.x, x2, which);
							if ((result == -1)||(result == 3)) k = p.num_fun*2;
						}*/
#if defined QUADR_TYPE_HANSEN
						result = use_quadratic_hansen (p, k, current.x,x2,x3,which);
#else
						result = use_quadratic (p, k, current.x, x2, which);
#endif
						//cout << "\tquadratic result = " << result << endl;
						if ((result == -1)||(result == 3)) k = p.num_fun*2;
					}
					if (result == 1) {
						int s = 0;
						for (int k = 1; k <= num_variables; ++k)s += which[k];
						if (p.num_fun + s <= num_variables) result = 1;
						else result = 0;
					}
				}
				//cout << "\tresult = " << result << endl;
				/*if (result == 3) {
					cout << "x = " << current.x << endl;
					cout << "x2 = " << x2 << endl;
				}*/
			}
#endif
			//cout << "result = " << result << endl;
			if (result == -1) {
				++stat->num_del_newt;
				//cout << "disjoint\n";
				return;
			}
			if (result == 1) {
				//cout << "result == 1 for x = ";// << current.x << endl;
				//current.show();
				//if (check_borsuk(p, current.x, var_num) != 1) cout << "OJEJ!!!\n";
				narrow_verified_solution (p, eps, current, which, old_which);
				//current.show();
				stat->num_ineq += p.compute_all_ineq(current.x, current.satisfied, y_ineq);
				num_satisfied = compute_num_satisfied(current, y_ineq, p);
				if (num_satisfied >= p.num_ineq) {
					solv_ptr->guar_sol.push_back (elem_guaranteed(std::move(current.x),
								      std::move(current.satisfied),
								      std::move(old_which)));
					//cout << "to verified list\n";
					return;
				}
				else if (num_satisfied == -1) {
					++stat->num_del_ineq;
					return;
				}
				// else try adding to `possible' solutions
			}
			else if (result == 3) {
				++stat->num_bis_newt;
				feeder.add (elem(std::move(x2), intvector(current.satisfied)));
				continue;
			}
			else if (result == 5) {
				feeder.add (elem(std::move(x2), intvector(current.satisfied)));
				feeder.add (elem(std::move(x3), intvector(current.satisfied)));
				continue;
			}
			// the box neither verified nor subdivided by tests
			// subdivide or store as a `possible' solution
			if (norm (current.x, k) < eps) {
#if defined HIGH_PREC_NEWTON
				if (p.num_fun > 0) {
					//cxsc::l_ivector long_x{current.x};
					cxsc::rvector c = (Inf(current.x) + Sup(current.x))*0.5;
					//cxsc::l_ivector long_xc{c};
					//cxsc::l_ivector long_yc;
					cxsc::l_ivector long_xc{c}, long_yc;
		                        p.compute_all_f_high_prec (long_xc, long_yc); //stat->num_f += p.num_fun;
					cxsc::l_ivector long_x{current.x}, long_y;
					cxsc::l_imatrix long_J;
					p.compute_all_grad_f_high_prec (long_x, long_y, long_J);
					//cout << "long_y = " << long_y << "\n";
					//cout << "long_J = " << long_J << "\n";
					//long_yc = yc;
					//cxsc::l_imatrix long_J{J};
					//cxsc::l_interval long_x2;
					cxsc::imatrix J(ColLen(long_J), RowLen(long_J));
					for (int i = 1; i <= ColLen(long_J); ++i) {
						for (int j = 1; j <= RowLen(long_J); ++j) {
							J[i][j] = long_J[i][j];
						}
					}
					rmatrix Y(p.num_fun, p.num_fun);
					rmatrix YY(p.num_fun, p.num_fun);
					error = compute_preconditioner (J, current.x, Y, x_sel, var_num, eq_num); ++stat->num_precond;
					permute_preconditioner (Y, var_num, eq_num, YY);
					++stat->num_high_prec;
					result = use_nosubm_GS<cxsc::l_interval> (p, current.x, long_J, long_yc, var_num, eq_num, YY, x2, which, singul);
					if (result == -1) {
						//std::cout << "REMOVED by high prec!!!\n";
						++stat->num_del_high_prec;
						return;
					}
				}
#endif
				/*var_num = cxsc::intvector(p.num_fun);
				int j = 0;
				for (int i = 1; i <= VecLen(old_which); ++i) {
					if (old_which[i] == 0) var_num[++j] = i;
				}*/
				/*xc = (Inf(current.x) + Sup(current.x))/2;
				p.compute_all_f (xc, yc); stat->num_f += p.num_fun;
				++stat->num_precond;
				error = compute_preconditioner (J, current.x, Y, x_sel, var_num, eq_num);*/
				//bool res = create_sufficient_sign_covering(p, current.x);
				//std::cout << "res = " << res << "\n";
				if (p.num_fun == 2) {
#if (ADVANCED_VERIF != NONE)

#if (ADVANCED_VERIF == PROPER)
					if (check_system_of_two_equations(p, current.x, eps/2) == 1) {
						//std::cout << "Hurraaaaaaaaaaaaaa!!!!!\n";
						solv_ptr->advanced_guar_sol.push_back(
							elem_guaranteed(std::move(current.x), std::move(current.satisfied), std::move(old_which))
						);
					}
					else
#elif (ADVANCED_VERIF == BORSUK_ONLY)
					if (check_borsuk_parallel(p, current.x) == 1) {
						//std::cout << "A jednak !!!\n";
						solv_ptr->advanced_guar_sol.push_back(
							elem_guaranteed(std::move(current.x), std::move(current.satisfied), std::move(old_which))
						);
					}
					else
#endif

#endif
						solv_ptr->sol.push_back (std::move(current));
				}
				else if (p.num_fun /*- num_satisfied*/ < num_variables) {
#if (ADVANCED_VERIF != NONE)
					//std::cout << "plum\n";
					if (var_num[1] == -1) {
						//----std::cout << "No var_num!!!!\n";
						solv_ptr->sol.push_back (std::move(current));
						return;
					}
					which = 1;
					for (int ii = 1; ii <= p.num_fun; ++ii) which[var_num[ii]] = 0;
					{
						std::stringstream ss;
						//ss << "var_num = " << var_num << "\n";
						//ss << "which = " << which << "\n";
						std::cout << ss.str();
					}
					int td;
					if ((td = compute_topol_degree_underdet(p, current.x, which, eps)) != 0) {
						{
							std::stringstream ss;
							ss << "top_deg = " << td << "\n";
							std::cout << ss.str();
						}
						//std::cout << "Verified by top_deg!!!\n";
						solv_ptr->advanced_guar_sol.push_back(
							elem_guaranteed(std::move(current.x), std::move(current.satisfied), std::move(old_which))
						);
					}
					/*else if (check_borsuk_parallel(p, current.x) == 1) {
						solv_ptr->advanced_guar_sol.push_back(
							elem_guaranteed(std::move(current.x), std::move(old_which))
						);
					}*/
					else
#endif
						//std::cout << "Possible: " << current.x << "\n";
						solv_ptr->sol.push_back (std::move(current));
				}
				else {
#if (ADVANCED_VERIF != NONE)
					//else if (p.num_fun >= num_variables) {
					cxsc::ivector xx {current.x};
					for (int i = 1; i <= num_variables;++i) xx[i] += cxsc::interval(-1e-6, 1e-6);
					int td;
					//int res = create_sufficient_sign_covering(p, xx);
					//std::cout << "res = " << res << "\n";
#if (ADVANCED_VERIF == BORSUK_ONLY)
					if (check_borsuk_parallel(p, xx) == 1) {
						//std::cout << "infl: Borsuk verified\n";
						solv_ptr->advanced_guar_sol.push_back(
							elem_guaranteed(std::move(xx), std::move(current.satisfied), std::move(old_which))
						);
					}
					else
#elif (ADVANCED_VERIF == PROPER)
					if ((td = compute_topol_degree(p, xx, eps)) != 0) {
						{
							std::stringstream ss;
							if (td != 1 && td != -1)
								ss << "infl: top_deg = " << td << "\n";
							std::cout << ss.str();
						}
						//std::cout << "Verified by top_deg!!!\n";
						solv_ptr->advanced_guar_sol.push_back(
							elem_guaranteed(std::move(xx), std::move(current.satisfied), std::move(old_which))
						);
					}
					else
#endif

#endif
						//std::cout << "Possible: " << current.x << "\n";
						solv_ptr->sol.push_back (std::move(current));
				}
				return;
			}
box_subdiv:
			//cout << "bisection\n";
			int i0;
			//k = MaxSmear (J, diam(current.x), num_variables, p.num_fun, i0);
			//k = MaxSmearDiam (J, diam(current.x), num_variables, p.num_fun, i0);
			//k = MaxSumMagnitude (J, diam(current.x), num_variables, p.num_fun);
			//k = MinSumMagnitude (J, diam(current.x), num_variables, p.num_fun);
			//k = ZeroNearBound (y, J, diam(current.x), num_variables, p.num_fun);
			//k = heuristic_sonic (y, J, diam(current.x), num_variables, p.num_fun, eps);
			//k = heuristic (J, diam(current.x), num_variables, p.num_fun, eps);
			//k = heuristic_extended (J, diam(current.x), which, num_variables, p.num_fun, eps);
			//k = heuristic_norm (J, diam(current.x), which, num_variables, p.num_fun, eps);
			//k = heuristic_norm_impr (J, diam(current.x), which, num_variables, p.num_fun, eps);
			//----k = heuristic_norm_3 (J, diam(current.x), which, num_variables, p.num_fun, eps);
			//---k = heuristic_norm_4 (J, diam(current.x), which, num_variables, p.num_fun, eps);
			k = heuristic_norm_8 (J, diam(current.x), which, num_variables, p.num_fun, eps);
			//--k = heuristic_norm_7 (J, diam(current.x), which, num_variables, p.num_fun, eps);
			//k = heuristic_norm_6 (J, diam(current.x), which, num_variables, p.num_fun, eps);
			//k = heuristic_norm_5 (J, diam(current.x), which, num_variables, p.num_fun, eps);
			//-if (below_eps(current.x, 0.5/*0.5*/) <= (num_variables - p.num_fun/2)) k = MaxSmear (J, diam(current.x), num_variables, p.num_fun, i0);
			//if (below_eps(current.x, 0.125) <= p.num_fun/2) k = MaxSmear (J, diam(current.x), num_variables, p.num_fun, i0);
			//-else k = heuristic_norm_3 (J, diam(current.x), which, num_variables, p.num_fun, eps);
			//cout << "k = " << k << endl;
			/*if (!which[k]) {
				cout << "x = " << current.x << endl;
				cout << "k = " << k << endl;
			}*/
#if (N_SECTION == 2)
			x2 = current.x;
			real c = (Inf(current.x[k]) + Sup(current.x[k]))*0.5;///2;
			//real c = (Inf(current.x[k])*0.55 + Sup(current.x[k])*0.45);
			SetSup(current.x[k], c);
			SetInf(x2[k], c);
			++stat->num_bisec; ++stat->num_bis_2;
			feeder.add (elem(std::move(x2), intvector(current.satisfied)));
#elif (N_SECTION == 3)
			x2 = x3 = current.x;
			real c1 = 2.0*Inf(current.x[k])/3 + Sup(current.x[k])/3;
			real c2 = Inf(current.x[k])/3 + 2.0*Sup(current.x[k])/3;
			SetSup(x2[k], c1);
			current.x[k] = interval (c1, c2);
			SetInf(x3[k], c2);
			++stat->num_bisec; ++stat->num_bis_3;
			//current.x = x2;
			feeder.add (elem(std::move(x2), intvector(current.satisfied)));
			feeder.add (elem(std::move(x3), intvector(current.satisfied)));
#elif (N_SECTION == -25)
			real c = (Inf(current.x[k]) + Sup(current.x[k]))/2;
			xc = c;
			//xc = (Inf(current.x) + Sup(current.x))/2;
			//cout << "xc = " << xc << "\n";
			p.compute_all_f (xc, yc); stat->num_f += p.num_fun;
			//cout << "yc = " << yc << "\n";
			bool lower = true;
			//int kkk;
			for (int kk = 1; kk <= p.num_fun; ++kk) {
				//cout << "kk = " << kk << "\n";
				if (AbsMax(yc[kk]) > 1e-4/*1e-2/*1e-12*/) {
					lower = false;
					break;
				}
			}
			//cout << "lower = " << lower << endl;
			if (lower/*norm(yc, kkk) <= 1e-4*/) {
				//cout << "trisection of x = " << current.x << endl;
				//abort();
				//cout << "xc = " << xc << "\n";
				//cout << "yc = " << yc << "\n";
				x2 = x3 = current.x;
				real c1 = 2.0*Inf(current.x[k])/3 + Sup(current.x[k])/3;
				real c2 = Inf(current.x[k])/3 + 2.0*Sup(current.x[k])/3;
				SetSup(x2[k], c1);
				current.x[k] = interval (c1, c2);
				SetInf(x3[k], c2);
				feeder.add (elem(std::move(x2), intvector(current.satisfied)));//my_new_elem (x2));
				feeder.add (elem(std::move(x3), intvector(current.satisfied)));//my_new_elem (x3));
				++stat->num_bis_3;
			}
			else {
				x2 = current.x;
				SetSup(current.x[k], c);
				SetInf(x2[k], c);
				feeder.add (elem(std::move(x2), intvector(current.satisfied)));
				++stat->num_bis_2;
			}
			//cout << "after\nx = " << current.x << endl;
			++stat->num_bisec;
#else
		cout << "not implemented (yet ?)\n";
		exit (-1);
#endif
		} while (true);
	}
};


//******************************

#if defined INITIAL_SOBOL_EXCLUSION

#define EPS_SOBOL_EXCL 1e-4

#if defined COMPLEMENT_NEW
tbb::spin_mutex excl_mut;
#endif

class SobolTolBody {
#if defined COMPLEMENT_NEW
	std::vector<ivector> *excl_boxes;
#else
	tbb::concurrent_vector<ivector> *excl_boxes;
#endif
	tbb::concurrent_vector<elem_guaranteed> *solutions;
	const problem_desc &p;
	ivector x_bound, x_gen; //bounds for the solutions and for region, where seeds are generated
	imatrix J, J_ineq;
	double **Sobol;
public:
	void operator () (const tbb::blocked_range<int> &r) const {
		for (int i = r.begin(), i_end = r.end(); i != i_end; ++i) {
			//cout << "i = " << i << endl;
			cxsc::ivector x0 (VecLen(p.x0));
			for (int j = 1; j <= VecLen(x0); ++j) {
				//x0[j] = interval (Inf(p.x0[j]) + (diam(p.x0[j]) * Sobol[i][j-1]));
				x0[j] = interval (Inf(x_gen[j]) + (diam(x_gen[j]) * Sobol[i][j-1]));
			}
			//cout << "for exclusion: x0 = " << x0 << endl;
			cxsc::interval y0;
			cxsc::interval rhs (0.0);
			bool approx_sol = false;
			int eq_num = -1;
#if defined SOBOL_FULL
			ivector x_excl, x_excl_cur;
			bool no_x_excl = true;
#endif
			if (p.num_fun <= 0) {
				approx_sol = true;
				goto consider_ineq;
			}
#if (!defined SOBOL_FULL)
			eq_num = 1 + i % p.num_fun;
#else
			eq_num = 1;
#endif
			//cout << "eq_num = " << eq_num << endl;

#if (!defined SOBOL_FULL)
			p.compute_f (x0, eq_num, y0); ++stat->num_f;// += p.num_fun;
#else
			for (;;) {
				p.compute_f (x0, eq_num, y0); ++stat->num_f;// += p.num_fun;
				if (Sup(y0) < -EPS_SOBOL_EXCL || Inf(y0) > EPS_SOBOL_EXCL) break;
				if (++eq_num >= p.num_fun) {
					approx_sol = true;
					break;
				}
			}
consider_ineq:
			int ineq_num = -1;
			if (approx_sol) {
				// looks, like it is an approximate solution,
				// but are the inequality constraints satisfied as well???
				for (ineq_num = 1; ineq_num <= p.num_ineq; ++ineq_num) {
					p.compute_ineq (x0, ineq_num, y0); ++stat->num_ineq;
					if (Sup(y0) < Inf(p.rhs[ineq_num]) - EPS_SOBOL_EXCL || Inf(y0) > Sup(p.rhs[ineq_num]) + EPS_SOBOL_EXCL) {
						approx_sol = false;
						rhs = p.rhs[ineq_num];
						break;
					}
				}
			}
			if (approx_sol) {
				// also all inequality constraints are approximately satisfied; looks like it was an approximate solution
				// hence nothing to cut from the search domain
				//cout << "approximate solution: x = " << x0 << endl;
				continue;
			}
#endif
			real yy0;
			//interval b;
			if (Sup(y0) < Inf(rhs) - EPS_SOBOL_EXCL) {
				//b = interval (-Infinity, -Inf(y0) - EPS_SOBOL_EXCL); //Inf or Sup????????
				yy0 = -Sup(y0) + Inf(rhs) - EPS_SOBOL_EXCL;
			}
			else if (Inf(y0) > Sup(rhs) + EPS_SOBOL_EXCL) {
				//b = interval (-Sup(y0) + EPS_SOBOL_EXCL, Infinity);
				yy0 = Inf(y0) - Sup(rhs) - EPS_SOBOL_EXCL;
			}
			else continue;

			//cout << "yy0 = " << yy0 << "\n";
			ivector a = (ineq_num == -1)?J[eq_num]:J_ineq[ineq_num];
			//cout << "a = " << a << "\n";
			fesetround (FE_DOWNWARD);
			real sum = 0.0;
			for (int i = 1; i <= VecLen(a); ++i) sum += AbsMax(a[i]);
			real rho = abs(yy0/sum);
			ivector x = x0;
			fesetround (FE_TONEAREST);
			for (int i = 1; i <= VecLen(x); ++i) x[i] += interval(-rho, rho);
			//cout << "interior x = " << x << endl;
#if defined EPSILON_INFLATION_INIT
			// epsilon-inflation
#if defined SOBOL_FULL
			bool x_satisfied_previous_eq = true;
#endif
			for (;;) {
				/*ivector x_new = ivector ((Inf(x)+Sup(x))/2);
				for (int i = 1; i <= VecLen(x); ++i) {
					real delta = diam (x[i])*0.55;
					x_new[i] += interval(-delta, delta);
				}*/
				rvector xc = (Inf(x)+Sup(x))/2;
				ivector x_new = ivector (xc);
				for (int i = 1; i <= VecLen(x); ++i) {
					real delta = diam (x[i])*0.55;
					/*if (Inf(x[i]) > Inf(x_bound[i])) SetInf(x_new[i], xc[i] - delta);
					else SetInf(x_new[i], Inf(x_bound[i]));
					if (Sup(x[i]) < Sup(x_bound[i])) SetSup(x_new[i], xc[i] + delta);
					else SetSup(x_new[i], Sup(x_bound[i]));*/
					SetInf(x_new[i], max(xc[i] - delta, Inf(x_bound[i])));
					SetSup(x_new[i], min(xc[i] + delta, Sup(x_bound[i])));
					//x_new[i] += interval(-delta, delta);
				}
				//if (x_new == x) cout << "ojejkujej\n";
				if (ineq_num == -1) {
					p.compute_f (x_new, eq_num, y0); ++stat->num_f;
				}
				else {
					p.compute_ineq (x_new, ineq_num, y0); ++stat->num_ineq;
				}
				//cout << "y0 = " << y0 << endl;
				if ((x_new != x)&&((Inf(y0) > Sup(rhs) + EPS_SOBOL_EXCL)||(Sup(y0) < Inf(rhs) - EPS_SOBOL_EXCL))) {
					x = x_new;
#if defined SOBOL_FULL
					x_satisfied_previous_eq = true;
#endif
				}
#if (!defined SOBOL_FULL)
				else break;
#else
				else {
					if (x_satisfied_previous_eq){
						x_satisfied_previous_eq = false;
						//cout << " eq_num = " << eq_num << endl;
						//cout << " x = " << x << endl;
						x_excl_cur = x;
						for (int i = 1; i <= VecLen(x); ++i) {
							if (no_term[eq_num][i]) x_excl_cur[i] = p.x0[i];
						}
						//cout << " x_excl_cur = " << x_excl_cur << endl;
						if (no_x_excl || Lebesgue(x_excl_cur) > Lebesgue(x_excl)) {
							x_excl = x_excl_cur;
							no_x_excl = false;
						}
					}
					if (++eq_num > p.num_fun) break;
				}
				
#endif
			}
#else
			x_excl = x;
#endif

#if !defined SOBOL_FULL
			for (int i = 1; i <= VecLen(x); ++i) {
				if (no_term[eq_num][i]) x[i] = p.x0[i];
			}
#endif

#if defined SOBOL_FULL
#define EXCLUDED_BOX x_excl
#else
//			static_assert(false, "Badly organized Sobol exclusion code!\n");
#define EXCLUDED_BOX x
#endif
#if defined COMPLEMENT_NEW
			{
				//tbb::spin_mutex::scoped_lock excl_lock(excl_mut);
				scoped_lock_of<tbb::spin_mutex> excl_lock{excl_mut};
				excl_boxes->push_back (EXCLUDED_BOX);
			}
#else
			excl_boxes->push_back (EXCLUDED_BOX);
#endif
		}
	}
#undef EXCLUDED_BOX

#if defined COMPLEMENT_NEW
	SobolTolBody (std::vector<ivector> *excl_boxes_, tbb::concurrent_vector<elem_guaranteed> *solutions_, const problem_desc &p_, const ivector &x_bound_, const ivector x_gen_, imatrix &J_, imatrix &J_ineq_, double **Sobol_) :
		excl_boxes(excl_boxes_), solutions(solutions_), p(p_), x_bound(x_bound_), x_gen(x_gen_), J(J_), J_ineq(J_ineq_), Sobol(Sobol_) {}
#else
	SobolTolBody (tbb::concurrent_vector<ivector> *excl_boxes_, tbb::concurrent_vector<elem_guaranteed> *solutions_, const problem_desc &p_, const ivector &x_bound_, const ivector x_gen_, imatrix &J_, imatrix &J_ineq_, double **Sobol_) :
		excl_boxes(excl_boxes_), solutions(solutions_), p(p_), x_bound(x_bound_), x_gen(x_gen_), J(J_), J_ineq(J_ineq_), Sobol(Sobol_) {}
#endif
};


#endif

#if (defined INITIAL_SOBOL_EXCLUSION && defined COMPLEMENT_NEW)

struct CompletionBodyData {
	ivector y;
	std::vector<ivector> excl_boxes;
	CompletionBodyData (const ivector &y_, const std::vector<ivector> &excl_boxes_) : y(y_), excl_boxes(excl_boxes_) {};
};

class CompletionBody {
	tbb::concurrent_vector<ivector> *results;
	std::atomic<bool> *too_many_boxes;
public:
	void operator () (CompletionBodyData &data, tbb::feeder<CompletionBodyData>& feeder ) const {
		//cout << "y = " << data.y << endl;
		//cout << "excluding: " << data.excl_boxes.size() << " boxes\n";
		//size_t i = 0, i_Leb_max = -1;
		int i = -1, i_Leb_max = -1;
		ivector x(VecLen(data.y));
		real Leb_max (0.0);
		// choose the excluded box, having the largest intersection with y
		while (++i < data.excl_boxes.size()) {
			if (Disjoint(data.y, data.excl_boxes[i])) {
				//cout << "disjoint\n";
				//cout << "\ti_Leb_max = " << i_Leb_max << endl;
				if ((i + 1) >= data.excl_boxes.size()) break; // another condition????
				data.excl_boxes[i] = data.excl_boxes[data.excl_boxes.size() - 1];
				data.excl_boxes.pop_back();
				continue;
			}
			x = data.y & data.excl_boxes[i];
			real Leb_cur;
			if ((Leb_cur = Lebesgue(x)) > Leb_max) {
				Leb_max = Leb_cur;
				i_Leb_max = i;
			}
			//cout << "Leb_cur = " << Leb_cur << "\n";
		}
		//cout << "i_Leb_max = " << i_Leb_max << endl;
		//if (i_Leb_max != -1) cout << "excluding: " << data.excl_boxes[i_Leb_max] << "\n";
		if (i_Leb_max == -1) {
			results->push_back(data.y);
			return;
		}

		tbb::concurrent_vector<ivector> boxes;
		boxes.clear();
		complement (data.excl_boxes[i_Leb_max], data.y, boxes);
		if (data.excl_boxes.size() <= 1 || (*too_many_boxes)) {
			for (i = 0; i < boxes.size(); ++i) results->push_back(boxes[i]);
			//for (tbb::concurrent_vector<ivector>::const_iterator iter = boxes.begin(); iter != boxes.end(); ++iter) results->push_back(*iter);
		}
		else {
			data.excl_boxes[i_Leb_max] = data.excl_boxes[data.excl_boxes.size() - 1];
			data.excl_boxes.pop_back();
			for (i = 0; i < boxes.size(); ++i) {
				//cout << "created box: " << boxes[i] << "\n";
				feeder.add(CompletionBodyData(boxes[i], data.excl_boxes));
			}
		}
		if (results->size() >= 1024) *too_many_boxes = true;
		//if (results->size() >= 256) *too_many_boxes = true;
		//if (results->size() >= 128) *too_many_boxes = true;
	}
	CompletionBody (tbb::concurrent_vector<ivector> *results_, std::atomic<bool> *too_many_boxes_) : results(results_),
			too_many_boxes(too_many_boxes_) {};
};

#endif

bool compare (const ivector &x, const ivector &y) {
	//return x > y;
	//int i;
	//return (norm(x, i) > norm(y, i));
	//elem e(x);
	real l1 = Lebesgue (x);
	//e = elem(y);
	real l2 = Lebesgue (y);
	return (l1 > l2);
	//return (Lebesgue(x) < Lebesgue(y));
}



void solver::compute_stats() const {
	//for (int i = 0; i < num_threads; ++i) global_stat.add(stats[i]);//stats[i].show();
	const statistics<thread_private> initial_value;
	global_stat.add(
		tbb::parallel_reduce (blocked_range<int>(0, num_threads), initial_value,
			[&](const blocked_range<int> &r, statistics<thread_private> st)->statistics<thread_private> {
				for (int i = r.begin(), i_end = r.end(); i != i_end; ++i) {
					st.add(stats[i]);
				}
				return st;
			},
			[](const statistics<thread_private> &x, const statistics<thread_private> &y) -> statistics<thread_private> {
				return x + y;
			}
		)
	);

}


void solver::stat_show() const {
	compute_stats();
	global_stat.show();
}


#if defined INITIAL_SOBOL_EXCLUSION

void solver::exclude_Sobol_boxes (const problem_desc &p, const elem &e, tbb::concurrent_vector<elem> &box_set) {
	const ivector &x {e.x};
#if defined COMPLEMENT_NEW
	std::vector<ivector> excluded_boxes;
#else
	tbb::concurrent_vector<ivector> excluded_boxes;
#endif
	ivector y, y_ineq;
	imatrix J, J_ineq;
	p.compute_all_grad_f (x, y, J); global_stat.num_grad_f += p.num_fun;
	global_stat.num_grad_ineq += p.compute_all_grad_ineq (e.x, e.satisfied, y, J_ineq);
	excluded_boxes.clear();

//#define SOBOL_NUM_POINTS (VecLen(p.x0) + 1)
//----#define SOBOL_NUM_POINTS ((sparsity*sparsity*sparsity*2048)/(p.num_fun*p.num_fun*p.num_fun*VecLen(p.x0)*VecLen(p.x0)))
//#define SOBOL_NUM_POINTS (min(VecLen(p.x0)*VecLen(p.x0)*8, 10000))
//-+#define SOBOL_NUM_POINTS (VecLen(p.x0)*sparsity*sparsity/(p.num_fun*p.num_fun))
#define SOBOL_NUM_POINTS (VecLen(p.x0)*VecLen(p.x0))
//-#define SOBOL_NUM_POINTS (VecLen(p.x0)*4)
//---#define SOBOL_NUM_POINTS (VecLen(p.x0)*3)
//--#define SOBOL_NUM_POINTS (VecLen(p.x0)*2)
//-#define SOBOL_NUM_POINTS VecLen(p.x0)
//#define SOBOL_NUM_POINTS max(VecLen(p.x0)/2, 2)
//#define SOBOL_NUM_POINTS max(VecLen(p.x0)/4, 2)
//-#define SOBOL_NUM_POINTS max(sparsity/p.num_fun, 2)
//--#define SOBOL_NUM_POINTS max((int)sqrt(sparsity*VecLen(p.x0)*1.0/p.num_fun), 2)
	char sobol_params_file [20];
	strcpy (sobol_params_file, "new-joe-kuo-6.21201");
	double **Sobol = sobol_points (SOBOL_NUM_POINTS, VecLen(p.x0), sobol_params_file);
	rvector x_diam = diam(x);
	ivector x_gen (VecLen(x_diam));
	for (int i = 1; i <= VecLen(x_diam); ++i) {
		x_gen[i] = interval(Inf(x[i]) + x_diam[i]/10.0, Sup(x[i]) - x_diam[i]/10.0);
	}
	cout << "x_gen = " << x_gen << endl;
	cout << "Leb(x_gen) = " << Lebesgue(x_gen) << endl;
	cout << "Leb(x) = " << Lebesgue(x) << endl;
	cout << "Leb(x0)/Leb(x) = " << Lebesgue(p.x0)/Lebesgue(x) << endl;
	cout << "Leb(x0)-Leb(x) = " << Lebesgue(p.x0) - Lebesgue(x) << endl;
	//cout << "stat.num_bc3 = " << stat.num_bc3 << endl;
	//cout << "stat.num_bc3rev = " << stat.num_bc3rev << endl;
	tbb::parallel_for (tbb::blocked_range<int> (0, SOBOL_NUM_POINTS),
			   SobolTolBody (&excluded_boxes, &guar_sol, p, x, x/*_gen*/, J, J_ineq, Sobol)/*, ap**/);
	for (int i = 0; i < SOBOL_NUM_POINTS; ++i) delete [] Sobol[i];
	delete [] Sobol;

	cout << "Excluded_boxes (size = " << excluded_boxes.size() << ")\n";
	//cout << "e.g., x_excl = " << excluded_boxes[0] << "\n";
	tbb::parallel_sort (excluded_boxes.begin(), excluded_boxes.end(), compare); cout<<"sorted\n";
	/*for (size_t i = 0, len = excluded_boxes.size(); i < len; ++i) {
		cout << "x = " << excluded_boxes[i] << endl;
	}*/
#if defined COMPLEMENT_NEW
	tbb::concurrent_vector<CompletionBodyData> boxes;
	tbb::concurrent_vector<ivector> boxes1;
	boxes.clear();
	boxes.push_back(CompletionBodyData(x, excluded_boxes));
	std::atomic<bool> too_many_boxes;
	too_many_boxes = false;
	tbb::parallel_for_each(boxes.begin(), boxes.end(), CompletionBody(/*excluded_boxes,*/ &boxes1, &too_many_boxes));
#else
	tbb::parallel_sort (excluded_boxes.begin(), excluded_boxes.end(), compare); cout<<"sorted\n";
	//tbb::parallel_sort (excluded_boxes.begin(), excluded_boxes.end(), std::greater<cxsc::ivector>);
	tbb::concurrent_vector<ivector> boxes1, boxes2;
	boxes1.clear();
	boxes1.push_back (x);
	for (size_t i = 0, len = excluded_boxes.size(); i < len; ++i) {
		boxes2.clear();
		complement (excluded_boxes[i], boxes1, boxes2);
		boxes1 = boxes2;
		//cout << "iteracja: " << i << ", usuwanie: x = " << excluded_boxes[i] << endl;
		//for (size_t iii = 0; iii < boxes1.size(); ++iii) cout << "y = " << boxes1[iii]<<endl;
	}
#endif
	for (size_t i = 0, len = boxes1.size(); i < len; ++i) {
		box_set.push_back (elem(boxes1[i], e.satisfied));
	}
	cout << "Box_set (size = " << box_set.size() << ")\n";
	/*for (size_t i = 0, len = box_set.size(); i < len; ++i) {
		box_set[i].show();
	}*/
}

#endif


int solver::branch_and_bound (const problem_desc &p, const real &eps, create_func creat){
	stats = new statistics<thread_private> [num_threads];
	static stat_counter counter;
	static hc_initializer hc_init(&p);
#if defined __MIC__
	//static pinning_observer pinner(4);
	//pinner.observe(true);
#endif
	tbb::concurrent_vector<elem> box_set;
	//cout << "Poczatek" << endl;
	cout << "Liczba watkow: " << num_threads << endl;
	cout << "rhs = " << p.rhs << endl;
	imatrix J(p.num_fun, VecLen(p.x0));
#if defined LIN_PROG || defined LP_PRECOND
	glp_term_out (GLP_OFF);
#endif
	box_set.clear();
	ivector x = p.x0;
#if defined AUXILIARY_QUADRATIC
	no_quadratic_term = std::move(intvector(p.num_fun));
	no_quadratic_term = 1;
#endif
	no_term = intmatrix (p.num_fun, VecLen(x));
	no_term = 0;
	int sparsity = 0;
	{
		interval y;
		ivector g;
#if defined AUXILIARY_QUADRATIC
		imatrix H;
		for (int i = 1; i <= p.num_fun; ++i) {
			p.compute_hess_f (x, i, y, g, H); ++global_stat.num_hess_f;
			J[i] = g;
			for (int j = 1; j <= VecLen(g); ++j) {
				if (H[j][j] != 0.0) {
					no_quadratic_term[i] = 0;
					break;
				}
			}
			for (int j = 1; j <= VecLen(g); ++j) {
				no_term[i][j] = (g[j] == 0.0);
				sparsity += !!(g[j] != 0.0);
			}
		}
#else
		for (int i = 1; i <= p.num_fun; ++i) {
			p.compute_grad_f (x, i, y, g); ++global_stat.num_grad_f;
			J[i] = g;
			for (int j = 1; j <= VecLen(g); ++j) {
				no_term[i][j] = (g[j] == 0.0);
				sparsity += !!(g[j] != 0.0);
			}
		}
#endif
	}
	num_terms = p.count_variable_occurrences_in_equations();
	use_hc_narrow = cxsc::intvector(p.num_fun);
	for(int i = 1; i <= p.num_fun; ++i) {
		use_hc_narrow[i] = 0;
		for (int j = 1; j <= VecLen(x); ++j) {
			if (num_terms[i][j] == 1) use_hc_narrow[i] = 1;
		}
	}
	std::cout << "num_terms = " << num_terms << "\n";
	//std::cout << "no_term = " << no_term << "\n";
	std::cout << "sparsity " << sparsity << ", " << sparsity*1.0/p.num_fun/VecLen(x) << "\n";
	std::cout << "use_hc_narrow = " << use_hc_narrow << "\n";
	real sparsity_deg = sparsity*1.0/p.num_fun/VecLen(x);
	//cout << sparsity/p.num_fun << endl;
	//cout << "sqrt(sparsity): " << sqrt(sparsity*1.0/p.num_fun/VecLen(x)) << ", " << VecLen(x)*sqrt(sparsity*1.0/p.num_fun/VecLen(x)) << endl;

	std::vector< std::pair<int, int> > pairs_for_Ncmp;
	creat(J, pairs_for_Ncmp);

	/*{
		if (check_system_of_two_equations(p, x) == 1) {
			std::cout << "Yesss!\n";
		}
	}*/
	/*int td = compute_topol_degree(p, x, eps);
	{
		std::stringstream ss;
		ss << "Whole domain: top_deg = " << td << "\n";
		std::cout << ss.str();
	}
	//std::cout << "Verified by top_deg!!!\n";
	std::terminate();*/

	intvector satisfied(p.num_ineq);
	if (p.num_ineq > 0) satisfied = 0;
	elem e(x, satisfied);
#if defined BC3_INITIAL
	//ivector x = p.x0;
	intvector which (VecLen(x));
	which = 1;
	//int result = hc_cons_serial (p, eps, e, which); ++stat->num_hc;
	int result = hc_bc_cons_serial (p, eps, e, which); ++stat->num_hc;
	//int result = bc_cons_serial(p, eps, e, pairs_for_Ncmp, which); ++global_stat.num_bc3;
	//int result = bc_cons_parallel(p, eps, e, pairs_for_Ncmp, which); ++global_stat.num_bc3;
	if (result == -1) {
		++global_stat.num_del_bc;
		return 0;
	}
	cout << "x = " << e.x << endl;
#if defined BOUND_CONS
	//result = bound_cons(&p, eps, x, pairs_for_Ncmp, L2, which, 0.0625, 1.5); ++stat->num_bound_cons;
	//if (sparsity_deg >= 0.1) {
	if (sparsity_deg >= 0.05) {
		//result = bound_cons(p, eps, x, pairs_for_Ncmp, which, 0.015625, 0.5); ++stat->num_bound_cons;
		result = bound_cons(p, eps, e, pairs_for_Ncmp, which, 0.0625, 1.5); ++global_stat.num_bound_cons;
	}
	if (result == -1) return 0;
	cout << "after enforcing bound cons.: x = " << e.x << endl;
	//exit(0);
#endif
	//box_set.push_back (my_new_elem(x));
#endif

#if defined INITIAL_SOBOL_EXCLUSION
	//cout << "a\n";
	exclude_Sobol_boxes(p, e, box_set);
	//cout << "b\n";
#else
	box_set.push_back (std::move(e));
#endif
	cout << "parallel_for_each\n";
	parallel_for_each(box_set.begin(), box_set.end(), BBSolveBody(p, this, eps, 0.5/VecLen(p.x0), (sparsity_deg >= 0.1), pairs_for_Ncmp, creat));
#if defined __MIC__
	//pinner.observe(false);
#endif
	counter.observe(false);
	hc_init.observe(false);
	return 0;
}



int use_Ncmp (const problem_desc &p, ivector &x, const imatrix &Jac, 
	      const std::vector< std::pair<int, int> > &pairs_for_Ncmp, ivector &x2, intvector &which, bool &singul) {
	//cout << "use_Ncmp\n";
	if (pairs_for_Ncmp.size() == 0) {
		// no equations, only inequality constraints
		return 1;
	}
	int eq_num, var_num;
	interval yc;
	ivector xc = x;
	real c;
	which = 1;
	//for (int i = 0; i < pairs_for_Ncmp.size(); ++i) {
	for (std::pair<int, int> eq_var : pairs_for_Ncmp) {
		eq_num = eq_var.first;
		var_num = eq_var.second;
		//cout << "eq_num = " << eq_num << endl;
		//cout << "var_num = " << var_num << endl;
		xc[var_num] = c = (Inf(x[var_num]) + Sup(x[var_num]))/2;
		p.compute_f (xc, eq_num, yc); ++stat->num_f;
		if (!in(0.0, Jac[eq_num][var_num])) {//continue;
			//interval x_new = xc[var_num] - (yc - p.rhs[eq_num])/Jac[eq_num][var_num];
			interval x_new = xc[var_num] - yc/Jac[eq_num][var_num];
			if (Disjoint(x_new, x[var_num])) {
				//cout << "rozlaczne\n";
				//stat->num_del_newt++;
				return -1;
			}
			if (in(x_new, x[var_num])) {
				x[var_num] = x_new;
				//which[var_num] = 0;
			}
			else {
				x[var_num] &= x_new;
			}
		}
		else {
			if (in(0.0, yc)) { xc[var_num] = x[var_num]; singul = true;  continue; } // bo byloby 0/0...
			//xinterval extend = c - (yc - p.rhs[eq_num]) % Jac[eq_num][var_num];
			xinterval extend = c - yc % Jac[eq_num][var_num];
			//cout << "obliczono extend\n";
			ivector v = x[var_num] & extend;
			if (v[1] == EmptyIntval()) {
				//stat->num_del_newt++;
				return -1;
			}
			else if (v[2] != EmptyIntval()) {
				{
				//if (Inf(v[2]) - Sup(v[1]) >= 1e-3) {
				//if (Sup(v[1]) < Inf(v[2])) {
					x[var_num] = v[1];
					x2 = x;
					x2[var_num] = v[2];
					//cout << "x1 = " << x << endl;
					//cout << "x2 = " << x2 << endl;
					return 3;
				}
				
			}
			else {
				x[var_num] = v[1];
			}
		}
		xc[var_num] = x[var_num];
	}
	/*int suma = 0;
	for (int i = 1; i <= VecLen(which); ++i) suma += which[i];
	if (p.num_fun + suma <= VecLen(which)) return 1;
	//cout << "x = " << x << endl;
	//cout << "which = " << which << endl;*/
	return 0;
}


int use_nosubm_GS (const problem_desc &p, elem &e, imatrix A, ivector b, const intvector &var_num,
		   const intvector &eq_num, const rmatrix &Y, ivector &x2, intvector &which, bool &singul) {//,
		   //const int num_satisfied) {
	ivector &x = e.x;
	/*cout << "use_nosubm_GS\n";
	//cout << " x = " << x << endl;
	cout << " var_num = " << var_num << endl;
	cout << " eq_num = " << eq_num << endl;
	cout << " which = " << which << endl;
	cout << " A = " << A << endl;
	cout << " Y = " << Y << endl;*/
	if (VecLen(b) <= 0) {
		// no equations, only inequality constraints
		return 1;
	}
	//if (num_satisfied >= p.num_fun) cout << "Heyyy!!!";
	int i = 1, j, rows_num = ColLen(A), cols_num = RowLen(A);
	rvector xc = (Inf(x) + Sup(x))/2;
	ivector v = x - xc;
	interval x_new;
	xinterval extend;
	ivector vec(2);
	int variable, equation;
	A = Y*A;//left_mult (Y, A);
	b = Y*b;//left_mult (Y, b);
	//ivector xxc (x), yyc;
	which = 1;
	//cout << "A = " << A << endl;
	//cout << "b = " << b << endl;
	//cout << "v = " << v << endl;
	do {
		//cout << "i = " << i << endl;
		variable = var_num[i];
		equation = eq_num[i];
		/*cout << "variable = " << variable << endl;
		cout << "eq = " << equation << endl;
		cout << "A[ij]*v[j] = " << A[equation][1]*v[1] << endl;
		cout << "b[j]+A[ij]*v[j] = " << b[1]+A[equation][1]*v[1] << endl;*/
		interval sum = b[equation];
		//interval sum = b[equation] - p.rhs[equation];
		for (j = 1; j < variable; ++j) sum += A[equation][j]*v[j];
		for (j = variable + 1; j <= cols_num; ++j) sum += A[equation][j]*v[j];
		//if (norm(x, j) <= 1e-1) { //j unused
		/*if (1){//below_eps(x, 1e-1) >= (cols_num - rows_num)) { //lower or higher ???
			for (j = 1; j < variable; j++) sum += A[equation][j]*v[j];
			for (j = variable + 1; j <= cols_num; j++) sum += A[equation][j]*v[j];
		}
		else {
			for (int ii = 1; ii <= rows_num; ii++) xxc[var_num[ii]] = xc[var_num[ii]];
			p.compute_all_f (xxc, yyc); //stat->num_f += rows_num;
			left_mult (Y, yyc);
			sum = yyc[equation];
			for (j = 1; j <= rows_num; j++)
				if (var_num[j] != variable) sum += A[equation][var_num[j]]*v[var_num[j]];
		}*/
		/*interval sum (yyc[equation]);
		for (j = 1; j <= rows_num; j++) if (var_num[j] != variable) sum += A[equation][var_num[j]]*v[var_num[j]];*/
		//sum = yyc[equation];
		//cout << sum  << " == " << yyc[equation] << endl;//+ A[1][3 - variable]*v[3 - variable] << endl;
		if (!in (0.0, A[equation][variable])) {
			//cout << "notin\n";
			x_new = xc[variable] - sum/A[equation][variable];
			//x_new = xc[variable] - yyc[equation]/A[equation][variable];
			//cout << x_new << " == " << xc[variable] - yyc[equation]/A[equation][variable] << endl;
			if (Disjoint(x[variable], x_new)) {
				//stat->num_del_newt++;
				return -1;
			}
			if (in(x_new, x[variable])) {
				//if (p.is_eq[equation]) which[variable] = 0;
				which[variable] = 0;
				//if (p.rhs[equation] == 0.0) which[variable] = 0;
				x[variable] = x_new;
			}
			else x[variable] &= x_new;
			v[variable] = x[variable] - xc[variable];
		}
		else {
			//cout << "in\n";
			if (in (0.0, sum)) {singul = true; /*cout << "singul GS\n";*/ continue; }
			extend = xc[variable] - sum % A[equation][variable];
			vec = x[variable] & extend;
			//cout << "vec = " << vec << endl;
			if (vec[1] == EmptyIntval()) {
				//stat->num_del_newt++;
				return -1;
			}
			else if (vec[2] != EmptyIntval()) {
				x[variable] = vec[1];
				//modify_x (x, x_sel, var_num);
				x2 = x;
				x2[variable] = vec[2];
				//cout << "wynik 3\n";
				return 3;
			}
			else {
				x[variable] = vec[1];
				v[variable] = x[variable] - xc[variable];
			}
		}
	//} while (++i <= rows_num - num_satisfied);
	} while (++i <= rows_num);
	int s = 0;
	for (i = 1; i <= VecLen(which); ++i) s += which[i];
	//if (p.num_fun + s <= VecLen(which) + num_satisfied) return 1;
	if (p.num_fun + s <= VecLen(which)) return 1;
	return 0;
}


int use_Krawczyk (const problem_desc &p, ivector &x, imatrix A, ivector b, const intvector &var_num,
		  const intvector &eq_num, const rmatrix &Y, ivector &x2, intvector &which) {
	//cout << "use_Krawczyk\n";
	int rows_num = ColLen(A), cols_num = RowLen(A);
	rvector xc = (Inf(x) + Sup(x))/2;
	ivector v = x - xc;
	//ivector x_new = x + Y*(b + A*v);
	//ivector delta_x = Y*(b + A*v);
	//ivector delta_x = Y*b + (Y*A + Id(Y))*v;
	b -= p.rhs;
	rmatrix Id_sel (rows_num, cols_num);
	Id_sel = 0.0;
	for (int i = 1; i <= rows_num; ++i) Id_sel[eq_num[i]][var_num[i]] = 1.0;
	ivector delta_x = (Id_sel - Y*A)*v - Y*b;
	//cout << "x = " << x << endl;
	//cout << "Id_sel = " << Id_sel << endl;
	//cout << "delta_x = " << delta_x << endl;
	//cout << "x_new = " << x_new << endl;
	//cout << "Y*(b + A*v) = " << Y*(b + A*v) << endl;
	which = 1;
	//cout << "which = " << which << endl;
	for (int i = 1; i <= rows_num; ++i) {
		//cout << "i = " << i << endl;
		int variable = var_num[i];
		int equation = eq_num[i];
		//cout << "var = " << variable << endl;
		//interval x_new = x[variable] + delta_x[equation];
		interval x_new = xc[variable] + delta_x[equation];
		//cout << "x_new = " << x_new << endl;
		if (Disjoint (x[variable], x_new)) return -1;
		else if (in(x_new, x[variable])) {
			which[variable] = 0;
			x[variable] = x_new;
		}
		else x[variable] &= x_new;
	}
	int s = 0;
	for (int i = 1; i <= VecLen(which); ++i) s += which[i];
	if (p.num_fun + s <= VecLen(which)) return 1;
	return 0;
}


int use_Jacobi (const problem_desc &p, ivector &x, imatrix A, ivector b, const intvector &var_num,
		const intvector &eq_num, const rmatrix &Y, ivector &x2, intvector &which, bool &singul) {
	//cout << "use_Jacobi\n";
	int rows_num = ColLen(A), cols_num = RowLen(A);
	rvector xc = (Inf(x) + Sup(x))/2;
	ivector v = x - xc;
	//cout << " v = " << v << endl;
	ivector x_new;
	//xinterval extend;
	//ivector vec(2);
	//int variable, equation;
	b -= p.rhs;
#if defined LOW_LEVEL
	double *DY = (double *)Y.to_blas_array();
	double *Amid = new double[rows_num*cols_num];
	double *Arad = new double[rows_num*cols_num];
	double *Aabs = new double[rows_num*cols_num];
	double *Amid_1 = new double[rows_num*cols_num];
	int rnd = fegetround();
	const double eta = numeric_limits<double>::denorm_min();
	const double realmin = numeric_limits<double>::min();
	const double eps = eta/realmin/2;//numeric_limits<double>::min();
	cout << "eta = " << eta << ", eps = " << eps << ", realmin = " << realmin << endl;
	exit(0);
	fesetround (FE_UPWARD);
	int ind = 0;
	for (int i = 1; i <= rows_num; ++i)
		for (int j = 1; j <= cols_num; ++j) {
			Arad[ind] = _double((Sup(A[i][j]) - Inf(A[i][j]))/2);
			Amid[ind] = _double((Inf(A[i][j]) + Sup(A[i][j]))/2);
			Aabs[ind] = _double(abs(Sup(A[i][j])));
			++ind;
		}
	cblas_dgemm (CblasRowMajor, CblasNoTrans, CblasNoTrans, rows_num, cols_num, rows_num, 1.0, DY, rows_num, Amid, cols_num, 0.0, Amid_1, cols_num);
	fesetround (rnd);
#endif
	A = Y*A;//left_mult (Y, A);
	b = Y*b;//left_mult (Y, b);
	ivector d (rows_num);
	int variable, equation;
	for (int i = 1; i <= rows_num; ++i) {
		//cout << "i = " << i << endl;
		variable = var_num[i];
		equation = eq_num[i];
		//cout << "variable = " << variable << endl;
		//cout << "eq = " << equation << endl;
		d[equation] = A[equation][variable];
		A[equation][variable] = 0.0;
	}
	//cout << "d = " << d << endl;
	x_new = A*v + b;
	which = 1;
	for (int i = 1; i <= rows_num; ++i) {
		//cout << "i = " << i << endl;
		variable = var_num[i];
		equation = eq_num[i];
		//cout << "variable = " << variable << endl;
		//cout << "eq = " << equation << endl;
		//cout << "\ti = " << i << endl;
		if (!in (0.0, d[equation])) {
			x_new[equation] /= d[equation];
			x_new[equation] = xc[variable] - x_new[equation];
			if (Disjoint (x[variable], x_new[equation])) {
				return -1;
			}
			if (in(x_new[equation], x[variable])) {
				which[variable] = 0;
				x[variable] = x_new[equation];
			}
			else x[variable] &= x_new[equation];
		}
		else {
			if (in (0.0, x_new[equation])) { singul = true; continue; }
			xinterval extend = xc[variable] - x_new[equation] % d[equation];
			ivector vec = x[variable] & extend;
			if (vec[1] == EmptyIntval()) {
				return -1;
			}
			else if (vec[2] != EmptyIntval()) {
				x[variable] = vec[1];
				x2 = x;
				x2[variable] = vec[2];
				return 3;
			}
			else x[variable] = vec[1];
		}
	}
	int s = 0;
	for (int i = 1; i <= VecLen(which); ++i) s += which[i];
	if (p.num_fun + s <= VecLen(which)) return 1;
	return 0;
}


int compute_preconditioner (const imatrix &A, const ivector &x, rmatrix &Y, 
			    ivector &x_selected, intvector &col_num, intvector &row_num) {
	//cout << "precond\n";
	//cout << "A = " << A << endl;
	//rmatrix V = mid(A);
	rmatrix V = (Inf(A) + Sup(A))*0.5;
	int rows_num = ColLen(A), cols_num = RowLen(A);
	rmatrix V_selected (rows_num, rows_num);
	//cout << "before intvectors\n";
	col_num = intvector (cols_num);
	row_num = intvector (rows_num);
	for (int i = 1; i <= cols_num; ++i) if (diam(x[i]) <= 1e-16) Col(V, i) = 0.0;
	//std::cout << "V = " << V << "\n";
	//cout << "GaussElim\n";
	GaussElim (V, row_num, col_num);
	int i = 1;
	do {
		Col(V_selected, i) = Col(V, col_num[i]);
		x_selected[i] = x[col_num[i]];
	} while (++i <= rows_num);
	//cout << "V_selected = " << V_selected << endl;
	//cout << "A_selected = " << A_selected << endl;
	//cout << "x_selected = " << x_selected << endl;
	int error;
#if !defined APRX
	MatInv (V_selected, Y, error);
#else
	MatInvAprx (V_selected, Y, error);
#endif
	/*cout << "error: " << error << endl;
	cout << "Y = " << Y << endl;*/
	if (error) Y = Id(V_selected);
	/*if (error) cout << "popr Y = " << Y << endl;
	if (error) cout << "ilocz = " << Y*A_selected << endl;*/
	//cout << "end precond\n";
	return error;
}


#if defined LP_PRECOND
int compute_LP_CW_preconditioner (const imatrix &A,  const ivector &x, rmatrix &Y,
				  intvector &col_num, intvector &row_num) {
	//cout << "compute_LP_CW_preconditioner\n";
	int cols_num = VecLen(x), rows_num = ColLen(A);
	int var_num_xx = 2*rows_num + 2*cols_num - 2;
	rmatrix midA = (Inf(A) + Sup(A))/2;
	//rmatrix AA (cols_num, var_num_xx);
	rvector xx(var_num_xx), /*bb(cols_num),*/ cc(var_num_xx);
	Y = rmatrix (rows_num, rows_num);
	col_num = intvector (cols_num);
	row_num = intvector (rows_num);
	GaussElim (midA, row_num, col_num);
	/*for (int i = 1; i <= rows_num; ++i) {
		row_num[i] = col_num[i] = i;
	}*/
	glp_prob *prob;
	{
		//LP_mutex::scoped_lock sc_lock(LP_mut);
		scoped_lock_of<LP_mutex> sc_lock{LP_mut};
		prob = glp_create_prob();
		glp_add_rows (prob, cols_num);
		glp_add_cols (prob, var_num_xx);
	}
	double *A_row = new double [var_num_xx + 1];
	int *ind_row = new int [var_num_xx + 1];
	for (int num = 1; num <= var_num_xx; ++num) ind_row[num] = num;
	for (int num = 1; num <= rows_num; ++num) {
		int variable = col_num[num];
		int equation = row_num[num];
		//cout << "num = " << num << endl;
		//cout << "variable = " <<variable << endl;
		//cout << "equation = " << equation << endl;
		//ind_row[i] = i;
		int j;
		for (j = 1; j < variable; ++j) {
			//cout << "\t1)j = " << j << endl;
			for (int ii = 1; ii <= rows_num; ++ii) {
				A_row[ii] = _double(midA[ii][j]);
				A_row[rows_num + ii] = _double(-midA[ii][j]);
			}
			for (int ii = rows_num + 1; ii <= var_num_xx; ++ii) A_row[ii] = 0.0;
			A_row[2*rows_num + j] = -1.0;
			A_row[2*rows_num + cols_num - 1 + j] = 1.0;
			glp_set_mat_row (prob, j, var_num_xx, ind_row, A_row);
			glp_set_row_bnds (prob, j, GLP_FX, 0.0, 0.0);
		}
		int j1 = variable;
		for (j = variable + 1; j < cols_num; ++j, ++j1) {
			//cout << "\t2)j = " << j << endl;
			for (int ii = 1; ii <= rows_num; ++ii) {
				A_row[ii] = _double(midA[ii][j]);
				A_row[rows_num + ii] = _double(-midA[ii][j]);
			}
			for (int ii = rows_num + 1; ii <= var_num_xx; ++ii) A_row[ii] = 0.0;
			A_row[2*rows_num + j1] = -1.0;
			A_row[2*rows_num + cols_num - 1 + j1] = 1.0;
			glp_set_mat_row (prob, j1, var_num_xx, ind_row, A_row);
			glp_set_row_bnds (prob, j1, GLP_FX, 0.0, 0.0);
		}
		for (int ii = 1; ii <= rows_num; ++ii) {
			A_row[ii] = _double(Inf(A[ii][variable]));
			A_row[rows_num + ii] = _double(-Sup(A[ii][variable]));
		}
		for (int ii = 2*rows_num + 1; ii <=var_num_xx; ++ii) A_row[ii] = 0.0;
		//cout << "final row\n";
		glp_set_mat_row (prob, cols_num, var_num_xx, ind_row, A_row);
		glp_set_row_bnds (prob, cols_num, GLP_FX, 1.0, 1.0);
		//cout << "AA = " << AA << endl;
		/*bb = 0.0;
		bb[cols_num] = 1.0;
		cout << "bb = " << bb << endl;*/
		/*for (int ii = 1; ii <= rows_num; ++ii) {
			real c (0.0);
			for (j = 1; j <= cols_num; ++j)
				if (j != i) c -= diam(x[j])*Inf(A[ii][j]);*/
		cc = 0.0;
		for (j = 1; j < variable; ++j) {
			//cout << "\t3)j = " << j << endl;
			real w = diam(x[j]);
			for (int ii = 1; ii <= rows_num; ++ii) {
				//real w = diam(x[ii]);
				cc[j] -= w*Inf(A[ii][j]);
				cc[rows_num + j] += w*Sup(A[ii][j]);
				cc[2*rows_num + j] = w;
			}
		}
		for (j = variable + 1; j <= cols_num; ++j) {
			//cout << "\t4)j = " << j << endl;
			real w = diam(x[j]);
			for (int ii = 1; ii <= rows_num; ++ii) {
				cc[j] -= w*Inf(A[ii][j]);
				cc[rows_num + j] += w*Sup(A[ii][j]);
				cc[2*rows_num + j] = w;
			}
		}
		for (j = 1; j <= var_num_xx; ++j) {
			glp_set_col_bnds (prob, j, GLP_LO, 0.0, 0.0);
			glp_set_obj_coef (prob, j, _double(cc[j]));
		}
		//cout << "cc = " << cc << endl;
		int err;
		//real vmin, vmax, z_;
		{
			//LP_mutex::scoped_lock sc_lock(LP_mut);
			scoped_lock_of<LP_mutex> sc_lock{LP_mut};
			glp_set_obj_dir (prob, GLP_MIN);
			glp_adv_basis (prob, 0);
			err = glp_simplex (prob, nullptr);
			for (j = 1; j <= var_num_xx; ++j) {
				xx[j] = glp_get_col_prim (prob, j);
			}
			//cout << "xx = " << xx << endl;
			//z_ = glp_get_obj_val (prob);
		}
		/*intvector B_start_vector;
		real z_;
		RevSimplex (AA, bb, cc, xx, B_start_vector, z_, err);*/
		if (!err) {
			for (j = 1; j <= rows_num; ++j)
				Y[equation][j] = xx[j] - xx[rows_num + j];
			//cout << "Y[equation] = " << Y[equation] << endl;
		}
		else {
			cout << "err = " << err << endl;
			Y = Id(Y);
			break;
		}
	}
	{
		//LP_mutex::scoped_lock sc_lock(LP_mut);
		scoped_lock_of<LP_mutex> sc_lock{LP_mut};
		glp_delete_prob (prob);
	}
	//cout << "koniec compute_LP_CW_preconditioner\n";
	//cout << "Y = " << Y << endl;
	return 0;
}
#endif


void permute_preconditioner (const rmatrix &Y, const intvector &col_num, const intvector &row_num, rmatrix &YY) {
	const int rows_num = ColLen (Y);
	int i = 1;//, j;
	do {
		YY[row_num[i]] = Y[i];
		/*j = 1;
		do YY[row_num[i]][col_num[i]] = Y[i][j];
		while (++j <= rows_num);*/
	} while (++i <= rows_num);
	//cout << "po permute\n";
}


int narrow_verified_solution (const problem_desc &p, const real &eps, elem &e, intvector &which, intvector &old_which) {
	ivector &x {e.x};
	old_which = which;
	int result = 1;
	ivector y, x2, xc, yc, x_sel(p.num_fun);
	imatrix J;
	rmatrix Y(p.num_fun, p.num_fun), YY(p.num_fun, p.num_fun);
	intvector var_num, eq_num;
	bool singul = false;
	int narrowed = 0;
	int k;
	/*int num_satisfied = 0;
	for (int i = 1; i <= VecLen(e.satisfied); ++i) {
		if (e.satisfied[i] == 1) ++num_satisfied; //???
	}*/
	while ((result == 1)&&(norm_subvec(x, which, k) >= eps)) {
		old_which = which;
		//p.compute_all_grad_f (x, e.satisfied, y, J); stat->num_grad_f+=p.num_fun;
		p.compute_all_grad_f (x, y, J); stat->num_grad_f+=p.num_fun;
		//old_x = x;

		xc = (Inf(x) + Sup(x))/2;
		//p.compute_all_f (xc, e.satisfied, yc); stat->num_f += p.num_fun;
		p.compute_all_f (xc, yc); stat->num_f += p.num_fun;
#if defined LP_PRECOND
		int error = compute_LP_CW_preconditioner (J, x, YY, var_num, eq_num);
		++stat->num_precond;
		//cout << "YY3 = " << YY << endl;
#else
		int error = compute_preconditioner (J, x, Y, x_sel, var_num, eq_num);
		++stat->num_precond;
		permute_preconditioner (Y, var_num, eq_num, YY);
#endif

#if defined NOSUBM
		//result = use_nosubm_GS (p, e, J, yc, var_num, eq_num, YY, x2, which, singul, num_satisfied);
		result = use_nosubm_GS (p, e, J, yc, var_num, eq_num, YY, x2, which, singul);
#elif defined KRAWCZYK
		result = use_Krawczyk (p, x, J, yc, var_num, eq_num, YY, x2, which);
#elif defined JACOBI
		result = use_Jacobi (p, x, J, yc, var_num, eq_num, YY, x2, which, singul);
#else
		/*error = compute_preconditioner (J, x, Y, A_sel, x_sel, var_num, eq_num);
		++stat->num_precond;
		result = use_GS (p, x, x_sel, A_sel, var_num, Y, x2, which);*/
		cout << "nothing\n";
		result = 0;
#endif
		narrowed |= result;
		//if (old_x == x) break;
	}
	return narrowed;
}


int process_verified_box (const problem_desc &p, ivector &x, intvector &which, std::vector< std::pair<int, int> > &old_pairs_for_Ncmp) {
	int vars_num = VecLen(x), num_pairs = 0;
	int total_pairs_num = old_pairs_for_Ncmp.size();
	std::vector< std::pair<int, int> > pairs_for_Ncmp;
	pairs_for_Ncmp.clear();
	int i = 0;
	do {
		if (which(old_pairs_for_Ncmp[i].second)) {
			pairs_for_Ncmp.push_back(old_pairs_for_Ncmp[num_pairs++]);
		}
	} while (++i < total_pairs_num);
	ivector y;
	imatrix J;
	p.compute_all_grad_f (x, y, J); stat->num_grad_f += p.num_fun;
	return 0;
}



real norm (const ivector &x, int &n) {
	n = 1;
	real y = diam (x[1]), yy;
	for (int i = 1; i <= VecLen(x); ++i) if ((yy = diam(x[i])) > y) {
		y = yy;
		n = i;
	}
	return y;		
}

real norm_subvec (const ivector &x, const intvector &which, int &n) {
	real y = 0.0, yy;
	int i = 1;
	do {
		if ((!which[i])&&((yy = diam(x[i])) > y)) {
			y = yy;
			n = i;
		}
	} while (++i <= VecLen(x));
	return y;
}

int below_eps (const ivector &x, const real &eps) {
	int count = 0, i = 1;
	do if (diam(x[i]) < eps) count++;
	while (++i <= VecLen(x));
	return count;
}

/*inline int contained_in (const interval &x, const interval &y) {
	if (Sup(x) > Sup(y) || Inf(x) < Inf(y)) return 0;
	return 1;
}*/


int compute_num_satisfied (elem &e, const ivector &y_ineq, const problem_desc &p) {
	int num_satisfied = 0;
	for (int i = 1; i <= p.num_ineq; ++i) {
		if (e.satisfied[i] == 1) ++num_satisfied;
		else if (Disjoint(y_ineq[i], p.rhs[i])) {
			//cout << "no feasible point in\nx = " << current.x << endl;
			return -1;
		}
		else if (e.satisfied[i] == 0) {
			if (y_ineq[i] <= p.rhs[i]) {// cxsc
				e.satisfied[i] = 1;
				++num_satisfied;
			}
			else if (Disjoint(y_ineq[i], p.rhs[i])) {
				e.satisfied[i] = -1;
				return -1;
			}
		}
	}
	return num_satisfied;
}


#if defined INITIAL_SOBOL_EXCLUSION

int complement (ivector &x, ivector &y, tbb::concurrent_vector<ivector> &result) {
	// complement of x in y
	// result assumed to be cleared!!!
	//cout << "complement: x = " << x << ", y = " << y << endl;
	if (VecLen(x) != VecLen(y)) return -1;
	//result.clear();
	if (Disjoint (x, y)) {
		result.push_back (y);
		return 1;
	}
	//elem e(x & y);
	//if (Lebesgue(e) < 1e-12) {
	//if (Lebesgue(x & y) < 1e-12) {
	if (Lebesgue(x & y) < 1e-12 && Lebesgue(x) >= 1e-12) {
		result.push_back (y);
		return 1;
	}
	for (int i = 1; i <= VecLen(y); ++i) {
		interval z = x[i] & y[i];
		if (Inf(y[i]) < Inf(z)) {
			ivector w = y;
			w[i] = interval (Inf(y[i]), Inf(z));
			result.push_back (w);
		}
		if (Sup(z) < Sup(y[i])) {
			ivector w = y;
			w[i] = interval (Sup(z), Sup(y[i]));
			result.push_back (w);
		}
		y[i] = z;
	}
	//for (size_t i = 0; i < result.size(); ++i) cout << "resulting x = " << result[i] << "\n";
	return 0;
}


int complement (ivector &x, tbb::concurrent_vector<ivector> &list, tbb::concurrent_vector<ivector> &result) {
	// complement of x in y
	// result assumed to be cleared!!!
	//result.clear();
	ivector y;
	/*while (list.size() != 0) {
		list.pop (y);
		complement (x, y, result);
	}*/
	for (size_t i = 0, len = list.size(); i < len; ++i) complement (x, list[i], result);
	//for (auto &box : list) complement(x, box, result);
	list.clear();
	return 0;
}

#endif

int GaussElim (rmatrix V, intvector &row_num, intvector &col_num) {
	//cout << "GE\n";
	int i = 1, rows_num = ColLen(V), cols_num = RowLen(V);
	int k, j, error;
	real maxval;
	rvector tmp (cols_num), tmp2 (rows_num);
	int act_row, act_col, itmp;
	// assigning primary values to row_num and col_num
	// we assume rows_num <= cols_num
	do {
		row_num[i] = col_num[i] = i;
	} while (++i <= rows_num);
	while (i <= cols_num) {
		col_num[i] = i;
		++i;
	} //while (++i <= cols_num);
	/*cout << "V = " << V << endl;
	cout << "row_num = " << row_num << endl;
	cout << "col_num = " << col_num << endl;*/
	// main loop
	i = 1;
	do {
		// pivoting
		maxval = abs(V[i][i]);
		act_row = act_col = i;
		//row_num[i] = i;
		//col_num[i] = i;
		for (k = i; k <= rows_num; ++k)
			//if (satisfied[k] != 0) continue;
			for (j = i; j <= cols_num; ++j)
		      	 	if (abs(V[k][j]) > maxval) {
					act_row = k;
					act_col = j;
					//row_num[i] = k;
					//col_num[i] = j;
					maxval = abs(V[k][j]);
				}
		// rows exchanging
		tmp = V[i];
		V[i] = V[act_row/*row_num[i]*/];
		V[act_row/*row_num[i]*/] = tmp;
		itmp = row_num[i];
		row_num[i] = row_num[act_row];
		row_num[act_row] = itmp;
		// columns exchanging
		tmp2 = rvector (Col(V, i));
		Col(V, i) = Col(V, act_col/*col_num[i]*/);
		Col(V, act_col/*col_num[i]*/) = tmp2;
		itmp = col_num[i];
		col_num[i] = col_num[act_col];
		col_num[act_col] = itmp;
		// elimination
		for (k = i + 1; k <=rows_num; ++k) {
			real coef = V[k][i]/V[i][i];
			V[k] -= coef*V[i];
			V[k][i] = 0.0;
		}
	} while (++i <= rows_num);
	/*cout << "V = " << V << endl;
	cout << "row_num = " << row_num << endl;
	cout << "col_num = " << col_num << endl;*/
	return 0;
}


int Herbort_create_pair_list (const imatrix &J, std::vector< std::pair<int, int> > &pairs_for_Ncmp) {
	int i = 1, j = 1;
	//cout << "J = " << J << endl;
	
	pairs_for_Ncmp.clear();
	for (int i = 1; i <= ColLen(J); ++i) {
		//cout << "i = " << i << endl;
		j = i;
		do {
			//cout << " j = " << j << endl;
			if (J[i][j] != 0.0) {
				pairs_for_Ncmp.push_back(std::make_pair(i, j));
			}
			if (++j > RowLen(J)) j = 1;
		} while (j != i);
	}

	return 0;
}


int GE_create_pair_list (const imatrix &J, std::vector< std::pair<int, int> > &pairs_for_Ncmp) {
	int licz1 = 0, licz2 = 0;
	int i = 1, rows_num = ColLen(J), cols_num = RowLen(J);
	intvector row_num (rows_num), col_num (cols_num);
	//cout << "J = " << J << endl;
	
	pairs_for_Ncmp.clear();
	//GaussElim (mid(J), row_num, col_num);
	GaussElim ((Inf(J) + Sup(J))/2.0, row_num, col_num);
	do {
		//cout << "i = " << i << endl;
		pairs_for_Ncmp.push_back(std::make_pair(row_num[i], col_num[i]));
		//pairs_for_Ncmp[i][1] = row_num[i];
		//pairs_for_Ncmp[i][2] = col_num[i];
	} while (++i <= rows_num);

	return 0;
}


int Goulard_GE_create_pair_list (const imatrix &J, std::vector< std::pair<int, int> > &pairs_for_Ncmp) {
	int i = 1, rows_num = ColLen(J), cols_num = RowLen(J);
	int licz2 = 0;
	rmatrix W;
	intvector row_num (rows_num), col_num (cols_num);
	Goulard_compute_weight_matrix (J, W);
	GaussElim(W, row_num, col_num);
	pairs_for_Ncmp.clear();
	do {
		pairs_for_Ncmp.push_back(std::make_pair(row_num[i], col_num[i]));
	} while (++i <= rows_num);

	return 0;
}


int Goulard_compute_weight_matrix (const imatrix &J, rmatrix &W) {
	int j = 1;
	rvector maxJ_j (RowLen(J));
	real maxim;
	W = Inf (J); // to set proper dimensions of W
	do {
		//cout << "j = " << j << endl;
		//i = 2;
		maxJ_j[j] = AbsMax(J[1][j]);
		for (int i = 2; i <= ColLen(J); ++i) {
			if ((maxim = AbsMax(J[i][j])) > maxJ_j[j]) maxJ_j[j] = maxim;
		}
	} while (++j <= RowLen(J));
	int i = 1;
	do {
		j = 1;
		do {
			if (in(0, J[i][j])) W[i][j] = AbsMax(J[i][j]);
			else W[i][j] = AbsMin(J[i][j]) + maxJ_j[j];
		} while (++j <= RowLen(J));
	} while (++i <= ColLen(J));
	//cout << "W = " << W << endl;
	return 0;
}

