
#include <fenv.h>

int find_inner_sol_tss_center (ivector &a, interval &b, ivector &x, rvector &x0) {
	real y0 = min (abs(Inf(b)), abs(Sup(b)));
	fesetround (FE_DOWNWARD);
	real sum = 0.0;
	for (int i = 1; i <= VecLen(a); ++i) sum += AbsMax(a[i]);
	real rho = abs(y0/sum);
	x = x0;
	for (int i = 1; i <= VecLen(x); ++i) x[i] += interval(-rho, rho);
	fesetround (FE_TONEAREST);
	return 0;
}

