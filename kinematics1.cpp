
#include "solver.hpp"
#include "problem_desc_impl.hpp"

#include <string>

#define N 12
#define M N

// N - variables, M - equations

template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> eq (const adhc_vector<level, sparse_mode, n, T> &x, const int num) {
	adhc_ari<level, sparse_mode, n, T> result;
#define s(i) x[2*i - 1]
#define c(i) x[2*i]
	if (num <= 6) {
		result = sqr(x[2*num - 1]) + sqr(x[2*num]) - 1.0;
	}
	else if (num == 7) {
		result = 3*s(2) + 2*s(3) + s(4) - 3.9701;
	}
	else if (num == 8) {
		result = c(1)*(3*c(2) + 2*c(3) + c(4)) - 4.0616;
	}
	else if (num == 9) {
		result = s(1)*(3*c(2) + 2*c(3) + c(4)) - 1.7172;
	}
	else if (num == 10) {
		result = s(5)*(s(2) + s(3) + s(4)) - 1.9791;
	}
	else if (num == 11) {
		result = c(6)*(c(2) + c(3) + c(4)) - c(5)*s(6)*(s(2) + s(3) + s(4)) - 0.4077;
	}
	else { // num == 12
		result = c(1)*s(5)*(c(2) + c(3) + c(4)) + s(1)*c(5) - 1.9115;
	}
#undef c
#undef s
	return result;
}


template<int level, SparsityLevel sparse_mode, int n, class T>
struct kinematics {
	adhc_ari<level, sparse_mode, n, T> operator() (const adhc_vector<level, sparse_mode, n, T> &x, const int i) const {
		return eq(x, i);
	};
};


//#define SPARSITY SparsityLevel::dense
//#define SPARSITY sparse
#define SPARSITY SparsityLevel::highly_sparse


int main() {
#include "num_threads.hpp"
	opdotprec = 1;
	ivector x(N);
	x = interval (-1.0, 1.0);
	real eps = 5e-2;
	eps = 1e-6;
	//eps = 1e-8;
	problem_desc_impl<SPARSITY, N, M, kinematics> p(x);
	solver s (num_threads);
	cout << "x0 = " << x << endl;
	create_func creat = Herbort_create_pair_list;
	//create_func creat = Goulard_create_pair_list;
	//create_func creat = GE_create_pair_list;
	//create_func creat = Goulard_GE_create_pair_list;
	s.branch_and_bound (p, eps, creat);
//#define SHOW
#if defined SHOW
	cout << "Possible: " << endl;
	for (auto &x : s.sol) {
		x.show();
	}
	cout << endl << "Guaranteed: " << endl;
	for (auto &x : s.guar_sol) {
		x.show();
	}
	std::cout << "========\n";
#endif
	s.show_results();
	s.stat_show();
	return 0;
}

