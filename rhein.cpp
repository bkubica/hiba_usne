
#include "solver.hpp"
#include "problem_desc_impl.hpp"

#include <string>

#define N 8
//constexpr int N = 6;
#define M 5

// N - variables, M - equations

template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> eq1 (const adhc_vector<level, sparse_mode, n, T> &x) {
	adhc_ari<level, sparse_mode, n, T> result;
	result = -3.933*x[1] + 0.107*x[2] + 0.126*x[3] - 9.99*x[5] - 45.83*x[7] - 7.64*x[8] - 0.727*x[2]*x[3] + 8.39*x[3]*x[4] - 684.4*x[4]*x[5] + 63.5*x[4]*x[7];
	return result;
}

template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> eq2 (const adhc_vector<level, sparse_mode, n, T> &x) {
	adhc_ari<level, sparse_mode, n, T> result;
	result = -0.987*x[2] - 22.95*x[4] - 28.37*x[6] + 0.949*x[1]*x[3] + 0.173*x[1]*x[5];
	return result;
}

template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> eq3 (const adhc_vector<level, sparse_mode, n, T> &x) {
	adhc_ari<level, sparse_mode, n, T> result;
	result = 0.002*x[1] - 0.235*x[3] + 5.67*x[5] + 0.921*x[7] - 6.51*x[8] - 0.716*x[1]*x[2] - 1.578*x[1]*x[4] + 1.132*x[4]*x[7];
	return result;
}

template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> eq4 (const adhc_vector<level, sparse_mode, n, T> &x) {
	adhc_ari<level, sparse_mode, n, T> result;
	result = x[1] - x[4] - 0.168*x[6] - x[1]*x[2];
	return result;
}

template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> eq5 (const adhc_vector<level, sparse_mode, n, T> &x) {
	adhc_ari<level, sparse_mode, n, T> result;
	result = -x[3] - 0.196*x[5] - 0.0071*x[7] + x[1]*x[4];
	return result;
}

template<int level, SparsityLevel sparse_mode, int n, class T>
struct rhein {
	rhein() {};

	adhc_ari<level, sparse_mode, n, T> operator() (const adhc_vector<level, sparse_mode, n, T> &x, const int i) const {
		switch(i) {
			case 1: return eq1(x);
			case 2: return eq2(x);
			case 3: return eq3(x);
			case 4: return eq4(x);
			case 5: return eq5(x);
		}
		std::terminate();
	};
};


template<typename T>
real rh_Lebesgue (tbb::concurrent_vector<T> &vec) {
	real result (0.0);
	size_t i = 0, i_end = vec.size();
	while (i < i_end) {
		int ii = 1;
		real leb (1.0);
		do {
			if (ii != 6 && ii != 8) leb *= diam(vec[i].x[ii]);
		} while (++ii <= VecLen(vec[i].x));
		result += leb;//Lebesgue(vec.at(i++));
		++i;
	}
	return result;
}

template<typename T>
interval rh_ILebesgue (tbb::concurrent_vector<T> &vec) {
	interval result (0.0);
	size_t i = 0, i_end = vec.size();
	while (i < i_end) {
		int ii = 1;
		interval leb (1.0);
		do {
			if (ii != 6 && ii != 8) leb *= diam(vec[i].x[ii]);
		} while (++ii <= VecLen(vec[i].x));
		result += leb;//Lebesgue(vec.at(i++));
		++i;
	}
	return result;
}



//#define SPARSITY SparsityLevel::dense
//#define SPARSITY sparse
#define SPARSITY SparsityLevel::highly_sparse


int main() {
#include "num_threads.hpp"
	opdotprec = 1;
	ivector x(N);
	x = interval (-2.0, 2.0);
	x[6] = interval(0.1);
	x[8] = interval(0.0);
	real eps = /*1e-4;/ /1e-3;/ */ 1e-1;
	//real eps = 5e-2;
	eps = 1e-4;
	problem_desc_impl<SPARSITY, N, M, rhein> p{x};
	solver s (num_threads);
	cout << "x0 = " << x << endl;
	create_func creat = Herbort_create_pair_list;
	//create_func creat = Goulard_create_pair_list;
	//create_func creat = GE_create_pair_list;
	//create_func creat = Goulard_GE_create_pair_list;
	s.branch_and_bound (p, eps, creat);
//#define SHOW
#if defined SHOW
	cout << "Possible: " << endl;
	for (auto &x : s.sol) {
		x.show();
	}
	cout << endl << "Guaranteed: " << endl;
	for (auto &x : s.guar_sol) {
		x.show();
	}
	std::cout << "========\n";
#endif
	s.show_results();
	cout << "Possible measure (rh): " << rh_Lebesgue (s.sol) << " in " << rh_ILebesgue (s.sol) << endl;
	cout << "Guaranteed measure (rh): " << rh_Lebesgue (s.guar_sol) << " in " << rh_ILebesgue (s.guar_sol) << endl;
	s.stat_show();
	return 0;
}

