
#include "solver.hpp"
#include "problem_desc_impl.hpp"

#include <string>

#define N 200
//#define N 42
#define M N

// N - variables, M - equations

template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> eq1 (const adhc_vector<level, sparse_mode, n, T> &x, const int num) {
	adhc_ari<level, sparse_mode, n, T> result;
	//result = x[num] + ((5.0 - x[num + 1])*x[num + 1] - 2.0)*x[num + 1] - 13.0;
	result = x[num] + 5.0*sqr(x[num + 1]) - power(x[num + 1], 3) - 2.0*x[num + 1] - 13.0;
	return result;
}


template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> eq2 (const adhc_vector<level, sparse_mode, n, T> &x, const int num) {
	adhc_ari<level, sparse_mode, n, T> result;
	//result = x[num - 1] + ((x[num] + 1.0)*x[num] - 14.0)*x[num] - 29.0;
	result = x[num - 1] + power(x[num], 3) + sqr(x[num]) - 14.0*x[num] - 29.0;
	return result;
}


template<int level, SparsityLevel sparse_mode, int n, class T>
struct extended_freudenstein {
	adhc_ari<level, sparse_mode, n, T> operator() (const adhc_vector<level, sparse_mode, n, T> &x, const int i=0) const {
		/*adhc_ari<level, sparse_mode, n, T> result;
		result = (i%2 == 1)?eq1(x, i):eq2(x, i);
		return result;*/
		return (i%2 == 1)?eq1(x, i):eq2(x, i);
	};
};


//#define SPARSITY SparsityLevel::dense
//#define SPARSITY sparse
#define SPARSITY SparsityLevel::highly_sparse
//#define SPARSITY SparsityLevel::another_sparse


int main() {
#include "num_threads.hpp"
	opdotprec = 1;
	ivector x(N);
	x = interval (-1e8, 1e8);
	//x = interval (-1e8, 1.0001e8);
	real eps = 1e-6;

	problem_desc_impl<SPARSITY, N, M, extended_freudenstein> p(x);

	solver s (num_threads);
	cout << "x0 = " << x << endl;
	create_func creat = Herbort_create_pair_list;
	//create_func creat = Goulard_create_pair_list;
	//create_func creat = GE_create_pair_list;
	//create_func creat = Goulard_GE_create_pair_list;
	s.branch_and_bound (p, eps, creat);
//#define SHOW
#if defined SHOW
	cout << "Possible: " << endl;
	for (auto &x : s.sol) {
		x.show();
	}
	cout << endl << "Guaranteed: " << endl;
	for (auto &x : s.guar_sol) {
		x.show();
	}
	std::cout << "========\n";
#endif
	s.show_results();
	s.stat_show();
	return 0;
}

