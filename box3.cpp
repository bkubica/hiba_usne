
#include "solver.hpp"
#include "problem_desc_impl.hpp"

#include <string>

#define N 3
#define M 3

// N - variables, M - equations

template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> eq1 (const adhc_vector<level, sparse_mode, n, T> &x) {
	adhc_ari<level, sparse_mode, n, T> result;
	result = exp(-0.1*x[1]) - exp(-0.1*x[2]) - x[3]*(exp(-0.1) - exp(-1.0));
	return result;
}

template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> eq2 (const adhc_vector<level, sparse_mode, n, T> &x) {
	adhc_ari<level, sparse_mode, n, T> result;
	result = exp(-0.2*x[1]) - exp(-0.2*x[2]) - x[3]*(exp(-0.2) - exp(-2.0));
	return result;
}

template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> eq3 (const adhc_vector<level, sparse_mode, n, T> &x) {
	adhc_ari<level, sparse_mode, n, T> result;
	result = exp(-0.3*x[1]) - exp(-0.3*x[2]) - x[3]*(exp(-0.3) - exp(-3.0));
	return result;
}


template<int level, SparsityLevel sparse_mode, int n, class T>
struct box3 {
	adhc_ari<level, sparse_mode, n, T> operator() (const adhc_vector<level, sparse_mode, n, T> &x, const int i) const {
		switch (i) {
			case 1: return eq1(x);
			case 2: return eq2(x);
			default: return eq3(x);
		}
	}
};


//#define SPARSITY SparsityLevel::dense
//#define SPARSITY sparse
#define SPARSITY SparsityLevel::highly_sparse


int main() {
#include "num_threads.hpp"
	opdotprec = 1;
	ivector x(N);
	x[1] = x[2] = interval (-100.0, 100.0);
	x[3] = interval (0.1, 100.0);
	real eps = 1e-5;//1e-6;
	eps = 1e-8;
	problem_desc_impl<SPARSITY, N, M, box3> p(x);
	solver s (num_threads);
	cout << "x0 = " << x << endl;
	create_func creat = Herbort_create_pair_list;
	//create_func creat = Goulard_create_pair_list;
	//create_func creat = GE_create_pair_list;
	//create_func creat = Goulard_GE_create_pair_list;
	s.branch_and_bound (p, eps, creat);
#define SHOW
#if defined SHOW
	cout << "Possible: " << endl;
	for (auto &x : s.sol) {
		x.show();
	}
	cout << endl << "Guaranteed: " << endl;
	for (auto &x : s.guar_sol) {
		x.show();
	}
	std::cout << "========\n";
#endif
	s.show_results();
	s.stat_show();
	return 0;
}

