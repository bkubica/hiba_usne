
#include "solver.hpp"
#include "problem_desc_impl.hpp"

/* Parameterizing the 3-layer neural network, so that it approximated the XOR function. *
 * It requires the high-precision Newton to be turned-off, as it results in an error    *
 * (probably a bug in the exp() function for cxsc::l_interval and/or cxsc::l_real, and  *
 * the eps is still not satisfying. 
 * Adding inequality constraints, distinct from the equations did not help...           */

// N - variables, M - equations

constexpr int num_points_in_sample = 4;

constexpr std::array<int, 3> num_neurons_in_layer { {2, 2, 1} };
//constexpr int num_neurons_in_layer[] = {2, 2, 1};

constexpr int num_layers = num_neurons_in_layer.size();
//constexpr int num_layers = std::size(num_neurons_in_layer);
//constexpr int num_layers = sizeof(num_neurons_in_layer)/sizeof(num_neurons_in_layer[0]);

constexpr int num_neurons_in_layers_up_to (int n) {
	/*int sum{0};
	for (int i = 0; i < n; ++i) sum += num_neurons_in_layer[i];
	return sum;*/
	return (n < 0)?(0):(num_neurons_in_layer[n] + num_neurons_in_layers_up_to(n - 1));
}

constexpr int num_connections_in_layers_up_to (int n) {
	return (n <= 0)?(0):(num_neurons_in_layer[n]*(num_neurons_in_layer[n - 1] + 1) + num_connections_in_layers_up_to(n - 1));
	// (num_neurons_in_layer[n - 1] + 1) stands for `connections from all neurons from the previous layer + bias'
}

constexpr int num_equations_for_single_point_in_sample = num_neurons_in_layers_up_to(num_layers - 2) - num_neurons_in_layer[0];

//#define N (num_connections_in_layers_up_to(num_layers) + num_points_in_sample*num_neurons_in_layers_up_to(num_layers))
//#define M (num_points_in_sample*(num_neurons_in_layers_up_to(num_layers) - num_neurons_in_layer[0]))
constexpr int N      = num_connections_in_layers_up_to(num_layers - 1) + num_points_in_sample*num_neurons_in_layers_up_to(num_layers - 2);
constexpr int M_eq   = num_points_in_sample*(num_neurons_in_layers_up_to(num_layers - 2) - num_neurons_in_layer[0]);
constexpr int M_ineq = num_points_in_sample;


/*struct PointOfSample {
	cxsc::ivector x;
	cxsc::interval y;
};


struct Sample {
	std::vector<PointOfSample> pts;
};*/


//#define weight(layer, i, j) x[1 + layer*num_neur_per_layer + i*num_neur_per_layer + j] /* connection between neuron i in layer and j in (layer + 1) */
//#define state(layer, i) x[1 + num_layers*num_neur_per_layer + layer*num_neur_per_layer + i]

#define weight(layer, i, j) x[/*1 +*/ num_connections_in_layers_up_to(layer - 1) + i*(num_neurons_in_layer[layer]) + j] /* connection between neuron i in layer and j in (layer + 1) */
#define state(sample_point, layer, i) x[/*1 +*/ num_connections_in_layers_up_to(num_layers - 1) + sample_point*(num_neurons_in_layers_up_to(num_layers - 2)) + num_neurons_in_layers_up_to(layer - 1) + i]

// ``1 +'', beacuse of the cxsc features!!!


template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> sigma (const adhc_ari<level, sparse_mode, n, T> &x) {
	//static cxsc::interval one{1.0};
	adhc_ari<level, sparse_mode, n, T> one;
	one = cxsc::real(1.0);
	return one/(one + exp(-10.0*x));
}


template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> identity (const adhc_ari<level, sparse_mode, n, T> &x) {
	return x;
}


template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> single_neuron_activation (
		const adhc_vector<level, sparse_mode, n, T> &x,
		const int sample_point,
		const int layer,
		const int index,
		std::function<adhc_ari<level, sparse_mode, n, T>(const adhc_ari<level, sparse_mode, n, T> &)> activation_fun
			= sigma<level, sparse_mode, n, T>) {
	//std::cout << "activation(" << layer << ", " << index << ")\n";
	if (layer == 0) return state(sample_point, layer, index);
	const int layer1 = layer - 1;
	//const int index1 = index - 1;
	adhc_ari<level, sparse_mode, n, T> sum;// = adhc_ari<level, sparse_mode, n, T>::get_instance();
	//sum = weight(layer, 0, index)*state(sample_point, layer1, 0);
	sum = weight(layer, 0, index); // bias
	for (int i = 1; i <= num_neurons_in_layer[layer1]; ++i) sum += weight(layer, i, index)*state(sample_point, layer1, i);
	//return max(sum, 0.0);
	//return max(sum, sum*0.01);
	//return sigma(sum);
	return activation_fun(sum);
}



/*template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> network_value (const adhc_vector<level, sparse_mode, n, T> &xx, const PointOfSample &point) {
	adhc_vector<level, sparse_mode, n, T> x = xx;
	adhc_ari<level, sparse_mode, n, T> result;
	for (int i = 0; i < num_neur_per_layer; ++i) state(0, i) = point.x[i];
	for (int layer = 0 + 1; layer < num_layers; ++layer) {
		for (int i = 0; i < num_neur_per_layer; ++i) {
			state(layer, i) = single_neuron_activation(x, layer, i);
		}
	}
	// ...
	return result;
}*/


template<int level, SparsityLevel sparse_mode, int n, class T>
static std::array<std::function<adhc_ari<level, sparse_mode, n, T>(const adhc_ari<level, sparse_mode, n, T> &)>, num_layers>
		activation_funs = { {
	sigma<level, sparse_mode, n, T>,
	sigma<level, sparse_mode, n, T>
} };
//};


template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> eq_i (const adhc_vector<level, sparse_mode, n, T> &x, int i) {
	adhc_ari<level, sparse_mode, n, T> result;// = adhc_ari<level, sparse_mode, n, T>::get_instance();
	//std::cout << "\ni = " << i << "\t";
	const int sample_point = (i - 1) / num_equations_for_single_point_in_sample;
	int layer = 1;// + (i - 1) / 2;//num_neur_per_layer;
	//for (int ii = i - 1; ii >= 0; ii -= num_neurons_in_layer[layer], ++layer);
	//--layer;
	int index = (i - 1) % num_equations_for_single_point_in_sample;
	while (index >= num_neurons_in_layer[layer]) {
		index -= num_neurons_in_layer[layer];
		++layer;
	}
	++index;
	//std::cout << i << " = (" << sample_point << ", " << layer << ", " << index << ")\n";
	//static std::function<adhc_ari<level, sparse_mode, n, T>(const adhc_ari<level, sparse_mode, n, T> &)> activation_funs[] = {
	result = single_neuron_activation(x, sample_point, layer, index, activation_funs<level, sparse_mode, n, T>[layer - 1]) - state(sample_point, layer, index);
	/*if (layer == 1)	result = single_neuron_activation(x, sample_point, layer, index, identity<level, sparse_mode, n, T>) - state(sample_point, layer, index);
	else result = single_neuron_activation(x, sample_point, layer, index) - state(sample_point, layer, index);*/
	//std::cout << result << "\n";
	return result;
}


template<int level, SparsityLevel sparse_mode, int n, class T>
struct neur {
	adhc_ari<level, sparse_mode, n, T> operator() (const adhc_vector<level, sparse_mode, n, T> &x, const int i) const {
		return eq_i(x, i);
	}
};

template<int level, SparsityLevel sparse_mode, int n, class T>
struct neur_output {
	adhc_ari<level, sparse_mode, n, T> operator() (const adhc_vector<level, sparse_mode, n, T> &x, const int i) const {
		//cout << "ineq " << i << endl;
		return single_neuron_activation(x, i - 1, num_layers - 1, 1, activation_funs<level, sparse_mode, n, T>[num_layers - 2]);
	}
};


#define SPARSITY SparsityLevel::highly_sparse


int main() {
#include "num_threads.hpp"
	opdotprec = 1;
	ivector x(N);
	x = cxsc::interval (-2.0, 2.0);
	state(0, 0, 1) = 1.0; state(0, 0, 2) = 1.0;
	state(1, 0, 1) = 0.0; state(1, 0, 2) = 0.0;
	state(2, 0, 1) = 1.0; state(2, 0, 2) = 0.0;
	state(3, 0, 1) = 0.0; state(3, 0, 2) = 1.0;
	for (int i = 0; i < num_points_in_sample; ++i) {
		for (int j = 1l; j <= num_neurons_in_layer[0]; ++j) {
			state(i, 0, j) += cxsc::interval(-1e-2, 1e-2);
		}
	}
	/*state(4, 0, 1) = 10.0; state(4, 0, 2) = 10.0; state(4, 2, 1) = 0.0; // 1 xor 1 = 0
	state(5, 0, 1) = -10.0; state(5, 0, 2) = -10.0; state(5, 2, 1) = 0.0; // 0 xor 0 = 0
	state(6, 0, 1) = 10.0; state(6, 0, 2) = 0.0; state(6, 2, 1) = 1.0; // 1 xor 0 = 1
	state(7, 0, 1) = 0.0; state(7, 0, 2) = 10.0; state(7, 2, 1) = 1.0; // 0 xor 1 = 1*/
	//weight(1, 0, 1) = -1.5; weight(1, 0, 2) = -0.5;
	//-weight(1, 0, 1) = -1.0; weight(1, 0, 2) = -1.0;
	//weight(1, 1, 1) = weight(1, 1, 2) = weight(1, 2, 1) = weight(1, 2, 2) = 1.0;
	//weight(2, 0, 1) = -0.5;
	//weight(2, 1, 1) = -1.0; weight(2, 2, 1) = 1.0;
	//for (int i = Lb(x); i <= Ub(x); ++i) if (diam(x[i]) <= 1e-2) x[i] += cxsc::interval(-1e-2, 1e-2);
	problem_desc_impl<SPARSITY, N, M_eq, neur, M_ineq, neur_output> p{x};
	/*state(0, 2, 1)*/p.rhs[1] = 0.0; // 1 xor 1 = 0
	/*state(1, 2, 1)*/p.rhs[2] = 0.0; // 0 xor 0 = 0
	/*state(2, 2, 1)*/p.rhs[3] = 1.0; // 1 xor 0 = 1
	/*state(3, 2, 1)*/p.rhs[4] = 1.0; // 0 xor 1 = 1
	for (int i = 1; i <= num_points_in_sample; ++i) p.rhs[i] += cxsc::interval(-1e-2, 1e-2);
	cout << "x = " << x << "\n";
	cout << "N = " << N << "\n";
	cout << "M_eq = " << M_eq << "\n";
	cout << "M_ineq = " << M_ineq << "\n";
	for (int i = 0; i < num_layers; ++i) cout << "neurons: " << num_neurons_in_layers_up_to(i) << "\n";
	for (int i = 0; i < num_layers; ++i) cout << "connections: " << num_connections_in_layers_up_to(i) << "\n";
	/*const int layer = 2;
	const int sample_point = 0;
	for (int i = 0; i < num_neurons_in_layer[layer - 1]; ++i) {
		cout << "x: " << 1 + num_connections_in_layers_up_to(num_layers) + sample_point*num_neurons_in_layers_up_to(num_layers) + num_neurons_in_layers_up_to(layer - 1) + i << "\n";
		//for (int j = 0; j < num_neurons_in_layer[layer]; ++j) cout << "w: " << 1 + num_connections_in_layers_up_to(layer - 1) + i*num_neurons_in_layer[layer] + j << "\n";
	}*/
	real eps = 1e-6;
	eps = 1e-3;
	eps = 5e-3;
	//eps = 4.0;
	//eps = 2.0;
	eps = 1.0;
	//eps = 0.5;
	//eps = 2e-1;
	//eps = 1e-1;
	//eps = 5e-2;
	//----eps = 2e-2;
	solver s (num_threads);
	cout << "x0 = " << x << endl;
	//std::terminate();
	create_func creat = Herbort_create_pair_list;
	//create_func creat = Goulard_create_pair_list;
	//create_func creat = GE_create_pair_list;
	//create_func creat = Goulard_GE_create_pair_list;
	s.branch_and_bound (p, eps, creat);
//#define SHOW
#if defined SHOW
	cout << "Possible: " << endl;
	/*for (size_t i = 0; i < s.sol.size(); ++i) {
		//cout << "i = " << i << endl;
		s.sol[i].show();
	}*/
	for (auto &x : s.sol) {
		x.show();
	}
	cout << endl << "Guaranteed: " << endl;
	/*for (size_t i = 0; i < s.guar_sol.size(); ++i) {
		s.guar_sol[i].show();
	}*/
	for (auto &x : s.guar_sol) {
		x.show();
	}
	std::cout << "========\n";
#endif
	s.show_results();
	s.stat_show();
	return 0;
}

