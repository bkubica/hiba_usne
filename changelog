
Changelog of HIBA_USNE Beta 2.8.9-1
Released on 27th of September 2022
----------------------------------

- Corrected the neural-3-layer example: now it trains a NN to approximate the
XOR function. Activation functions, etc., are easily changed. The performance
for this example is still rather poor; possibly it requires handling
inequality constraints, distinguished from equations.
- Minor maintenance changes to the solver (mostly irrelevant).


***************************************************************************
Changelog of HIBA_USNE Beta 2.8.9
Released on 31st of August 2022
----------------------------------

Updated the solver to collaborate with the new versions of oneTBB.
In particular:
- Replaced tbb::task_scheduler_init with tbb::global_control.
- Replaced tbb::parallel_do with tbb::parallel_for_each.
- Updated the interface of tbb::spin_rw_mutex.
- Minor related changes (names of some methods, task_arena -> this_task_arena, etc.).
- Also, a (somewhat controversial) change to the problem_desc_impl class, not
to copy the syntactic tree after each task stealing (it seems to be irrelevant
for earlier versions of TBB; also it did not turn out to result in a
significant speedup, but it seems sane).


***************************************************************************
Changelog of HIBA_USNE Beta 2.8.4
Released on 11th of April 2022
----------------------------------

- Updated the solver to collaborate with ADHCv2.2.
- Corrected some memory management errors, detected by valgrind.
- Minor bugfixes & maintenance changes.


***************************************************************************
Changelog of HIBA_USNE Beta 2.8.2
Released on 13th of August 2021
----------------------------------

- Corrected the high precision Newton operator, based on the cxsc::l_interval
type: now the preconditioner is correctly computed.
- Minor bugfixes & maintenance changes.


***************************************************************************
Changelog of HIBA_USNE Beta 2.8
Released on 8th of July 2021
----------------------------------

- Made compatible with the ADHC 2.0 library.
- Added a high precision Newton operator, based on the cxsc::l_interval type.
- Maintenance changes.


***************************************************************************
Changelog of HIBA_USNE Beta 2.6
Released on 10th of August 2020
----------------------------------

- Added new procedures for box verification (see the first paper below).
- Adjusted to work with ADHC 1.1.
- New test problems have been added (for the second of below papers: stationary-hopf.cpp
and hopfield.cpp).
- Maintenance changes for GCC 9.3.0 compatibility.
- Minor bug fixes.

Comment: this version was used in experiments, described in the following papers:

(i) Kubica BJ, Kurek J. A Parallel Method of Verifying Solutions for Systems of Two Nonlinear Equations.
[In:] International Conference on Parallel Processing and Applied Mathematics 2019 Sep 8 (pp. 418-430). Springer, Cham.

(ii) Kubica BJ, Hoser P, Wiliński A. Interval Methods for Seeking Fixed Points of
Recurrent Neural Networks.
[In:} International Conference on Computational Science 2020 Jun 3 (pp. 414-423). Springer, Cham.


***************************************************************************
Changelog of HIBA_USNE Beta 2.5
Released on 5th of May 2017
----------------------------------

- Adjusted to work with ADHC Alfa.0.5; affects backward compatibility!
- Hull consistency is used now, in addition to box consistency; consequently,
we obtain a procedure equivalent to BC4.
- New test problems have been added (Broyden-tridiagonal, Kinematics1 and More-Cosnard).
- Corrected the code for Broyden-banded test problem.
- Some maintenance changes.
- Corrected spelling errors in the changelog. ;-)


***************************************************************************


Changelog of HIBA_USNE Beta 2.0
Released on 28th of September 2016
----------------------------------

- ADHC library is now used for automatic differentiation.
- Several maintenance changes (cleaned some code, changed variable names to
more meaningful, removed some dead code, etc.).


***************************************************************************


Changelog of HIBA_USNE Beta 1.2
Released on 29th of September 2015
----------------------------------

- Added a parallel procedure for bound consistency checking.
- Added support for the MIC architecture.
- The initial box consistency enforcing procedure was parallelized.
- Replaced new/delete semantics with the C++11 std::move in maintaining tasks.
- Removed the (now unnecessary) memory allocator interface.
- Removed some dead code, in particular several macros for discontinued code
versions.
- Statistics are now kept in thread-specific structures and joined by
reduction in the end.
- Major cleanup of the code (yeah, I've read a Bob Martin's book ;-) ).
- Minor bug fixes.

