
#if !defined __SOLVER_HPP__
#define __SOLVER_HPP__

#include <vector>

//#include <tbb/task_scheduler_init.h>
#include <tbb/global_control.h>
#include <tbb/blocked_range.h>
//#include <tbb/parallel_do.h>
#include <tbb/parallel_for_each.h>
//#include <tbb/atomic.h>
#include <tbb/concurrent_vector.h>

#include <ivector.hpp>
#include <xscclass.hpp>

#include "lists.hpp"
#include "problem_desc.hpp"

using namespace cxsc;
using namespace tbb;
using namespace std;

//typedef int (*create_func)(const imatrix &, std::vector< std::pair<int, int> > &);
using create_func = int (*)(const imatrix &, std::vector< std::pair<int, int> > &);


struct solver {
	tbb::concurrent_vector<elem> sol;
	tbb::concurrent_vector<elem_guaranteed> guar_sol;
	tbb::concurrent_vector<elem_guaranteed> advanced_guar_sol;
	//lock_type lock_L, lock_sol, lock_guar_sol;
	int num_threads;
	//task_scheduler_init init;
	tbb::global_control glob_cntrl;

	solver (const int num_threads_) : num_threads(num_threads_), glob_cntrl(tbb::global_control::max_allowed_parallelism, num_threads_) {
		sol.clear();
		guar_sol.clear();
		advanced_guar_sol.clear();
	};

	void show_results() const {
		std::stringstream ss;
		ss << "Possible: " << sol.size() << "\n";
		ss << "Guaranteed: " << guar_sol.size() << "\n";
		ss << "Advanced guaranteed: " << advanced_guar_sol.size() << "\n";
		//ss << "Possible measure: " << Lebesgue(sol) << " in " << ILebesgue(sol) << "\n";
		ss << "Possible measure: " << Lebesgue_parallel (sol) << " in " << ILebesgue_parallel (sol) << "\n";
		//ss << "Guaranteed measure: " << Lebesgue(guar_sol) << " in " << ILebesgue(guar_sol) << "\n";
		ss << "Guaranteed measure: " << Lebesgue_parallel (guar_sol) << " in " << ILebesgue_parallel (guar_sol) << "\n";
		//ss << "Advanced guaranteed measure: " << Lebesgue(advanced_guar_sol) << " in ";
		//ss << ILebesgue(advanced_guar_sol) << "\n";
		ss << "Advanced guaranteed measure: " << Lebesgue_parallel(advanced_guar_sol) << " in ";
		ss << ILebesgue(advanced_guar_sol) << "\n";
		ss << "Possible   max diameter: " << MaxDiam_parallel(sol) << "\n";
		ss << "Guaranteed max diameter: " << MaxDiam_parallel(guar_sol) << "\n";
		ss << "Advanced guaranteed max diameter: " << MaxDiam_parallel(advanced_guar_sol) << "\n";
		std::cout << ss.str();
	};

	void compute_stats() const;

	void stat_show() const;

	void exclude_Sobol_boxes (const problem_desc &p, const elem &e, tbb::concurrent_vector<elem> &box_set);

	int branch_and_bound (const problem_desc &p, const real &eps, create_func creat);
};


int use_Ncmp (const problem_desc &p, ivector &x, const imatrix &Jac, 
	      const std::vector< std::pair<int, int> > &pairs_for_Ncmp, ivector &x2, intvector &which, bool &singul);

//int use_GS (problem_desc &p, ivector &x, ivector &x_sel, imatrix A,
//	    const intvector &var_num, const rmatrix &Y, ivector &x2, intvector &which);

//int use_generalized_GS (const problem_desc &p, ivector &x, imatrix A, const intvector &var_num,
//			const intvector &eq_num, const rmatrix &Y, ivector &x2, intvector &, bool &);

int use_Krawczyk (const problem_desc &p, ivector &x, imatrix A, ivector b, const intvector &var_num,
		  const intvector &eq_num, const rmatrix &Y, ivector &x2, intvector &which);

int use_nosubm_GS (const problem_desc &p, elem &e, imatrix A, ivector b, const intvector &var_num,
		   const intvector &eq_num, const rmatrix &Y, ivector &x2, intvector &which, bool &singul);//, const int num_satisfied);

int use_Jacobi (const problem_desc &p, ivector &x, imatrix A, ivector b, const intvector &var_num,
		const intvector &eq_num, const rmatrix &Y, ivector &x2, intvector &which, bool &singul);

int compute_preconditioner (const imatrix &A, const ivector &x, rmatrix &Y, ivector &x_selected,
			    intvector &col_num, intvector &row_num);

int compute_LP_CW_preconditioner (const imatrix &A,  const ivector &x, rmatrix &Y,
				  intvector &col_num, intvector &row_num);

void permute_preconditioner (const rmatrix &Y, const intvector &col_num, const intvector &row_num, rmatrix &YY);

int narrow_verified_solution (const problem_desc &p, const real &eps, elem &e, intvector &which, intvector &old_which);

int process_verified_box (const problem_desc &p, ivector &x, intvector &which,
			  std::vector< std::pair<int, int> > &old_pairs_for_Ncmp);


real norm (const ivector &x, int &n);
real norm_subvec (const ivector &x, const intvector &which, int &n);
int below_eps (const ivector &x, const real &eps);

int compute_num_satisfied (elem &e, const ivector &y_ineq, const problem_desc &p);

int GaussElim (rmatrix V, intvector &row_num, intvector &col_num);

int Herbort_create_pair_list (const imatrix &J, std::vector< std::pair<int, int> > &pairs_for_Ncmp);
//int Goulard_create_pair_list (imatrix &J, intmatrix &pairs_for_Ncmp, intmatrix &L2);
int GE_create_pair_list (const imatrix &J, std::vector< std::pair<int, int> > &pairs_for_Ncmp);
int Goulard_GE_create_pair_list (const imatrix &J, std::vector< std::pair<int, int> > &pairs_for_Ncmp);

int Goulard_compute_weight_matrix (const imatrix &J, rmatrix &W);

#endif

