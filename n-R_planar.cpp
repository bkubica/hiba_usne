
#include "solver.hpp"
#include "problem_desc_impl.hpp"

#include <string>

#define N 5
#define M 3

// N - variables, M - equations

#define x_fin 1.0
#define y_fin 1.0
#define phi_fin (M_PI/2)
#define l(i) 1.0

template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> eq_x (const adhc_vector<level, sparse_mode, n, T> &x) {
	adhc_ari<level, sparse_mode, n, T> result, phi;
	result = cxsc::real(-x_fin);
	phi = cxsc::real(0.0);
	for (int i = 1; i <= N; ++i) {
		phi += x[i];
		result += l(i)*cos(phi);
	}
	return result;
}


template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> eq_y (const adhc_vector<level, sparse_mode, n, T> &x) {
	adhc_ari<level, sparse_mode, n, T> result, phi;
	result = cxsc::real(-y_fin);
	phi = cxsc::real(0.0);
	for (int i = 1; i <= N; ++i) {
		phi += x[i];
		result += l(i)*sin(phi);
	}
	return result;
}


template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> eq_phi (const adhc_vector<level, sparse_mode, n, T> &x) {
	adhc_ari<level, sparse_mode, n, T> result;
	result = cxsc::real(-phi_fin);
	for (int i = 1; i <= N; ++i) result += x[i];
	return result;
}


template<int level, SparsityLevel sparse_mode, int n, class T>
struct n_r {
	adhc_ari<level, sparse_mode, n, T> operator() (const adhc_vector<level, sparse_mode, n, T> &x, const int i) const {
		switch(i) {
			case 1: return eq_x(x);
			case 2: return eq_y(x);
			default: return eq_phi(x);
		}
	};
};


#define SPARSITY SparsityLevel::highly_sparse


int main() {
#include "num_threads.hpp"
	opdotprec = 1;
	ivector x(N);
	x = interval (-M_PI, M_PI);
	real eps = 1e-6; // N = 3
	//eps = 0.5; // N = 6 (krotko)
	//eps = 2e-1; // N = 6 (dlugo)
#if (N == 5)
	eps = 2e-2; // N = 5
#endif
	//eps = 5e-3; // N = 4
	//eps = 1e-3; // N = 4 (tez mozna)
	//eps = 1e-4;
	problem_desc_impl<SPARSITY, N, M, n_r> p(x);
	solver s (num_threads);
	cout << "x0 = " << x << endl;
	create_func creat = Herbort_create_pair_list;
	//create_func creat = Goulard_create_pair_list;
	//create_func creat = GE_create_pair_list;
	//create_func creat = Goulard_GE_create_pair_list;
	s.branch_and_bound (p, eps, creat);
#if defined SHOW
	cout << "Possible: " << endl;
	for (size_t i = 0; i < s.sol.size(); ++i) {
		//cout << "i = " << i << endl;
		s.sol[i].show();
		{
			interval x_i (0.0), y_i (0.0), phi_i(0.0);
			//result = -x_fin;
			//phi_i = 0.0;
			for (int ii = 1; ii <= N; ++ii) {
				phi_i += s.sol[i].x[ii];
				x_i += l(ii)*cos(phi_i);
				y_i += l(ii)*sin(phi_i);
				cout << "joint " << ii << ": x = " << x_i << ", y = " << y_i << ", phi = " << phi_i << endl;
			}
		}
	}
	cout << endl << "Guaranteed: " << endl;
	for (size_t i = 0; i < s.guar_sol.size(); ++i) {
		s.guar_sol[i].show();
		{
			interval x_i (0.0), y_i (0.0), phi_i(0.0);
			//result = -x_fin;
			//phi_i = 0.0;
			for (int ii = 1; ii <= N; ++ii) {
				phi_i += s.guar_sol[i].x[ii];
				x_i += l(ii)*cos(phi_i);
				y_i += l(ii)*sin(phi_i);
				cout << "joint " << ii << ": x = " << x_i << ", y = " << y_i << ", phi = " << phi_i << endl;
			}
		}
	}
#endif
	s.show_results();
	s.stat_show();
	return 0;
}

