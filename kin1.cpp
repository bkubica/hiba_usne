
#include "solver.hpp"
#include "problem_desc_impl.hpp"

#include <string>

#define N 6
#define M 6

// N - variables, M - equations

template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> eq1 (const adhc_vector<level, sparse_mode, n, T> &x) {
	adhc_ari<level, sparse_mode, n, T> result;
	result = sin(x[2])*cos(x[5])*sin(x[6])-sin(x[3])*cos(x[5])*sin(x[6])-sin(x[4])*cos(x[5])*sin(x[6])+ cos(x[2])*cos(x[6])+cos(x[3])*cos(x[6])+cos(x[4])*cos(x[6])-0.4077;
	return result;
}

template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> eq2 (const adhc_vector<level, sparse_mode, n, T> &x) {
	adhc_ari<level, sparse_mode, n, T> result;
	result = cos(x[1])*cos(x[2])*sin(x[5])+cos(x[1])*cos(x[3])*sin(x[5])+cos(x[1])*cos(x[4])*sin(x[5])+sin(x[1])*cos(x[5])-1.9115;
	return result;
}

template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> eq3 (const adhc_vector<level, sparse_mode, n, T> &x) {
	adhc_ari<level, sparse_mode, n, T> result;
	result = sin(x[2])*sin(x[5])+sin(x[3])*sin(x[5])+sin(x[4])*sin(x[5])-1.9791;
	return result;
}

template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> eq4 (const adhc_vector<level, sparse_mode, n, T> &x) {
	adhc_ari<level, sparse_mode, n, T> result;
	result = cos(x[1])*cos(x[2])+cos(x[1])*cos(x[3])+cos(x[1])*cos(x[4])+cos(x[1])*cos(x[2])+ cos(x[1])*cos(x[3])+cos(x[1])*cos(x[2])-4.0616;
	return result;
}

template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> eq5 (const adhc_vector<level, sparse_mode, n, T> &x) {
	adhc_ari<level, sparse_mode, n, T> result;
	result = sin(x[1])*cos(x[2])+sin(x[1])*cos(x[3])+sin(x[1])*cos(x[4])+sin(x[1])*cos(x[2])+sin(x[1])*cos(x[3])+sin(x[1])*cos(x[2])-1.7172;
	return result;
}

template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> eq6 (const adhc_vector<level, sparse_mode, n, T> &x) {
	adhc_ari<level, sparse_mode, n, T> result;
	result = sin(x[2])+sin(x[3])+sin(x[4])+sin(x[2])+sin(x[3])+sin(x[2])-3.9701;
	return result;
}


template<int level, SparsityLevel sparse_mode, int n, class T>
struct kin1 {
	adhc_ari<level, sparse_mode, n, T> operator() (const adhc_vector<level, sparse_mode, n, T> &x, const int i) const {
		switch(i) {
			case 1: return eq1(x);
			case 2: return eq2(x);
			case 3: return eq3(x);
			case 4: return eq4(x);
			case 5: return eq5(x);
			default: return eq6(x);
		};
	};
};


#define SPARSITY SparsityLevel::highly_sparse


int main() {
#include "num_threads.hpp"
	opdotprec = 1;
	ivector x(N);
	x = interval (0.0, 2*M_PI);
	real eps = 1e-5;//1e-6;
	problem_desc_impl<SPARSITY, N, M, kin1> p(x);
	solver s (num_threads);
	cout << "x0 = " << x << endl;
	create_func creat = Herbort_create_pair_list;
	//create_func creat = Goulard_create_pair_list;
	//create_func creat = GE_create_pair_list;
	//create_func creat = Goulard_GE_create_pair_list;
	s.branch_and_bound (p, eps, creat);
#define SHOW
#if defined SHOW
	cout << "Possible: " << endl;
	for (auto &x : s.sol) {
		x.show();
	}
	cout << endl << "Guaranteed: " << endl;
	for (auto &x : s.guar_sol) {
		x.show();
	}
	std::cout << "========\n";
#endif
	s.show_results();
	s.stat_show();
	return 0;
}

