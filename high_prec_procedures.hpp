
#if !defined __HIGH_PREC_PROCEDURES_HPP__
#define __HIGH_PREC_PROCEDURES_HPP__

template <class T>
int use_nosubm_GS (const problem_desc &p, ivector &x, typename Types<T>::dense_matrix_type A,
                   typename Types<T>::dense_vector_type b, const intvector &var_num, const intvector &eq_num,
                   const rmatrix &Y, ivector &x2, intvector &which, bool &singuli); 
#endif

