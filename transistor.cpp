
#include "solver.hpp"
#include "problem_desc_impl.hpp"

#include <string>

#define N 9
#define M N

// N - variables, M - equations

template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> eq1 (const adhc_vector<level, sparse_mode, n, T> &x, const imatrix &g, const int num) {
	adhc_ari<level, sparse_mode, n, T> result;
	result = (1.0 - x[1]*x[2])*x[3]*(exp(x[5]*(T(g[1][num]) - T(g[3][num]*1e-3)*x[7] - T(g[5][num]*1e-3)*x[8])) - 1.0) - T(g[5][num]) + T(g[4][num])*x[2];
	return result;
}

template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> eq2 (const adhc_vector<level, sparse_mode, n, T> &x, const imatrix &g, const int num) {
	adhc_ari<level, sparse_mode, n, T> result;
	result = (1.0 - x[1]*x[2])*x[4]*(exp(x[6]*(T(g[1][num] - g[2][num]) - T(g[3][num]*1e-3)*x[7] + T(g[4][num]*1e-3)*x[9])) - 1.0) - T(g[5][num])*x[1] + T(g[4][num]);
	return result;
}

template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> eq3 (const adhc_vector<level, sparse_mode, n, T> &x) {
	adhc_ari<level, sparse_mode, n, T> result;
	//result = x[1] - x[3] - x[2]*x[4];
	result = x[1]*x[3] - x[2]*x[4];
	return result;
}


/*template<int level, SparsityLevel sparse_mode, int n, class ... Args>
struct transistor;

template<int level, SparsityLevel sparse_mode, int n, class T>
struct transistor<level, sparse_mode, n, cxsc::imatrix> {*/

template<int level, SparsityLevel sparse_mode, int n, class T, class ...>
struct transistor {
	const cxsc::imatrix &g;

	transistor (const cxsc::imatrix &g_) : g(g_) {//5, 4) {
		//g_ = imatrix(5, 4);
		/*g[1][1] = 0.485; g[1][2] = 0.752; g[1][3] = 0.869; g[1][4] = 0.982;
		g[2][1] = 0.369; g[2][2] = 1.254; g[2][3] = 0.703; g[2][4] = 1.455;
		g[3][1] = 5.2095; g[3][2] =  10.0677; g[3][3] = 22.9274; g[3][4] = 20.2153;
		g[4][1] = 23.3037; g[4][2] = 101.779; g[4][3] = 111.461; g[4][4] = 191.267;
		g[5][1] = 28.5132; g[5][2] =  111.8467; g[5][3] = 134.3884; g[5][4] = 211.4823;*/
		//cout << level << ", g = " << g << "\n";
	};

	adhc_ari<level, sparse_mode, n, T> operator() (const adhc_vector<level, sparse_mode, n, T> &x, const int i) const {
		if (i <= 4) return eq1(x, g, i);
		else if (i <= 8) return eq2(x, g, i - 4);
		else if (i == 9) return eq3(x);
		else throw std::invalid_argument("transistor(x, " + std::to_string(i) + ")");
		//else exit (-1);
	};
};


//#define SPARSITY SparsityLevel::dense
//#define SPARSITY sparse
#define SPARSITY SparsityLevel::highly_sparse
//#define SPARSITY SparsityLevel::another_sparse


int main() {
#include "num_threads.hpp"
	opdotprec = 1;
	ivector x(N);
	x = interval (0.0, 10.0);
	static cxsc::imatrix g(5, 4); // params of the problem
	{
		g[1][1] = 0.485; g[1][2] = 0.752; g[1][3] = 0.869; g[1][4] = 0.982;
		g[2][1] = 0.369; g[2][2] = 1.254; g[2][3] = 0.703; g[2][4] = 1.455;
		g[3][1] = 5.2095; g[3][2] =  10.0677; g[3][3] = 22.9274; g[3][4] = 20.2153;
		g[4][1] = 23.3037; g[4][2] = 101.779; g[4][3] = 111.461; g[4][4] = 191.267;
		g[5][1] = 28.5132; g[5][2] =  111.8467; g[5][3] = 134.3884; g[5][4] = 211.4823;
	}
	//x = interval (-1.0, 1.0);
	real eps = 1e-8;
	problem_desc_impl<SPARSITY, N, M, transistor, 0, default_fun_type, const imatrix &> p(x, g);
	//for(int i = 1; i <=p.num_fun; ++i) p.rhs[i] += interval(-1e-2, 1e-2);
	solver s (num_threads);
	cout << "x0 = " << x << endl;
	create_func creat = Herbort_create_pair_list;
	//create_func creat = Goulard_create_pair_list;
	//create_func creat = GE_create_pair_list;
	//create_func creat = Goulard_GE_create_pair_list;
	s.branch_and_bound (p, eps, creat);
#define SHOW
#if defined SHOW
	cout << "Possible: " << endl;
	for (auto &x : s.sol) {
		x.show();
	}
	cout << endl << "Guaranteed: " << endl;
	for (auto &x : s.guar_sol) {
		x.show();
	}
	std::cout << "========\n";
#endif
	s.show_results();
	s.stat_show();
	return 0;
}

