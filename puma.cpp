
#include "solver.hpp"
#include "problem_desc_impl.hpp"

#include <string>

#define N 8
#define M 7

// N - variables, M - equations

template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> eq1 (const adhc_vector<level, sparse_mode, n, T> &x) {
	adhc_ari<level, sparse_mode, n, T> result;
	result = sqr(x[1]) + sqr(x[2]) - 1.0;
	return result;
}

template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> eq2 (const adhc_vector<level, sparse_mode, n, T> &x) {
	adhc_ari<level, sparse_mode, n, T> result;
	result = (sqr(x[3]) + sqr(x[4]) - 1.0);
	return result;
}

template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> eq3 (const adhc_vector<level, sparse_mode, n, T> &x) {
	adhc_ari<level, sparse_mode, n, T> result;
	result = (sqr(x[5]) + sqr(x[6]) - 1.0);
	return result;
}

template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> eq4 (const adhc_vector<level, sparse_mode, n, T> &x) {
	adhc_ari<level, sparse_mode, n, T> result;
	result = (sqr(x[7]) + sqr(x[8]) - 1.0);
	return result;
}

template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> eq5 (const adhc_vector<level, sparse_mode, n, T> &x) {
	adhc_ari<level, sparse_mode, n, T> result;
	result = 0.004731*x[1]*x[3] - 0.3578*x[2]*x[3] - 0.1238*x[1] - 0.001637*x[2] - 0.9338*x[4] + x[7];
	return result;
}

template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> eq6 (const adhc_vector<level, sparse_mode, n, T> &x) {
	adhc_ari<level, sparse_mode, n, T> result;
	result = 0.2238*x[1]*x[3] + 0.7623*x[2]*x[3] + 0.2638*x[1] - 0.07745*x[2] -0.6734*x[4] -0.6022;
	return result;
}

template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> eq7 (const adhc_vector<level, sparse_mode, n, T> &x) {
	adhc_ari<level, sparse_mode, n, T> result;
	result = x[6]*x[8] + 0.3578*x[1] + 0.004731*x[2];
	return result;
}

template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> eq8 (const adhc_vector<level, sparse_mode, n, T> &x) {
	adhc_ari<level, sparse_mode, n, T> result;
	result = -0.7623*x[1] + 0.2238*x[2] + 0.3461;
	return result;
}


template<int level, SparsityLevel sparse_mode, int n, class T>
struct puma {
	puma() {};

	adhc_ari<level, sparse_mode, n, T> operator() (const adhc_vector<level, sparse_mode, n, T> &x, const int i) const {
		switch(i) {
			case 1: return eq1(x);
			case 2: return eq2(x);
			case 3: return eq3(x);
			case 4: return eq4(x);
			case 5: return eq5(x);
			case 6: return eq6(x);
			case 7: return eq7(x);
			case 8: return eq8(x);
		}
		std::terminate();
	};
};


//#define SPARSITY SparsityLevel::dense
//#define SPARSITY sparse
#define SPARSITY SparsityLevel::highly_sparse


int main (void) {
#include "num_threads.hpp"
	opdotprec = 1;
	ivector x(N);
	x = interval (-1.0, 1.0);
	real eps = 5e-2;//1e-2;//*1e-6;/ */1e-4;//1e-1;
#if (M > 6)
	eps = 1e-6;
#endif
	//eps = 1e-4;
	//eps = 1e-1;
	//eps = 1e-8;
	problem_desc_impl<SPARSITY, N, M, puma> p(x);
	solver s (num_threads);
	cout << "x0 = " << x << endl;
	create_func creat = Herbort_create_pair_list;
	//create_func creat = Goulard_create_pair_list;
	//create_func creat = GE_create_pair_list;
	//create_func creat = Goulard_GE_create_pair_list;
	s.branch_and_bound (p, eps, creat);
//#define SHOW
#if defined SHOW
	cout << "Possible: " << endl;
	for (auto &x : s.sol) {
		x.show();
	}
	cout << endl << "Guaranteed: " << endl;
	for (auto &x : s.guar_sol) {
		x.show();
	}
	std::cout << "========\n";
#endif
	s.show_results();
	s.stat_show();
	return 0;
}

