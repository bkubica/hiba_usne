
#include "solver.hpp"
#include "problem_desc_impl.hpp"

#include <string>

#define N 5
#define M 2

// N - variables, M - equations

template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> eq1 (const adhc_vector<level, sparse_mode, n, T> &x) {
	adhc_ari<level, sparse_mode, n, T> result;
	result = sqr(x[1]) + sqr(x[2]) + sqr(x[3]) + sqr(x[4]) + sqr(x[5]) - 1.0;
	//result = sqr(x[1]) + sqr(x[2]) + sqr(x[3]) + sqr(x[4]) + sqr(x[1] + x[2] + x[3] + x[4]) - 1.0;
	//result = 2*(sqr(x[1]) + sqr(x[2]) + sqr(x[3]) + sqr(x[4]) + x[1]*(x[2] + x[3] + x[4]) + x[2]*(x[3] + x[4]) +x[3]*x[4]) - 1.0;
	//-result = 4.0 - 2.0*sqr(x[1] - 1.0);
	return result;
}

template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> eq2 (const adhc_vector<level, sparse_mode, n, T> &x) {
	adhc_ari<level, sparse_mode, n, T> result;
	result = x[1];
	for (int i = 2; i <= N; ++i) result += x[i];
	//result = (2.0 - sqr(x[1] + 1.0))*(2.0 - sqr(x[2] - 1.0));
	return result;
}


template<int level, SparsityLevel sparse_mode, int n, class T>
struct alexandre_under {
	adhc_ari<level, sparse_mode, n, T> operator() (const adhc_vector<level, sparse_mode, n, T> &x, const int i) const {
		return (i == 1)?eq1(x):eq2(x);
	};
};


//#define SPARSITY SparsityLevel::dense
//#define SPARSITY sparse
#define SPARSITY SparsityLevel::highly_sparse


int main() {
#include "num_threads.hpp"
	opdotprec = 1;
	ivector x(N);
	x[1] = x[2] = interval (-1.0, 1.0);
	x[3] = interval (-0.7, 0.7);
	x[4] = interval (-0.8, 0.8);
	x[5] = interval (-2.0, 2.0);
	//x = cxsc::interval(-1.0, 1.0);
	real eps = 1e-5;//1e-6;
	eps = 1.0;
	eps = 1e-1;
	eps = 5e-2;
	//eps = 2e-2;
	//eps = 1e-3;
	//eps = 1e-7;
	problem_desc_impl<SPARSITY, N, M, alexandre_under> p(x);
	solver s (num_threads);
	cout << "x0 = " << x << endl;
	create_func creat = Herbort_create_pair_list;
	//create_func creat = Goulard_create_pair_list;
	//create_func creat = GE_create_pair_list;
	//create_func creat = Goulard_GE_create_pair_list;
	s.branch_and_bound (p, eps, creat);
//#define SHOW
#if defined SHOW
	cout << "Possible: " << endl;
	for (auto &x : s.sol) {
		x.show();
	}
	cout << endl << "Guaranteed: " << endl;
	for (auto &x : s.guar_sol) {
		x.show();
	}
	std::cout << "========\n";
#endif
	s.show_results();
	s.stat_show();
	return 0;
}

