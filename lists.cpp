
//#include <tbb/parallel_for.h>
#include <tbb/parallel_reduce.h>
#include <tbb/partitioner.h>
//#include <tbb/spin_mutex.h>

#include "lists.hpp"
#include "solver.hpp"


template <typename T>
void save_bounds (ofstream &outfile, const ivector &x0) {}

/*template <elem>
void save_bounds (ofstream &outfile, const ivector &x0) {}*/

template <> void save_bounds<elem_guaranteed> (ofstream &outfile, const ivector &x0) {
	outfile << Inf(x0[1]) << Sup(x0[1]) << endl << Inf(x0[2]) << Sup(x0[2]) << endl << endl;
}

template <typename T>
void save_cairo (const tbb::concurrent_vector<T> &vec, const ivector &x0, const char *name) {
	ofstream outfile (name, ios::out);
	//T *cur;
	size_t i = 0, end = vec.size();
	save_bounds<T> (outfile, x0);
	//outfile << Inf(x0[1]) << endl << Sup(x0[1]) << endl << Inf(x0[2]) << Sup(x0[2]) << endl << endl;
	while (i < end) {
		const T *cur = &(vec.at(i++));
		outfile << Inf(cur->x[1]) << "\t" << Sup(cur->x[1]) << "\t" << Inf(cur->x[2]) << "\t" << Sup(cur->x[2]) << endl;
		/*outfile << Inf(cur->x[1]) << "\t" << Inf(cur->x[2]) << "\t" << Inf(cur->x[1]) << "\t" << Sup(cur->x[2]) << endl;
		outfile << Inf(cur->x[1]) << "\t" << Sup(cur->x[2]) << "\t" << Sup(cur->x[1]) << "\t" << Sup(cur->x[2]) << endl;
		outfile << Sup(cur->x[1]) << "\t" << Sup(cur->x[2]) << "\t" << Sup(cur->x[1]) << "\t" << Inf(cur->x[2]) << endl;
		outfile << Sup(cur->x[1]) << "\t" << Inf(cur->x[2]) << "\t" << Inf(cur->x[1]) << "\t" << Inf(cur->x[2]) << endl;*/
	}
	outfile.close ();
}



bool sufficiently_reduced (const ivector &x_new, const ivector &x_old) {
	real maxval = diam(x_old[1]) - diam(x_new[1]), val;
	for (int i = 2, i_max = VecLen(x_new); i <= i_max; ++i) {
		if ((val = diam(x_old[i]) - diam(x_new[i])) > maxval) maxval = val;
	}
	int k;
	//return (norm (x_new, k)/4 - maxval < 0);
	return (norm (x_old, k)/4 <= maxval);
}


//real Lebesgue (elem &e) {
real Lebesgue (const ivector &x) {
	real leb (1.0);
	int i = 1;
	do {
		leb *= diam(x[i]);
	} while (++i <= VecLen(x));
	return leb;
}


interval ILebesgue (const elem &e) {
	interval leb (1.0);
	int i = 1;
	do {
		leb *= interval(diam(e.x[i]));
	} while (++i <= VecLen(e.x));
	return leb;
}


template<typename T>
real Lebesgue (const tbb::concurrent_vector<T> &vec) {
	real result (0.0);
	size_t i = 0, i_end = vec.size();
	while (i < i_end) {
		result += Lebesgue(vec.at(i++).x);
	}
	return result;
}


template <typename T>
interval ILebesgue (const tbb::concurrent_vector<T> &vec) {
	interval result (0.0);
	size_t i = 0, i_end = vec.size();
	while (i < i_end) {
		result += ILebesgue(vec.at(i++));
	}
	return result;
}


template <typename T>
class LebBody {
	const tbb::concurrent_vector<T> *vec_ptr;
	real result;
public:
	void operator () (const tbb::blocked_range<int> &r) {
		real localSum (0.0);
		size_t i = r.begin();
		size_t i_end = r.end();
		while (i != i_end) {
			localSum += Lebesgue ((*vec_ptr).at(i++).x);
		} 
		result += localSum;
	};

	void join (const LebBody &leb) {
		result += leb.result;
	};

	real get_result() {
		return result;
	};

	LebBody (const tbb::concurrent_vector<T> *vec_ptr_) : vec_ptr(vec_ptr_), result(0.0) {};

	LebBody (const LebBody &leb, tbb::split) : vec_ptr(leb.vec_ptr), result(0.0) {};
};



template<typename T>
real Lebesgue_parallel (const tbb::concurrent_vector<T> &vec) {
	LebBody<T> body(&vec);
	tbb::parallel_reduce (tbb::blocked_range<int> (0, vec.size()), body);//, part);
	return body.get_result();
}


template<typename T>
interval ILebesgue_parallel (const tbb::concurrent_vector<T> &vec) {
	const interval initial_value (0.0);
	//tbb::spin_mutex sum_lock;
	//tbb::parallel_for (tbb::blocked_range<int> (0, vec.size()), MaxDiamBody<T>(&vec, &result, &sum_lock));
	return tbb::parallel_reduce (blocked_range<int>(0, vec.size()), initial_value,
        		[&](const blocked_range<int> &r, interval res)->interval {
				for (int i = r.begin(), i_end = r.end(); i != i_end; ++i) res += ILebesgue(vec[i]);
           			return res;
        		},
			[](const interval x, const interval y )->interval {
				return x + y;
			}
	);
}


template<typename T>
real MaxDiam_parallel (const tbb::concurrent_vector<T> &vec) {
	const real initial_value = 0.0;
	//tbb::parallel_for (tbb::blocked_range<int> (0, vec.size()), ILebBody<T>(&vec, &result, &sum_lock), part);
	return tbb::parallel_reduce (blocked_range<int>(0, vec.size()), initial_value,
        		[&](const blocked_range<int> &r, real max_diam)->real {
				real tmp;
				int k;
				for (int i = r.begin(), i_end = r.end(); i != i_end; ++i) {
					if ((tmp = norm (vec[i].x, k)) > max_diam) max_diam = tmp;
				}	
           			return max_diam;
        		},
			[](const real x, const real y )->real {
				return (x >= y)?x:y;
			}
	);
}


// requesting instantiations - necessary on g++ and some other compilers

template void save_cairo (const tbb::concurrent_vector<elem> &vec, const ivector &x0, const char *name);
template void save_cairo (const tbb::concurrent_vector<elem_guaranteed> &vec, const ivector &x0, const char *name);

template real Lebesgue (const tbb::concurrent_vector<elem> &vec);
template real Lebesgue (const tbb::concurrent_vector<elem_guaranteed> &vec);

template interval ILebesgue (const tbb::concurrent_vector<elem> &vec);
template interval ILebesgue (const tbb::concurrent_vector<elem_guaranteed> &vec);


template real Lebesgue_parallel (const tbb::concurrent_vector<elem> &vec);
template real Lebesgue_parallel (const tbb::concurrent_vector<elem_guaranteed> &vec);

template interval ILebesgue_parallel (const tbb::concurrent_vector<elem> &vec);
template interval ILebesgue_parallel (const tbb::concurrent_vector<elem_guaranteed> &vec);

template real MaxDiam_parallel (const tbb::concurrent_vector<elem> &vec);
template real MaxDiam_parallel (const tbb::concurrent_vector<elem_guaranteed> &vec);

