
#if defined LIN_PROG

#include "LP.hpp"
#include "scoped_lock.hpp"

#include <lop.hpp>
#include <rev_simp.hpp>

#include <glpk.h>

//#include "tbb/mutex.h"

extern LP_mutex LP_mut;

int LP_narrowing (ivector &x, ivector &y, imatrix &J, ivector &xc, ivector &yc, imatrix &Jc) {
	// the function should return:
	// 	-1, if no solution can be in the box
	// 	 0, otherwise
	int n_x = VecLen(x), n_y = VecLen(y);
	/*rmatrix A (2*n_y + n_x, 2*n_y + 2*n_x);//= mid(Jc);
	rvector b (2*n_y + n_x);
	rvector c (2*n_y + 2*n_x);
	//cout << "x = " << x << endl;
	c = 0.0;
	A = 0.0;
	int i = 1;
	for (; i <= n_y; ++i) {
		for (int j = 1; j <= n_x; ++j) {
			A[i][j] = mid(Jc[i][j]);
			A[n_y + i][j] = -A[i][j];//-Jc[i][j];
		}
	}
	for (i = 1; i <= n_x; ++i) A[2*n_y + i][i] = 1.0;
	for (i = 1; i <= (2*n_y + n_x); ++i) A[i][n_x + i] = 1.0;*/
	ivector v = x - xc;
	ivector pom = (J - Jc)*v + yc; // a moze ``- yc'' ?????????????????????????????
	/*for (i = 1; i <= n_y; ++i) {
		b[i] = -Inf(pom[i]);
		b[n_y + i] = Sup(pom[i]);
	}
	//rvector x_ (2*n_y + 2*n_x);
	rvector v_ (2*n_y + 2*n_x);
	v_ = 0.0;
	for (i = 1; i <= n_x; ++i) {
		b[2*n_y + i] = Sup(v[i]);
		v_[i] = Inf(v[i]);
	}*/
	/*cout << "b = " << b << endl;
	cout << "x_ = " << x_ << endl;*/
	/*b -= A*v_;
	cout << "Jc = " << Jc << endl;
	cout << "A = " << A << endl;
	cout << "b = " << b << endl;
	cout << "c = " << c << endl;*/
	glp_prob *prob;
	{
		//LP_mutex::scoped_lock sc_lock(LP_mut);
		scoped_lock_of<LP_mutex> sc_lock{LP_mut};
		//glp_term_out (GLP_OFF);
		prob = glp_create_prob();
		glp_add_rows (prob, 2*n_y);//ColLen(A));
		glp_add_cols (prob, n_x);//RowLen(A));
	}
	/*glp_set_col_name (prob, 1, "x1");
	glp_set_col_name (prob, 2, "x2");
	glp_set_col_name (prob, 3, "x3");
	glp_set_col_name (prob, 4, "x4");*/
	double *A_row = new double [n_x + 1];
	int *ind_row = new int [n_x + 1];
	int i = 1;
	//cout << "n_x = " << n_x << endl;
	//cout << "ind_row = ";
	for (; i <= n_x; ++i) {
		ind_row[i] = i;
		//cout << " " << ind_row[i];
	}
	//cout << endl;
	//cout << "A_row = ";
	for (i = 1; i <= n_y; ++i) {
		for (int j = 1; j <= n_x; ++j) {
			A_row[j] = _double(mid(Jc[i][j]));
			//cout << " " << A_row[j];
		}
		//cout << endl;
		LP_mutex::scoped_lock sc_lock(LP_mut);
		glp_set_mat_row (prob, i, n_x, ind_row, A_row);
		glp_set_row_bnds (prob, i, GLP_UP, 0.0, _double(-Inf(pom[i])));
	}
	for (i = 1; i <= n_y; ++i) {
		for (int j = 1; j <= n_x; ++j) {
			A_row[j] = -_double(mid(Jc[i][j]));
		}
		//LP_mutex::scoped_lock sc_lock(LP_mut);
		scoped_lock_of<LP_mutex> sc_lock{LP_mut};
		glp_set_mat_row (prob, n_y + i, n_x, ind_row, A_row);
		glp_set_row_bnds (prob, n_y + i, GLP_UP, 0.0, _double(Sup(pom[i])));
	}
	delete [] A_row;
	delete [] ind_row;
	for (i = 1; i <= n_x; ++i) {
		//LP_mutex::scoped_lock sc_lock(LP_mut);
		scoped_lock_of<LP_mutex> sc_lock{LP_mut};
		glp_set_col_bnds (prob, i, GLP_DB, _double(Inf(v[i])), _double(Sup(v[i])));
		glp_set_obj_coef (prob, i, 0.0);
	}
	for (i = 1; i <= n_x/*VecLen(x)*/; ++i) {
		//cout << "i = " << i << endl;
		//c[i] = 1.0;
		real vmin, vmax, z_;
		//real xmin, xmax;
		{
			//LP_mutex::scoped_lock sc_lock(LP_mut);
			scoped_lock_of<LP_mutex> sc_lock{LP_mut};
			glp_set_obj_coef (prob, i, 1.0);
			glp_set_obj_dir (prob, GLP_MAX);
			glp_adv_basis (prob, 0);
			glp_simplex (prob, nullptr);
			z_ = glp_get_obj_val (prob);
		}
		interval z;
		/*int sol_num, err;
		intvector B_start_vector;
		interval z;
		intmatrix V;
		imatrix X;
		rvector v_;
		real z_;
		RevSimplex (A, b, c, v_, B_start_vector, z_, err);*/
		/*if (err) {
			cout << "err = " << err << endl;
			if (err == 3) continue;
			else return -1;
		}
		else cout << "ok\n";*/
		//if (z_ <  diam(v[i])) cout << "z_ < diam\n";
		/*cout << "err = " << err << endl;
		cout << "z_ = " << z_ << endl;
		cout << "v_ = " << v_ << endl;
		cout << "B_start_vector = " << B_start_vector << endl;*/
		z = z_; // ??????????
		//LinOpt (A, b, c, B_start_vector, z, V, X, sol_num, err);
		/*cout << "err = " << err << endl;
		cout << "V = " << V << endl;
		cout << "X = " << X << endl;
		cout << "z = " << z << endl;*/
		//cout << "xmax = " << Sup(z + Inf(x[i])) << endl;
		//if ((vmax = Sup(z + Inf(v[i]))) >= Inf(v[i])) SetSup(v[i], min(vmax, Sup(v[i])));
		if ((vmax = Sup(z)) >= Inf(v[i])) SetSup(v[i], min(vmax, Sup(v[i])));
		else return -1;
		//c = -c;
		//c[i] = -1.0;
		{
			//LP_mutex::scoped_lock sc_lock(LP_mut);
			scoped_lock_of<LP_mutex> sc_lock{LP_mut};
			glp_set_obj_dir (prob, GLP_MIN);
			glp_adv_basis (prob, 0);
			glp_simplex (prob, nullptr);
			z_ = glp_get_obj_val (prob);
		}
		/*RevSimplex (A, b, c, v_, B_start_vector, z_, err);
		if (err) {
			cout << "err = " << err << endl;
			if (err == 3) continue;
			else return -1;
		}
		else cout << "ok\n";*/
		//if (z_ > 0) cout << "z_ > 0\n";
		/*cout << "\nerr = " << err << endl;
		cout << "z_ = " << z_ << endl;
		cout << "v_ = " << v_ << endl;
		cout << "B_start_vector = " << B_start_vector << endl;*/
		z = z_; // ??????????????
		//LinOpt (A, b, c, B_start_vector, z, V, X, sol_num, err);
		/*cout << "err = " << err << endl;
		cout << "V = " << V << endl;
		cout << "X = " << X << endl;
		cout << "z = " << z << endl;*/
		//cout << "xmin = " << Inf(z + Inf(x[i])) << endl;
		//if ((vmin = Inf(z + Inf(v[i]))) <= Sup(v[i])) SetInf(v[i], max(vmin, Inf(v[i])));
		if ((vmin = Inf(z)) <= Sup(v[i])) SetInf(v[i], max(vmin, Inf(v[i])));
		else return -1;
		{
			//LP_mutex::scoped_lock sc_lock(LP_mut);
			scoped_lock_of<LP_mutex> sc_lock{LP_mut};
			glp_set_obj_coef (prob, i, 0.0);
		}
		//c = -c;
		//c[i] = 0.0;
	}
	{
		//LP_mutex::scoped_lock sc_lock(LP_mut);
		scoped_lock_of<LP_mutex> sc_lock{LP_mut};
		glp_delete_prob (prob);
	}
	//for (i = 1; i <= n_x; ++i) x[i] &= (v[i] + xc[i]);
	for (i = 1; i <= n_x; ++i) x[i] = v[i] + xc[i];
	//cout << "x = " << x << endl;
	return 0;
}
#endif

