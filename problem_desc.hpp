
#if !defined __PROBLEM_DESC_HPP__
#define __PROBLEM_DESC_HPP__

#include <ivector.hpp>
#include <l_interval.hpp>
#include <l_ivector.hpp>
#include <l_imatrix.hpp>
#include <xscclass.hpp>


using namespace cxsc;
using namespace std;


struct problem_desc {
	cxsc::ivector x0;
	const int num_fun;
	const int num_ineq;
	cxsc::ivector rhs;

	problem_desc (const ivector &x0_, const int num_f, const int num_ineq_=0) : x0(x0_), num_fun(num_f), num_ineq(num_ineq_), rhs(num_ineq) {
		if (num_ineq > 0) rhs = cxsc::interval(0.0);
	}

	problem_desc (const ivector &x0_, const int num_f, const ivector &rhs_) : x0(x0_), num_fun(num_f), num_ineq(VecLen(rhs)), rhs(rhs_) {}

	problem_desc (const problem_desc &) = delete;

	virtual ~problem_desc () {/*cout << "destruktor bazowy\n";*/};


	virtual void compute_f (const ivector &x, const int i, interval &y) const = 0;

	virtual void compute_grad_f (const ivector &x, const int i, interval &y, ivector &g) const = 0;

	virtual void compute_hess_f (const ivector &x, const int i, interval &y, ivector &g, imatrix &H) const = 0;

	virtual void compute_all_f (const ivector &x, ivector &y) const = 0;
	
	virtual void compute_all_grad_f (const ivector &x, ivector &y, imatrix &g) const = 0;

	virtual void compute_f_high_prec (const l_ivector &x, const int i, l_interval &y) const = 0;

	virtual void compute_grad_f_high_prec (const l_ivector &x, const int i, l_interval &y, l_ivector &g) const = 0;

	virtual void compute_all_f_high_prec (const l_ivector &x, l_ivector &y) const = 0;
	
	virtual void compute_all_grad_f_high_prec (const l_ivector &x, l_ivector &y, l_imatrix &g) const = 0;


	virtual void compute_ineq (const ivector &x, const int i, interval &y) const = 0;

	virtual int compute_all_ineq (const ivector &x, const intvector &satisfied, ivector &y) const = 0;

	virtual int compute_all_grad_ineq (const ivector &x, const intvector &satisfied, ivector &y, imatrix &g) const = 0;


	virtual int compute_all_ineq_high_prec (const l_ivector &x, const intvector &satisfied, l_ivector &y) const = 0;
	
	virtual int compute_all_grad_ineq_high_prec (const l_ivector &x, const intvector &satisfied, l_ivector &y, l_imatrix &g) const = 0;


	virtual int hc_initial_evaluate (const ivector &x, const int i) const = 0;

	virtual void hc_init_thread() const = 0;

	//virtual int hc_narrow(ivector &x, const int i) const = 0;
	//virtual int hc_narrow(ivector &x, const int i, intvector &which) const = 0;
	virtual int hc_narrow(ivector &x, const int i, intvector &which, const interval &rhs_i) const = 0;

	virtual cxsc::intmatrix count_variable_occurrences_in_equations() const = 0;
};

#endif

