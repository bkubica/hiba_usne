
#include <ivector.hpp>

#include <l_interval.hpp>
#include <l_ivector.hpp>
#include <l_imatrix.hpp>

class itensor;
class sitensor;
class citensor;
class scitensor;
class l_itensor;
class rtensor;
class srtensor;
class ctensor;
class sctensor;
class l_ctensor;


#include <typemanip.hpp>
#include <type_traits.hpp>

#include "problem_desc.hpp"
#include "high_prec_procedures.hpp"

/*int use_Ncmp (const problem_desc &p, ivector &x, const imatrix &Jac, 
	      const std::vector< std::pair<int, int> > &pairs_for_Ncmp, ivector &x2, intvector &which, bool &singul) {
	//cout << "use_Ncmp\n";
	int eq_num, var_num;
	interval yc;
	ivector xc = x;
	real c;
	which = 1;
	//for (int i = 0; i < pairs_for_Ncmp.size(); ++i) {
	for (std::pair<int, int> eq_var : pairs_for_Ncmp) {
		eq_num = eq_var.first;
		var_num = eq_var.second;
		//cout << "eq_num = " << eq_num << endl;
		//cout << "var_num = " << var_num << endl;
		xc[var_num] = c = (Inf(x[var_num]) + Sup(x[var_num]))/2;
		p.compute_f (xc, eq_num, yc); ++stat->num_f;
		if (!in(0.0, Jac[eq_num][var_num])) {//continue;
			interval x_new = xc[var_num] - yc/Jac[eq_num][var_num];
			if (Disjoint(x_new, x[var_num])) {
				//cout << "rozlaczne\n";
				//stat->num_del_newt++;
				return -1;
			}
			if (in(x_new, x[var_num])) {
				x[var_num] = x_new;
				//which[var_num] = 0;
			}
			else {
				x[var_num] &= x_new;
			}
		}
		else {
			if (in(0.0, yc)) { xc[var_num] = x[var_num]; singul = true;  continue; } // bo byloby 0/0...
			xinterval extend = c - yc % Jac[eq_num][var_num];
			//cout << "obliczono extend\n";
			ivector v = x[var_num] & extend;
			if (v[1] == EmptyIntval()) {
				//stat->num_del_newt++;
				return -1;
			}
			else if (v[2] != EmptyIntval()) {
				{
				//if (Inf(v[2]) - Sup(v[1]) >= 1e-3) {
				//if (Sup(v[1]) < Inf(v[2])) {
					x[var_num] = v[1];
					x2 = x;
					x2[var_num] = v[2];
					//cout << "x1 = " << x << endl;
					//cout << "x2 = " << x2 << endl;
					return 3;
				}
				
			}
			else {
				x[var_num] = v[1];
			}
		}
		xc[var_num] = x[var_num];
	}
	return 0;
}*/


template 
int use_nosubm_GS<cxsc::l_interval> (const problem_desc &p, ivector &x, typename Types<cxsc::l_interval>::dense_matrix_type A,
		   typename Types<cxsc::l_interval>::dense_vector_type b, const intvector &var_num, const intvector &eq_num,
		   const rmatrix &Y, ivector &x2, intvector &which, bool &singuli);

template <class T>
int use_nosubm_GS (const problem_desc &p, ivector &x, typename Types<T>::dense_matrix_type A,
		   typename Types<T>::dense_vector_type b, const intvector &var_num, const intvector &eq_num,
		   const rmatrix &Y, ivector &x2, intvector &which, bool &singuli) {
//int use_nosubm_GS (const problem_desc &p, typename adhc::Types<T>::dense_vector_type &x,
//		   typename adhc::Types<T>::dense_matrix_type A, 
//		   typename adhc::Types<T>::dense_vector_type b, const intvector &var_num,
//		   const intvector &eq_num, const rmatrix &Y,
//		   typename adhc::Types<T>::dense_vector_type &x2, intvector &which, bool &singul) {
	//cout << "use_nosubm_GS\n";
	//cout << " x = " << x << endl;
	/*cout << " var_num = " << var_num << endl;
	cout << " eq_num = " << eq_num << endl;
	//cout << " which = " << which << endl;
	cout << " A = " << A << endl;
	cout << " Y = " << Y << endl;*/
	typename Types<T>::dense_vector_type long_x{x};
	cxsc::rvector c = (Inf(x) + Sup(x))*0.5;
	//cxsc::ivector xc{c}, yc;
	//p.compute_all_f (xc, yc); //----stat->num_f += p.num_fun;

	typename Types<T>::dense_vector_type long_xc;
	//long_xc = (Inf(long_x) + Sup(long_x))*0.5;
	long_xc = typename Types<T>::dense_vector_type{c};
	//typename Types<T>::dense_vector_type long_yc;
	//long_yc = yc;
	//cxsc::l_imatrix long_A{A};
	//cxsc::l_ivector long_b{b};
	//cxsc::l_interval long_x2;
	int i = 1, j, rows_num = ColLen(A), cols_num = RowLen(A);
	//rvector xc = (Inf(x) + Sup(x))/2;
	typename Types<T>::dense_vector_type v = long_x - long_xc;
	//cout << " v = " << v << endl;
	T x_new;
	//xinterval extend;
	//ivector vec(2);
	int variable, equation;
	A = Y*A;//left_mult (Y, A);
	b = Y*b;//left_mult (Y, b);
	//ivector xxc (x), yyc;
	which = 1;
	//cout << "A = " << A << endl;
	//cout << "b = " << b << endl;
	//cout << "v = " << v << endl;
	do {
		//cout << "i = " << i << endl;
		variable = var_num[i];
		equation = eq_num[i];
		/*cout << "variable = " << variable << endl;
		cout << "eq = " << equation << endl;
		cout << "A[ij]*v[j] = " << A[equation][1]*v[1] << endl;
		cout << "b[j]+A[ij]*v[j] = " << b[1]+A[equation][1]*v[1] << endl;*/
		T sum{b[equation]};
		for (j = 1; j < variable; ++j) sum += A[equation][j]*v[j];
		for (j = variable + 1; j <= cols_num; ++j) sum += A[equation][j]*v[j];
		//if (norm(x, j) <= 1e-1) { //j unused
		/*if (1){//below_eps(x, 1e-1) >= (cols_num - rows_num)) { //lower or higher ???
			for (j = 1; j < variable; j++) sum += A[equation][j]*v[j];
			for (j = variable + 1; j <= cols_num; j++) sum += A[equation][j]*v[j];
		}
		else {
			for (int ii = 1; ii <= rows_num; ii++) xxc[var_num[ii]] = xc[var_num[ii]];
			p.compute_all_f (xxc, yyc); //stat->num_f += rows_num;
			left_mult (Y, yyc);
			sum = yyc[equation];
			for (j = 1; j <= rows_num; j++)
				if (var_num[j] != variable) sum += A[equation][var_num[j]]*v[var_num[j]];
		}*/
		/*interval sum (yyc[equation]);
		for (j = 1; j <= rows_num; j++) if (var_num[j] != variable) sum += A[equation][var_num[j]]*v[var_num[j]];*/
		//sum = yyc[equation];
		//cout << sum  << " == " << yyc[equation] << endl;//+ A[1][3 - variable]*v[3 - variable] << endl;
		if (!in (0.0, A[equation][variable])) {
			//cout << "notin\n";
			x_new = long_xc[variable] - sum/A[equation][variable];
			//x_new = xc[variable] - yyc[equation]/A[equation][variable];
			//cout << x_new << " == " << xc[variable] - yyc[equation]/A[equation][variable] << endl;
			if (Disjoint(long_x[variable], x_new)) {
				//stat->num_del_newt++;
				return -1;
			}
			if (in(x_new, long_x[variable])) {
				which[variable] = 0;
				long_x[variable] = x_new;
			}
			else long_x[variable] &= x_new;
			v[variable] = long_x[variable] - long_xc[variable];
			x[variable] = cxsc::interval(long_x[variable]);
		}
		/*else {
			//cout << "in\n";
			if (in (0.0, sum)) {singul = true; continue; }
			extend = xc[variable] - sum % A[equation][variable];
			vec = x[variable] & extend;
			//cout << "vec = " << vec << endl;
			if (vec[1] == EmptyIntval()) {
				//stat->num_del_newt++;
				return -1;
			}
			else if (vec[2] != EmptyIntval()) {
				x[variable] = vec[1];
				//modify_x (x, x_sel, var_num);
				x2 = x;
				x2[variable] = vec[2];
				//cout << "wynik 3\n";
				return 3;
			}
			else {
				x[variable] = vec[1];
				v[variable] = x[variable] - xc[variable];
			}
		}*/
	} while (++i <= rows_num);
	int s = 0;
	for (i = 1; i <= VecLen(which); ++i) s += which[i];
	if (p.num_fun + s <= VecLen(which)) return 1;
	return 0;
}


/*int use_Krawczyk (const problem_desc &p, ivector &x, imatrix A, ivector b, const intvector &var_num,
		  const intvector &eq_num, const rmatrix &Y, ivector &x2, intvector &which) {
	//cout << "use_Krawczyk\n";
	int rows_num = ColLen(A), cols_num = RowLen(A);
	rvector xc = (Inf(x) + Sup(x))/2;
	ivector v = x - xc;
	//ivector x_new = x + Y*(b + A*v);
	//ivector delta_x = Y*(b + A*v);
	//ivector delta_x = Y*b + (Y*A + Id(Y))*v;
	rmatrix Id_sel (rows_num, cols_num);
	Id_sel = 0.0;
	for (int i = 1; i <= rows_num; ++i) Id_sel[eq_num[i]][var_num[i]] = 1.0;
	ivector delta_x = (Id_sel - Y*A)*v - Y*b;
	//cout << "x = " << x << endl;
	//cout << "Id_sel = " << Id_sel << endl;
	//cout << "delta_x = " << delta_x << endl;
	//cout << "x_new = " << x_new << endl;
	//cout << "Y*(b + A*v) = " << Y*(b + A*v) << endl;
	which = 1;
	//cout << "which = " << which << endl;
	for (int i = 1; i <= rows_num; ++i) {
		//cout << "i = " << i << endl;
		int variable = var_num[i];
		int equation = eq_num[i];
		//cout << "var = " << variable << endl;
		//interval x_new = x[variable] + delta_x[equation];
		interval x_new = xc[variable] + delta_x[equation];
		//cout << "x_new = " << x_new << endl;
		if (Disjoint (x[variable], x_new)) return -1;
		else if (in(x_new, x[variable])) {
			which[variable] = 0;
			x[variable] = x_new;
		}
		else x[variable] &= x_new;
	}
	int s = 0;
	for (int i = 1; i <= VecLen(which); ++i) s += which[i];
	if (p.num_fun + s <= VecLen(which)) return 1;
	return 0;
}*/

