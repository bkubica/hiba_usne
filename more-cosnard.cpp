
#include "solver.hpp"
#include "problem_desc_impl.hpp"

#include <string>

#define N 20
#define M N

// N - variables, M - equations

inline cxsc::real t(const int k) {
	return k*1.0/(N + 1);
}

template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> eq (const adhc_vector<level, sparse_mode, n, T> &x, const int num) {
	adhc_ari<level, sparse_mode, n, T> result;
	/*if (num == 1) result = x[num] + 0.5*((1.0 - t(num))*t(1)*power(x[1] + cxsc::interval(t(1) + 1.0), 3) + t(num)*(1.0 - t(num))*t(2)*sqr(x[2] + cxsc::interval(t(2) + 1.0)));
	else result = x[num] + 0.5*((1.0 - t(num))*(t(1)*power(x[1] + cxsc::interval(t(1) + 1.0), 3) + t(2)*power(x[2] + cxsc::interval(t(2) + 1.0), 3)));
	return result;
	result = x[num];
	result += 0.5*cxsc::interval(1.0 - t(num))*t(1)*power(x[1] + cxsc::interval(t(1) + 1.0), 3);
	if (num == 1) result += 0.5*t(num)*sqr(x[2] + cxsc::interval(t(2) + 1.0));
	else result += 0.5*cxsc::interval(1.0 - t(num))*t(2)*power(x[2] + cxsc::interval(t(2) + 1.0), 3);
	return result;*/
	adhc_ari<level, sparse_mode, n, T> sum1, sum2;
	sum1 = T(0.0);
	sum2 = T(0.0);
	for (int j = 1; j <= num; ++j) sum1 += t(j)*power(x[j] + T(t(j) + 1.0), 3);
	//for (int j = 1; j <= num; ++j) sum1 += t(j)*sqr(x[j] + cxsc::interval(t(j) + 1.0))*(x[j] + cxsc::interval(t(j) + 1.0));
	for (int j = num + 1; j <= N; ++j) sum2 += T(1.0 - t(j))*sqr(x[j] + T(t(j) + 1.0));
	return x[num] + 0.5*(T(1.0 - t(num))*sum1 + t(num)*sum2);
	result += 0.5*(T(1.0 - t(num))*sum1 + t(num)*sum2);
	return result;
}

template<int level, SparsityLevel sparse_mode, int n, class T>
struct more_cosnard{
	adhc_ari<level, sparse_mode, n, T> operator() (const adhc_vector<level, sparse_mode, n, T> &x, const int i=0) const {
		return eq(x, i);
	};
};


//#define SPARSITY SparsityLevel::dense
//#define SPARSITY sparse
#define SPARSITY SparsityLevel::highly_sparse
//#define SPARSITY SparsityLevel::another_sparse


int main() {
#include "num_threads.hpp"
	opdotprec = 1;
	ivector x(N);
	x = interval (-4.0, 5.0);
	real eps = /*1e-2;// */1e-6;//1e-4;//1e-1;
	problem_desc_impl<SPARSITY, N, M, more_cosnard> p(x);
	solver s{num_threads};
	cout << "x0 = " << x << endl;
	create_func creat = Herbort_create_pair_list;
	//create_func creat = Goulard_create_pair_list;
	//create_func creat = GE_create_pair_list;
	//create_func creat = Goulard_GE_create_pair_list;
	s.branch_and_bound (p, eps, creat);
//#define SHOW
#if defined SHOW
	cout << "Possible: " << endl;
	for (auto &x : s.sol) {
		x.show();
	}
	cout << endl << "Guaranteed: " << endl;
	for (auto &x : s.guar_sol) {
		x.show();
	}
	std::cout << "========\n";
#endif
	s.show_results();
	s.stat_show();
	return 0;
}

