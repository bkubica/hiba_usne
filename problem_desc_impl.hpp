
#if !defined __PROBLEM_DESC_IMPL_HPP__
#define __PROBLEM_DESC_IMPL_HPP__

#include <ivector.hpp>
#include <l_ivector.hpp>
#include <l_imatrix.hpp>
#include <l_imath.hpp>
#include <xscclass.hpp>

#include <tbb/enumerable_thread_specific.h>
//#include <tbb/spin_mutex.h>

#include "problem_desc.hpp"
#include <adhc.hpp>

using namespace cxsc;
using namespace std;
using namespace adhc;


//thread_local bool tree_initialized = false;

template<int level, SparsityLevel sparse_mode, int num_vars, class T, class ... Args>
struct default_fun_type {
	default_fun_type(Args ... args_pack) {}

	adhc_ari<level, sparse_mode, num_vars, T> operator() (const adhc_vector<level, sparse_mode, num_vars, T> &x, int i) const {
		return adhc_ari<level, sparse_mode, num_vars, T>();
	}
};


//template <SparsityLevel sparse_mode, int num_vars, template<int lev, SparsityLevel sp> adhc_fun<lev, sp, num_vars>::adhc_fun_ptr>
template <SparsityLevel sparse_mode, int num_vars, int num_f, template<int, SparsityLevel, int, class, class ...> class func_type, int num_inequalities=0, template<int, SparsityLevel, int, class, class ...> class ineq_type=default_fun_type, class ... Args>
//template <SparsityLevel sparse_mode, int num_vars, template<int lev, SparsityLevel sp, int num> class adhc_ari<lev, sp, num> (*func_type)(const adhc_vector<lev, sp, num> &)>
struct problem_desc_impl : public problem_desc {
	// equations

	/*typename adhc_fun<0, sparse_mode, num_vars>::adhc_fun_ptr *f;
	typename adhc_fun<1, sparse_mode, num_vars>::adhc_fun_ptr *fg;
	typename adhc_fun<2, sparse_mode, num_vars>::adhc_fun_ptr *fgh;*/
	func_type<0, sparse_mode, num_vars, cxsc::interval, Args...> f;
	func_type<1, sparse_mode, num_vars, cxsc::interval, Args...> fg;
	func_type<2, sparse_mode, num_vars, cxsc::interval, Args...> fgh;

	func_type<-1, sparse_mode, num_vars, cxsc::interval, Args...> f_hc;

	func_type<0, sparse_mode, num_vars, cxsc::l_interval, Args...> f_high_prec;
	func_type<1, sparse_mode, num_vars, cxsc::l_interval, Args...> fg_high_prec;

	// inequalities

	ineq_type<0, sparse_mode, num_vars, cxsc::interval, Args...> ineq;
	ineq_type<1, sparse_mode, num_vars, cxsc::interval, Args...> ineq_g;
	ineq_type<2, sparse_mode, num_vars, cxsc::interval, Args...> ineq_gh;

	ineq_type<-1, sparse_mode, num_vars, cxsc::interval, Args...> ineq_hc;

	ineq_type<0, sparse_mode, num_vars, cxsc::l_interval, Args...> ineq_high_prec;
	ineq_type<1, sparse_mode, num_vars, cxsc::l_interval, Args...> ineq_g_high_prec;

    private:

	adhc_ari<-1, sparse_mode, num_vars> * (syntactic_tree[num_f + num_inequalities]);

	//tbb::spin_mutex tree_lock[num_f];

	//thread_local static adhc_ari<-1, sparse_mode, num_vars> * (syntactic_tree_tls[num_f]);
	mutable tbb::enumerable_thread_specific< adhc_ari<-1, sparse_mode, num_vars> * > syntactic_tree_tls[num_f + num_inequalities];
	mutable tbb::enumerable_thread_specific<bool> tree_initialized{false};

    public:

	problem_desc_impl (const ivector &x0_, Args ... args_pack) : problem_desc(x0_, num_f, num_inequalities),
			f(args_pack...), fg(args_pack...), fgh(args_pack...), f_hc(args_pack...),
			f_high_prec(args_pack...), fg_high_prec(args_pack...), 
			ineq(args_pack...), ineq_g(args_pack...), ineq_gh(args_pack...), ineq_hc(args_pack...),
			ineq_high_prec(args_pack...), ineq_g_high_prec(args_pack...) {
		//cout << "konstruktor impl\n";
		adhc_vector<-1, sparse_mode, num_vars> * x_hc_ptr = new adhc_vector<-1, sparse_mode, num_vars> (x0);
		//cout << "przed petla\n";
		int i = 1, i1 = 0;
		for (; i <= num_f; ++i, ++i1) {
			//cout << "tworzenie drzewa eq " << i << "\n";
			syntactic_tree[i1] = new adhc_ari<-1, sparse_mode, num_vars> (f_hc (*x_hc_ptr, i));
		}
		for (i = 1; i <= num_ineq; ++i, ++i1) {
			//cout << "tworzenie drzewa ineq " << i << "\n";
			syntactic_tree[i1] = new adhc_ari<-1, sparse_mode, num_vars> (ineq_hc (*x_hc_ptr, i));
		}
		for (int i1 = 0; i1 < num_f + num_ineq; ++i1) {
			std::cout << *syntactic_tree[i1] << "\n";
		}
		//hc_init_thread();
		/*{
			adhc_ari<-1, sparse_mode, num_vars> *some_tree = new adhc_ari<-1, sparse_mode, num_vars> (* syntactic_tree[0]);
			std::cout << some_tree << '\n';
			delete some_tree;
		}*/
		//cout << "koniec konstruktora impl\n";
	};

	problem_desc_impl (const problem_desc &) = delete;

	virtual ~problem_desc_impl () {
		//cout << "destruktor impl\n";
		for (int i1 = 0; i1 < num_f; ++i1) if (syntactic_tree[i1] != nullptr) delete syntactic_tree[i1];
	};

	void compute_f (const ivector &x, const int i, interval &y) const {
		const adhc_vector<0, sparse_mode, num_vars> x_{x};
		const adhc_ari<0, sparse_mode, num_vars> y_ = f(x_, i);
		y = std::get<0>(y_.deriv_value);
	}

	void compute_grad_f (const ivector &x, const int i, interval &y, ivector &g) const {
		//cout << "compute_grad_f, nr = " << i << endl;
		const adhc_vector<1, sparse_mode, num_vars> x_{x};
		const adhc_ari<1, sparse_mode, num_vars> y_ = fg(x_, i);
		y = std::get<0>(y_.deriv_value);
		g = std::get<1>(y_.deriv_value);
		//cout << "koniec compute_grad_f, nr = " << i << endl;
	}

	void compute_hess_f (const ivector &x, const int i, interval &y, ivector &g, imatrix &H) const {
		//cout << "compute_hess_f, nr = " << i << endl;
		const adhc_vector<2, sparse_mode, num_vars> x_{x};
		const adhc_ari<2, sparse_mode, num_vars> y_ = fgh(x_, i);
		y = std::get<0>(y_.deriv_value);
		g = std::get<1>(y_.deriv_value);
		H = std::get<2>(y_.deriv_value);
	}

	void compute_all_f (const ivector &x, ivector &y) const {
		//cout << "compute_all_f\n";
		y = ivector (num_fun);
		adhc_vector<0, sparse_mode, num_vars> x_{x};
		for (int i = 1; i <= num_fun; ++i) {
			adhc_ari<0, sparse_mode, num_vars> y_ = f(x_, i);
			y[i] = std::get<0>(y_.deriv_value);
		}
		//cout << "end compute_all_f\n";
	}

	void compute_all_grad_f (const ivector &x, ivector &y, imatrix &g) const {
		//cout << "old compute_all_grad_f\n";
		y = ivector (num_fun);
		g = imatrix (num_fun, VecLen(x));
		const adhc_vector<1, sparse_mode, num_vars> x_{x};
		for (int i = 1; i <= num_fun; ++i) {
			const adhc_ari<1, sparse_mode, num_vars> y_ = fg(x_, i);
			y[i] = std::get<0>(y_.deriv_value);
			g[i] = std::get<1>(y_.deriv_value);
		}
		//cout << "end compute_all_grad_f\n";
	}

	void compute_partial_derivative_f (const ivector &x, const int i, const int j, interval &y, interval &g) const {
		const adhc_vector<1, sparse_mode, num_vars> x_{x, j};
		const adhc_ari<1, sparse_mode, num_vars> y_ = fg(x_, i);
		y = std::get<0>(y_.deriv_value);
		g = std::get<1>(y_.deriv_value)[j];
	}

	void compute_f_high_prec (const l_ivector &x, const int i, l_interval &y) const {
		const adhc_vector<0, sparse_mode, num_vars, cxsc::l_interval> x_{x};
		const adhc_ari<0, sparse_mode, num_vars, cxsc::l_interval> y_ = f_high_prec(x_, i);
		y = std::get<0>(y_.deriv_value);
	}

	void compute_grad_f_high_prec (const l_ivector &x, const int i, l_interval &y, l_ivector &g) const {
		//cout << "compute_grad_f_high_prec, nr = " << i << endl;
		const adhc_vector<1, sparse_mode, num_vars, cxsc::l_interval> x_{x};
		const adhc_ari<1, sparse_mode, num_vars, cxsc::l_interval> y_ = fg_high_prec(x_, i);
		y = std::get<0>(y_.deriv_value);
		g = std::get<1>(y_.deriv_value);
		//cout << "end compute_grad_f_high_prec, nr = " << i << endl;
	}

	void compute_all_f_high_prec (const l_ivector &x, l_ivector &y) const {
		//cout << "compute_all_f_high_prec" << endl;
		y = l_ivector{num_fun};
		adhc_vector<0, sparse_mode, num_vars, cxsc::l_interval> x_{x};
		for (int i = 1; i <= num_fun; ++i) {
			adhc_ari<0, sparse_mode, num_vars, cxsc::l_interval> y_ = f_high_prec(x_, i);
			y[i] = std::get<0>(y_.deriv_value);
		}
		//cout << "end compute_all_f_high_prec" << endl;
	}

	void compute_all_grad_f_high_prec (const l_ivector &x, l_ivector &y, l_imatrix &g) const {
		//cout << "compute_all_grad_f_high_prec" << endl;
		y = l_ivector{num_fun};
		g = l_imatrix{num_fun, VecLen(x)};
		const adhc_vector<1, sparse_mode, num_vars, cxsc::l_interval> x_{x};
		for (int i = 1; i <= num_fun; ++i) {
			const adhc_ari<1, sparse_mode, num_vars, cxsc::l_interval> y_ = fg_high_prec(x_, i);
			y[i] = std::get<0>(y_.deriv_value);
			g[i] = std::get<1>(y_.deriv_value);
			//for (int j = 1; j <= RowLen(g); ++j) g[i, j] = std::get<1>(y_.deriv_value)[j];
		}
		//cout << "end compute_all_grad_f_high_prec" << endl;
	}


	void compute_ineq (const ivector &x, const int i, interval &y) const {
		const adhc_vector<0, sparse_mode, num_vars> x_{x};
		const adhc_ari<0, sparse_mode, num_vars> y_ = ineq(x_, i);
		y = std::get<0>(y_.deriv_value);
	}

	int compute_all_ineq (const ivector &x, const intvector &satisfied, ivector &y) const {
		//cout << "compute_all_f\n";
		y = ivector (num_ineq);
		adhc_vector<0, sparse_mode, num_vars> x_{x};
		int num_computed = 0;
		for (int i = 1; i <= num_ineq; ++i) {
			if (satisfied[i] == 0) {
				adhc_ari<0, sparse_mode, num_vars> y_ = ineq(x_, i);
				y[i] = std::get<0>(y_.deriv_value);
				++num_computed;
			}
			else {
				// y[i] should be a finite interval, even if Inf(rhs[i]) == -Infinity, or
				// Sup(rhs[i]) == +Infinity
				if (0.0 <= rhs[i]) y[i] = 0.0;
				else if (Inf(rhs[i]) > -cxsc::Infinity) y[i] = Inf(rhs[i]);
				else y[i] = Sup(rhs[i]);
			}
		}
		//cout << "end compute_all_f\n";
		return num_computed;
	}

	int compute_all_grad_ineq (const ivector &x, const intvector &satisfied, ivector &y, imatrix &g) const {
		//cout << "old compute_all_grad_f\n";
		y = ivector (num_ineq);
		g = imatrix (num_ineq, VecLen(x));
		const adhc_vector<1, sparse_mode, num_vars> x_{x};
		int num_computed = 0;
		for (int i = 1; i <= num_ineq; ++i) {
			if (satisfied[i] == 0) {
				const adhc_ari<1, sparse_mode, num_vars> y_ = ineq_g(x_, i);
				y[i] = std::get<0>(y_.deriv_value);
				g[i] = std::get<1>(y_.deriv_value);
				++num_computed;
			}
			else {
				g[i] = 0.0;
				// y[i] should be a finite interval, even if Inf(rhs[i]) == -Infinity, or
				// Sup(rhs[i]) == +Infinity
				if (0.0 <= rhs[i]) y[i] = 0.0;
				else if (Inf(rhs[i]) > -cxsc::Infinity) y[i] = Inf(rhs[i]);
				else y[i] = Sup(rhs[i]);
			}
		}
		//cout << "end compute_all_grad_f\n";
		return num_computed;
	}

	int compute_all_ineq_high_prec (const l_ivector &x, const intvector &satisfied, l_ivector &y) const {
		//cout << "compute_all_f_high_prec" << endl;
		y = l_ivector{num_ineq};
		adhc_vector<0, sparse_mode, num_vars, cxsc::l_interval> x_{x};
		int num_computed = 0;
		for (int i = 1; i <= num_ineq; ++i) {
			if (satisfied[i] == 0) {
				adhc_ari<0, sparse_mode, num_vars, cxsc::l_interval> y_ = ineq_high_prec(x_, i);
				y[i] = std::get<0>(y_.deriv_value);
				++num_computed;
			}
			else {
				// y[i] should be a finite interval, even if Inf(rhs[i]) == -Infinity, or
				// Sup(rhs[i]) == +Infinity
				if (0.0 <= rhs[i]) y[i] = 0.0;
				else if (Inf(rhs[i]) > -cxsc::Infinity) y[i] = Inf(rhs[i]);
				else y[i] = Sup(rhs[i]);
			}
		}
		//cout << "end compute_all_f_high_prec" << endl;
		return num_computed;
	}

	int compute_all_grad_ineq_high_prec (const l_ivector &x, const intvector &satisfied, l_ivector &y, l_imatrix &g) const {
		//cout << "compute_all_grad_f_high_prec" << endl;
		y = l_ivector{num_ineq};
		g = l_imatrix{num_ineq, VecLen(x)};
		const adhc_vector<1, sparse_mode, num_vars, cxsc::l_interval> x_{x};
		int num_computed = 0;
		for (int i = 1; i <= num_ineq; ++i) {
			if (satisfied[i] == 0) {
				const adhc_ari<1, sparse_mode, num_vars, cxsc::l_interval> y_ = ineq_g_high_prec(x_, i);
				y[i] = std::get<0>(y_.deriv_value);
				g[i] = std::get<1>(y_.deriv_value);
				++num_computed;
			}
			else {
				g[i] = 0.0;
				// y[i] should be a finite interval, even if Inf(rhs[i]) == -Infinity, or
				// Sup(rhs[i]) == +Infinity
				if (0.0 <= rhs[i]) y[i] = 0.0;
				else if (Inf(rhs[i]) > -cxsc::Infinity) y[i] = Inf(rhs[i]);
				else y[i] = Sup(rhs[i]);
			}
		}
		//cout << "end compute_all_grad_f_high_prec" << endl;
		return num_computed;
	}


	int hc_initial_evaluate (const ivector &x, const int i) const {
		//std::cout << syntactic_tree[i] << '\n';
		const int i1 = i - 1;
		//tbb::spin_mutex::scoped_lock lock (tree_lock[i1]);
		//--return evaluate_nodes (syntactic_tree[i1], x);
		adhc_ari<-1, sparse_mode, num_vars> *&tree = syntactic_tree_tls[i1].local();
		evaluate_nodes (tree, x);
		return 0;
	}


	void hc_init_thread() const {
		if (tree_initialized.local()) {
			//std::cout << "tree already initialized in this thread\n";
			return;
		}
		std::cout << "initializing tree in a thread\n";
		for (int i1 = 0; i1 < num_f + num_ineq; ++i1) {
			//std::cout << syntactic_tree[i1] << '\n';
			adhc_ari<-1, sparse_mode, num_vars> **ptr = &(syntactic_tree_tls[i1].local());
			*ptr = new adhc_ari<-1, sparse_mode, num_vars> (*syntactic_tree[i1]);
			//--**ptr = *syntactic_tree[i1];
			///*syntactic_tree_tls[i1]*/(*ptr)->show_MTsafe();
			//std::cout << '\n';
			//adhc_ari<-1, sparse_mode, num_vars> *tree = syntactic_tree_tls[i1].local();
			//*tree = *syntactic_tree[i1];
		}
		tree_initialized.local() = true;
		/*for (int i1 = 0; i1 < num_f; ++i1) {
			adhc_ari<-1, sparse_mode, num_vars> *tree = syntactic_tree_tls[i1].local();
			std::cout << tree << '\n';
		}*/
	}


	//int hc_narrow(ivector &x, const int i) const {
	//int hc_narrow(ivector &x, const int i, intvector &which) const {
	int hc_narrow(ivector &x, const int i, intvector &which, const interval &rhs_i) const {
		//return -2;
		//std::cout << "hc_narrow \n";// << x << ", " << i << ")\n";
		//evaluate_nodes (syntactic_tree[i], x);
		const int i1 = i - 1;
		adhc_ari<-1, sparse_mode, num_vars> *&tree = syntactic_tree_tls[i1].local();
		hc_forward_evaluate (tree, x);
		const int result = hc_enforce (tree, x, which, rhs_i);
		/*tbb::spin_mutex::scoped_lock lock;
		if (!lock.try_acquire(tree_lock[i1])) return -2;*/
		//--hc_forward_evaluate (syntactic_tree[i1], x);
		//--int result = hc_enforce (syntactic_tree[i1], x, which, cxsc::interval(0.0));
		//std::cout << "narrowing finished, x = " << x << '\n';
		return result;
	}


	cxsc::intmatrix count_variable_occurrences_in_equations() const {
		cxsc::intmatrix result(num_fun, num_vars);
		cxsc::intvector counts(num_vars);
		for (int i = 1, i1 = 0; i <= num_fun; ++i, ++i1) {
			counts = 0;
			count_variable_occurrences (syntactic_tree[i1], counts);
			result[i] = counts;
		}
		return result;
	}
};

#endif

