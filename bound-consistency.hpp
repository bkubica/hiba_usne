
#include "threaded_aux.hpp"

#if defined DISTR_MUTEX
int update_ub (const problem_desc &p, const real eps, elem &e, const std::vector< std::pair<int, int> > &pairs_for_Ncmp,
		intvector &which, distr_mutex *mutex_bound_cons_tab, const int i, const real lambda) {
#else
int update_ub (const problem_desc &p, const real eps, elem &e, const std::vector< std::pair<int, int> > &pairs_for_Ncmp,
		intvector &which, mutex_bound_cons_type &mutex_bound_cons, const int i, const real lambda) {
#endif
	mutex_bound_cons_type::scoped_lock lock;
	//cout << "przed\n";
#if defined DISTR_MUTEX
	for (int ii = 1; ii <= VecLen(e.x); ++ii) mutex_bound_cons_tab[ii].mut.lock_shared();
#elif defined NON_RW_MUTEX
	lock.acquire(mutex_bound_cons);
#else
	//mutex_bound_cons_tab[i].lock_shared();
	lock.acquire(mutex_bound_cons, false);
#endif
	elem e_cur = e;
	//e.cur.show();
#if defined DISTR_MUTEX
	for (int ii = VecLen(e.x); ii >= 1; --ii) mutex_bound_cons_tab[ii].mut.unlock_shared();
#else
	//mutex_bound_cons_tab[i].unlock();
	lock.release();
#endif
	//cout << "po\n";
	real c2 = lambda*Inf(e_cur.x[i]) + (1.0 - lambda)*Sup(e_cur.x[i]);
	SetInf(e_cur.x[i], c2);
	//int result = bc_cons_serial(p, eps, e_cur, pairs_for_Ncmp, which);
	int result = hc_bc_cons_serial(p, eps, e_cur, which);
	//int result = hc_cons_serial(p, eps, e_cur, which);
	//mutex_bound_cons_type::scoped_lock lock(mutex_bound_cons);
#if defined DISTR_MUTEX
	lock.acquire(mutex_bound_cons_tab[i].mut);//, true);
#else
	lock.acquire(mutex_bound_cons);//, true);
#endif
	if (result == -1) {
		SetSup(e.x[i], c2);
	}
	else if (Sup(e_cur.x[i]) < Sup(e.x[i])) {
		SetSup(e.x[i], Sup(e_cur.x[i]));
	}
	return 0;
}


#if defined DISTR_MUTEX
int update_lb (const problem_desc &p, const real eps, elem &e, const std::vector< std::pair<int, int> > &pairs_for_Ncmp,
		intvector &which, distr_mutex *mutex_bound_cons_tab, const int i, const real lambda) {
#else
int update_lb (const problem_desc &p, const real eps, elem &e, const std::vector< std::pair<int, int> > &pairs_for_Ncmp,
		intvector &which, mutex_bound_cons_type &mutex_bound_cons, const int i, const real lambda) {
#endif
	mutex_bound_cons_type::scoped_lock lock;
#if defined DISTR_MUTEX
	for (int ii = 1; ii <= VecLen(e.x); ++ii) mutex_bound_cons_tab[ii].mut.lock_shared();
#elif defined  NON_RW_MUTEX
	lock.acquire(mutex_bound_cons);
#else
	lock.acquire(mutex_bound_cons, false);
#endif
	elem e_cur = e;
#if defined DISTR_MUTEX
	for (int ii = VecLen(e.x); ii >= 1; --ii) mutex_bound_cons_tab[ii].mut.unlock_shared();
#else
	lock.release();
#endif
	real c1 = (1.0 - lambda)*Inf(e_cur.x[i]) + lambda*Sup(e_cur.x[i]);
	SetSup(e_cur.x[i], c1);
	//int result = bc_cons_serial(p, eps, e_cur, pairs_for_Ncmp, which);
	int result = hc_bc_cons_serial(p, eps, e_cur, which);
	//int result = hc_cons_serial(p, eps, e_cur, which);
#if defined DISTR_MUTEX
	lock.acquire(mutex_bound_cons_tab[i].mut);//, true);
#else
	lock.acquire(mutex_bound_cons);//, true);
#endif
	if (result == -1) {
		SetInf(e.x[i], c1);
	}
	else if (Inf(e_cur.x[i]) > Inf(e.x[i])) {
		SetInf(e.x[i], Inf(e_cur.x[i]));
	}
	return 0;
}


int bound_cons(const problem_desc &p, const real eps, elem &e, std::vector< std::pair<int, int> > &pairs_for_Ncmp,
		intvector &which, const real lambda_min, const real eps_equal_boundcons) {
	which = 1;
	//typename possibly_atomic<bool, thread_private>::value_type modified;
	possibly_atomic<bool, thread_shared> modified;
	real lambda = 0.25;
#if defined DISTR_MUTEX
	distr_mutex mutex_bound_cons_tab[VecLen(e.x) + 1];
	//cout << "size = " << sizeof(distr_mutex) << '\n';
#else
	mutex_bound_cons_type mutex_bound_cons;
#endif
	do {
		modified = false;
		parallel_for(1, VecLen(e.x) + 1, 1, [&](int i) {
		//for (int i = 1; i <= VecLen(x); ++i) {
			//cout << "i = " << i << "\n";
			mutex_bound_cons_type::scoped_lock lock;
#if defined DISTR_MUTEX
			lock.acquire(mutex_bound_cons_tab[i].mut, false);
#elif defined NON_RW_MUTEX
			lock.acquire(mutex_bound_cons);
#else
			lock.acquire(mutex_bound_cons, false);
#endif
			interval old_x = e.x[i];
			lock.release();
			tbb::parallel_invoke(
#if defined DISTR_MUTEX
				[&] {update_ub(p, eps, e, pairs_for_Ncmp, which, mutex_bound_cons_tab, i, lambda);},
				[&] {update_lb(p, eps, e, pairs_for_Ncmp, which, mutex_bound_cons_tab, i, lambda);}
#else
				[&] {update_ub(p, eps, e, pairs_for_Ncmp, which, mutex_bound_cons, i, lambda);},
				[&] {update_lb(p, eps, e, pairs_for_Ncmp, which, mutex_bound_cons, i, lambda);}
#endif
			);
#if defined DISTR_MUTEX
			lock.acquire(mutex_bound_cons_tab[i].mut, false);
#elif defined NON_RW_MUTEX
			lock.acquire(mutex_bound_cons);
#else
			lock.acquire(mutex_bound_cons, false);
#endif
			if (diam(e.x[i]) + /*EPS_EQUAL_BOUNDCONS*/eps_equal_boundcons < diam(old_x)) modified = true;
			lock.release();
		});
		if (!modified) lambda /= 2;
		++stat->num_bound_cons_recur;
	} while (lambda > lambda_min);
	//} while (lambda > 0.125);
	//--} while (lambda > 0.015625); // 1/64
	//} while (lambda > 0.0078125); // 1/128
	return 0;
}

