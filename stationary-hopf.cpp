
#include "solver.hpp"
#include "problem_desc_impl.hpp"

#include <string>


// N - variables, M - equations


constexpr int num_vectors_to_remember = 1;
constexpr int num_neurons = 8;

constexpr int N = num_neurons;
constexpr int M = N;

static cxsc::rmatrix vectors_to_remember(num_vectors_to_remember, num_neurons);

void create_vectors_to_remember() {
	for (int i = 1; i <= num_neurons; ++i) vectors_to_remember[1][i] = 1.0;
	//vectors_to_remember[1][1]=1.0; vectors_to_remember[1][2]=1.0; vectors_to_remember[1][3]= 1.0; vectors_to_remember[1][4]= 1.0;
	if (num_vectors_to_remember >= 2) {
		cout << "aaa\n";
		for (int i = 1; i <= num_neurons/2; ++i) vectors_to_remember[2][i] = 1.0;
		for (int i = 1 + num_neurons/2; i <= num_neurons; ++i) vectors_to_remember[2][i] = -1.0;
		//vectors_to_remember[2][1]=1.0; vectors_to_remember[2][2]=1.0; vectors_to_remember[2][3]=-1.0; vectors_to_remember[2][4]=-1.0;
	}
	if (num_vectors_to_remember >= 3) {
		for (int i = 1; i <= num_neurons - 2; ++i) vectors_to_remember[3][i] = 1.0;
		vectors_to_remember[3][num_neurons - 1] = vectors_to_remember[3][num_neurons] = -1.0;
		//vectors_to_remember[3][1]=1.0; vectors_to_remember[3][2]=1.0; vectors_to_remember[3][3]= 1.0; vectors_to_remember[3][4]=-1.0;
	}
	//cout << "vectors: " << vectors_to_remember << "\n";
}

static cxsc::rmatrix weights(num_neurons, num_neurons);

void create_weights_matrix() {
	weights = 0.0;
	for (int i = 1; i <= num_vectors_to_remember; ++i) {
		const rvector &vec = vectors_to_remember[i];
		//cout << "vec = " << vec << "\n";
		for (int j = 1; j <= N; ++j)
			for(int k = 1; k <= N; ++k)
				if (k != j) weights[j][k] += vec[j]*vec[k];
	}
	for (int i = 1; i <= N; ++i) weights[i][i] = 0.0;
	//cout << "weights: " << weights << "\n";
}


#define weight(i, j) weights[1 + i][1 + j]
#define state(num_vector, i) x[1 + i]


template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> sigma (const adhc_ari<level, sparse_mode, n, T> &x) {
	adhc_ari<level, sparse_mode, n, T> one;
	one = cxsc::real(1.0);
	//return 2.0*one/(one + exp(-20.0*x)) - one;
	return 2.0*one/(one + exp(-x)) - one;
}


template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> single_neuron_activation (const adhc_vector<level, sparse_mode, n, T> &x, const int num_vector, const int index) {
	int i = (index > 0)?0:1;
	adhc_ari<level, sparse_mode, n, T> sum = adhc_ari<level, sparse_mode, n, T>::get_instance();
	sum = weight(i, index)*state(num_vector, i);
	++i;
	for (; i < num_neurons; ++i) {
		//cout << "weight = x[" << (1 + i + index*(num_neurons - 1) + ((i>=index)?-1:0)) << "]\n";
		if (i != index && weight(i, index) != 0.0) sum += weight(i, index)*state(num_vector, i);
	}
	//return sum;
	return max(sum, 0.0);
	//return max(sum, sum*0.01);
	return sigma(sum);
}


template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> eq_i (const adhc_vector<level, sparse_mode, n, T> &x, int i) {
	adhc_ari<level, sparse_mode, n, T> result = adhc_ari<level, sparse_mode, n, T>::get_instance();
	//std::cout << "\neq i = " << i << "\t";
	const int num_vector = (i - 1) / num_neurons;
	int index = (i - 1) % num_neurons;
	//std::cout << "num_vector = " << num_vector << "\t";
	//std::cout << "index = " << index << "\n";
	return state(num_vector, index) - single_neuron_activation(x, num_vector, index);
}


template<int level, SparsityLevel sparse_mode, int n, class T>
struct hopfield_stationary_points {
	hopfield_stationary_points() {
		create_vectors_to_remember();
		create_weights_matrix();
	}
	adhc_ari<level, sparse_mode, n, T> operator() (const adhc_vector<level, sparse_mode, n, T> &x, const int i) const {
		return eq_i(x, i);
	};
};

#undef state
#undef weight

#define SPARSITY SparsityLevel::highly_sparse


int main() {
#include "num_threads.hpp"
	opdotprec = 1;
	create_vectors_to_remember();
	create_weights_matrix();
	ivector x(N);
	x = cxsc::interval (-2.0, 2.0);
	//x = cxsc::interval (-10.0, 10.0);
	cout << "vectors: " << vectors_to_remember << "\n";
	cout << "weights: " << weights << "\n";
	cout << "x = " << x << "\n";
	cout << "N = " << N << "\n";
	cout << "M = " << M << "\n";
	//for (int i = 0; i < num_layers; ++i) cout << "neurons: " << num_neurons_in_layers_up_to(i) << "\n";
	//for (int i = 0; i < num_layers; ++i) cout << "connections: " << num_connections_in_layers_up_to(i) << "\n";
	real eps = 1e-6; // N = 3
	problem_desc_impl<SPARSITY, N, M, hopfield_stationary_points> p{x};
	solver s (num_threads);
	cout << "x0 = " << x << endl;
	//std::terminate();
	create_func creat = Herbort_create_pair_list;
	//create_func creat = Goulard_create_pair_list;
	//create_func creat = GE_create_pair_list;
	//create_func creat = Goulard_GE_create_pair_list;
	s.branch_and_bound (p, eps, creat);
//#define SHOW
#if defined SHOW
	cout << "Possible: " << endl;
	for (auto &x : s.sol) {
		x.show();
	}
	cout << endl << "Guaranteed: " << endl;
	for (auto &x : s.guar_sol) {
		x.show();
	}
	std::cout << "========\n";
#endif
	s.show_results();
	s.stat_show();
	return 0;
}

