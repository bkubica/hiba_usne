
#include "solver.hpp"
#include "problem_desc_impl.hpp"

#include <string>

#define N 5
#define M N

// N - variables, M - equations

template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> eq_i (const adhc_vector<level, sparse_mode, n, T> &x, const int num) {
	adhc_ari<level, sparse_mode, n, T> result;
	result = x[num] - (1.0 - n);
	int i = 1;
	do {
		result += x[i];
	} while (++i <= n);
	return result;
}


template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> eq_last (const adhc_vector<level, sparse_mode, n, T> &x) {
	adhc_ari<level, sparse_mode, n, T> result;
	result = x[1];//cxsc::interval(1.0);
	int i = 2;
	do {
		result *= x[i];
	} while (++i <= n);
	//result = x[1]*x[2]*x[3]*x[4]*x[5];//????????????
	return result - 1.0;
}


template<int level, SparsityLevel sparse_mode, int n, class T>
struct brown {
	adhc_ari<level, sparse_mode, n, T> operator() (const adhc_vector<level, sparse_mode, n, T> &x, const int i) const {
		if (i < n) return eq_i(x, i);
		else return eq_last(x);
	};
};


#define SPARSITY SparsityLevel::highly_sparse


int main() {
#include "num_threads.hpp"
	opdotprec = 1;
	ivector x(N);
	x = interval (-100.0, 100.0);
	real eps = 1e-8;
	problem_desc_impl<SPARSITY, N, M, brown> p(x);
	solver s (num_threads);
	cout << "x0 = " << x << endl;
	create_func creat = Herbort_create_pair_list;
	//create_func creat = Goulard_create_pair_list;
	//create_func creat = GE_create_pair_list;
	//create_func creat = Goulard_GE_create_pair_list;
	s.branch_and_bound (p, eps, creat);
#define SHOW
#if defined SHOW
	cout << "Possible: " << endl;
	for (auto &x : s.sol) {
		x.show();
	}
	cout << endl << "Guaranteed: " << endl;
	for (auto &x : s.guar_sol) {
		x.show();
	}
	std::cout << "========\n";
#endif
	s.show_results();
	s.stat_show();
	return 0;
}

