
#include "threaded_aux.hpp"

#define USE_EPS
#define BREAK_ON_SMALL_PROGRESS

#define EPS_EQUAL 1e-2

int left_narrow (const problem_desc &p, ivector &x, const interval &rhs_nf, const int num_fun, const int num_var, const real &eps, real & lb) {
	//cout << "left_narrow, x = " << x << ", num_var = " << num_var << endl;
	//cout << "left: eq = " << num_fun << ", var = " << num_var << endl;
	//cout << "x = " << x << endl;
	ivector x_left = x;
	interval y_left;
	x_left[num_var] = Inf(x[num_var]);
	SetSup (x_left[num_var], succ(Inf(x[num_var])));
	p.compute_f (x_left, num_fun, y_left); ++stat->num_f;
	//cout << "y_left = " << y_left << endl;
	//if (in (0, y_left)) {
	if (!Disjoint(y_left, rhs_nf)) {
		lb = Inf(x[num_var]);
		return 0;
	}
	interval y;
	ivector g;
	g = ivector (x);
	p.compute_grad_f (x, num_fun, y, g); ++stat->num_grad_f;
	//p.compute_f (x, num_fun, y); ++stat->num_grad_f;
	//cout << "y = " << y << endl;
	//cout << "g = " << g << endl;
	//if (!in (0, y)) return -1;
	if (Disjoint(y, rhs_nf)) return -1;
	SetInf(x[num_var], succ(Inf(x[num_var])));
	if (in (0, g[num_var])) {
		//cout << "extended\n";
		//lb = Inf(x[num_var]);
		//return 0;
		//cout << "extend\n";
		/*interval yc;
		ivector xc = x;
		xc[num_var] = (Inf(x[num_var]) + Sup(x[num_var]))/2;
		p.compute_f (xc, num_fun, yc); ++stat->num_f;
		xinterval extend = mid(xc[num_var]) - yc % g[num_var];*/
		xinterval extend = Inf(x[num_var]) - (y_left - rhs_nf) % g[num_var];
		ivector vec = x[num_var] & extend;
		//if (vec[1] == EmptyIntval()) cout << "nothing\n";
		if (vec[1] == EmptyIntval()) return -1;
		if (Inf(vec[1]) - Inf(x[num_var]) < EPS_EQUAL && Sup(x[num_var]) - Sup(vec[1]) < EPS_EQUAL) {
#if !defined BREAK_ON_SMALL_PROGRESS
			lb = max(Inf(x[num_var]), Inf(vec[1]));
			goto bisec_left_bc;
#else
			lb = Inf(x[num_var]);
			return 0;
#endif
		}
		if (vec[2] != EmptyIntval()) {
			/*cout << "vec[2]\n";
			cout << "vec = " << vec << endl;
			if (vec[1] == vec[2]) cout << "vec[1] == vec[2]\n";
			if (vec[1] == x[num_var]) cout << "vec[1] equal\n";
			if (vec[2] == x[num_var]) cout << "vec[2] equal\n";*/
			ivector x1 = x;
			x1[num_var] = vec[1];
			if (left_narrow(p, x1, rhs_nf, num_fun, num_var, eps, lb) == -1) {
				x1[num_var] = vec[2];
				int result = left_narrow (p, x1, rhs_nf, num_fun, num_var, eps, lb);
				return result;
			}
			return 0;
		}
		else {
			//cout << "vec[1]\n";
			x[num_var] = vec[1];
			//return left_narrow (p, x, num_fun, num_var, eps, lb);
		}
	}
	else {
		//cout << "normal\n";
		/*interval yc;
		ivector xc = x;
		xc[num_var] = (Inf(x[num_var]) + Sup(x[num_var]))/2;
		p.compute_f (xc, num_fun, yc); ++stat->num_f;
		interval x_new = xc[num_var] - yc/g[num_var];*/
		interval x_new = x_left[num_var] - (y_left - rhs_nf)/g[num_var];
		if (Disjoint (x_new, x[num_var])) return -1;
		if (Inf(x_new) - Inf(x[num_var]) < EPS_EQUAL && Sup(x[num_var]) - Sup(x_new) < EPS_EQUAL) {
			x[num_var] &= x_new;
#if !defined BREAK_ON_SMALL_PROGRESS
			//goto bisec_left_bc;
#else
			lb = Inf(x[num_var]);
			return 0;
#endif
		}
		x[num_var] &= x_new;
	}
	//cout << "split\n";
bisec_left_bc:
#if defined USE_EPS
	if (diam(x[num_var]) < eps) {
#else
	if (Sup(x[num_var]) <= succ(Inf(x[num_var]))) {
#endif
		lb = Inf(x[num_var]);
		return 0;
	}
	ivector x1 = x;
	real c = (Inf(x[num_var]) + Sup(x[num_var]))/2;
	SetSup (x1[num_var], c);
	if (left_narrow(p, x1, rhs_nf, num_fun, num_var, eps, lb) == -1) {
		x1[num_var] = interval (c, Sup(x[num_var]));
		int result = left_narrow (p, x1, rhs_nf, num_fun, num_var, eps, lb);
		return result;
	}
	return 0;
}


int right_narrow (const problem_desc &p, ivector &x, const interval &rhs_nf, const int num_fun, const int num_var, const real &eps, real & ub) {
	//cout << "right: eq = " << num_fun << ", var = " << num_var << endl;
	ivector x_right = x;
	interval y_right;
	x_right[num_var] = Sup(x[num_var]);
	SetInf (x_right[num_var], pred(Sup(x[num_var])));
	//cout << "x_right = " << x_right << endl;
	p.compute_f (x_right, num_fun, y_right); ++stat->num_f;
	//cout << "y_right = " << y_right << endl;
	//cout << "g = " << g[num_var] << endl;
	//if (in (0, y_right)) {
	if (!Disjoint(y_right, rhs_nf)) {
		ub = Sup(x[num_var]);
		return 0;
	}
	interval y;
	ivector g;
	p.compute_grad_f (x, num_fun, y, g); ++stat->num_grad_f;
	//cout << "y = " << y << endl;
	//cout << "g = " << g << endl;
	//if (!in (0, y)) return -1;
	if (Disjoint(y, rhs_nf)) return -1;
	SetSup(x[num_var], pred(Sup(x[num_var])));
	if (in (0, g[num_var])) {
		//ub = Sup(x[num_var]);
		//return 0;
		xinterval extend = Sup(x[num_var]) - (y_right - rhs_nf) % g[num_var];
		ivector vec = x[num_var] & extend;
		if (vec[1] == EmptyIntval()) return -1;
		if (Inf(vec[1]) - Inf(x[num_var]) < EPS_EQUAL && Sup(x[num_var]) - Sup(vec[1]) < EPS_EQUAL) {
#if !defined BREAK_ON_SMALL_PROGRESS
			ub = Sup(x[num_var]);
			goto bisec_right_bc;
#else
			ub = Sup(x[num_var]);
			return 0;
#endif
		}
		if (vec[2] != EmptyIntval()) {
			ivector x1 = x;
			x1[num_var] = vec[2];
			if (right_narrow(p, x1, rhs_nf, num_fun, num_var, eps, ub) == -1) {
				x1[num_var] = vec[1];
				int result = right_narrow (p, x1, rhs_nf, num_fun, num_var, eps, ub);
				return result;
			}
			return 0;
		}
		else {
			//cout << "vec[1]\n";
			x[num_var] = vec[1];
			//return left_narrow (p, x, num_fun, num_var, eps, lb);
		}
	}
	else {
		/*cout << "xr = " << x_right[num_var] << endl;
		cout << "yr = " << y_right << endl;
		cout << "gn = " << g[num_var] << endl;
		cout << "pom = " << /*x_right[num_var]*/// - y_right/g[num_var] << endl;
		/*interval yc;
		ivector xc = x;
		xc[num_var] = (Inf(x[num_var]) + Sup(x[num_var]))/2;
		p.compute_f (xc, num_fun, yc); ++stat->num_f;
		interval x_new = xc[num_var] - yc/g[num_var];*/
		interval x_new = x_right[num_var] - (y_right - rhs_nf)/g[num_var];
		//cout << "x_new = " << x_new << endl;
		if (Disjoint (x_new, x[num_var])) return -1;
		if (Inf(x_new) - Inf(x[num_var]) < EPS_EQUAL && Sup(x[num_var]) - Sup(x_new) < EPS_EQUAL) {
			x[num_var] &= x_new;
#if !defined BREAK_ON_SMALL_PROGRESS
			//goto bisec_right_bc;
#else
			ub = Sup(x[num_var]);
			return 0;
#endif
		}
		x[num_var] &= x_new;
	}
bisec_right_bc:
#if defined USE_EPS
	if (diam(x[num_var]) < eps) {
#else
	if (Sup(x[num_var]) <= succ(Inf(x[num_var]))) {
#endif
		ub = Sup(x[num_var]);
		return 0;
	}
	ivector x1 = x;
	real c = (Inf(x[num_var]) + Sup(x[num_var]))/2;
	SetInf (x1[num_var], c);
	if (right_narrow(p, x1, rhs_nf, num_fun, num_var, eps, ub) == -1) {
		x1[num_var] = interval (Inf(x[num_var]), c);
		int result = right_narrow (p, x1, rhs_nf, num_fun, num_var, eps, ub);
		return result;
	}
	return 0;
}


//**************************************************************************


template <thread_privacy_t priv>
int bc3revise (const problem_desc &p, ivector &x, const interval &rhs_nf, const int num_fun, const int num_var, const real &eps, intvector &which, possibly_atomic<bool, priv> &modified) {
//int bc3revise (const problem_desc &p, ivector &x, const int num_fun, const int num_var, const real eps, intvector &which, tbb::atomic<bool> &modified) {
	//cout << "bc3revise, num_fun = " << num_fun << ", num_var = " << num_var << endl;
	real lb, ub;
	//modified = false;
	ivector old_x = x;
	if (left_narrow (p, x, rhs_nf, num_fun, num_var, eps, lb) == -1) return -1;
	if (lb > Inf(x[num_var]))SetInf(x[num_var], lb);
	//else cout << "lb too low!\n";
	if (right_narrow (p, x, rhs_nf, num_fun, num_var, eps, ub) == -1) return -1;
	if (lb > ub) {
		cout << "x = " << x << endl;
		cout << "old_x = " << old_x << endl;
		cout << "num_fun = " << num_fun << endl;
		cout << "num_var = " << num_var << endl;
		cout << "lb = " << lb << endl;
		cout << "ub = " << ub << endl;
		if ((lb - EPS_EQUAL > Inf(old_x[num_var]))||(ub + EPS_EQUAL < Sup(old_x[num_var]))) modified = true;
		if ((lb > Inf(old_x[num_var]))&&(ub < Sup(old_x[num_var]))) which[num_var] = 0;
		return 0;
		//exit(0);
	}
	//lb = Inf (x[num_var]);
	//ub = Sup (x[num_var]);
	//cout << "lb = " << lb << endl;
	//cout << "ub = " << ub << endl;
	//cout << "old: " << x[num_var] << ", new: " << interval (lb, ub) << endl;
	//if (diam(x[num_var])/(ub - lb) >= 1.015625) modified = true;
	if ((lb - EPS_EQUAL > Inf(old_x[num_var]))||(ub + EPS_EQUAL < Sup(old_x[num_var]))) modified = true;
	//if ((lb - 1e-1 > Inf(x[num_var]))||(ub + 1e-1 < Sup(x[num_var]))) modified = true;
	if ((lb > Inf(old_x[num_var]))&&(ub < Sup(old_x[num_var]))) which[num_var] = 0;
	x[num_var] &= interval (lb, ub);
	return 0;
}


int bc_cons_serial(const problem_desc &p, const real &eps, ivector &x, const std::vector< std::pair<int, int> > &pairs_for_Ncmp,
		   intvector &which) {
	which = 1;
	possibly_atomic<bool, thread_private> modified;
	do {
		modified = false;
		//for (int i = 0; i < pairs_for_Ncmp.size(); ++i) {
		//for (std::pair<int, int> eq_var : pairs_for_Ncmp) {
		for (auto eq_var : pairs_for_Ncmp) {
			int result = bc3revise<thread_private>(p, x, interval(0.0), eq_var.first, eq_var.second, eps, which, modified);
			++stat->num_bc3rev;
			//cout << "result = " << result << endl;
			if (result == -1) {
				++stat->num_del_bc;
				return -1;
			}
		}
	} while (modified);
	return 0;
}


int bc_cons_parallel(const problem_desc &p, const real &eps, ivector &x, const std::vector< std::pair<int, int> > &pairs_for_Ncmp,
		     intvector &which) {
	//cout << "bc_cons_parallel" << endl;
	possibly_atomic<bool, thread_shared> modified;
	modified = false;
	possibly_atomic<bool, thread_shared> koniec;
	koniec = false;
// these macros are dfined in threaded_aux.hpp
#if !defined NON_RW_MUTEX
	using mutex_bc_cons_type = tbb::spin_rw_mutex;
#elif !defined DISTR_MUTEX
	using mutex_bc_cons_type = tbb::spin_mutex;
#endif

#if defined DISTR_MUTEX
	//cout << "len = " << VecLen(x) + 1 << endl;
	distr_mutex mutex_bc_cons_tab[VecLen(x) + 1];
#else
	mutex_bc_cons_type mutex_bc_cons;
#endif
	do {
		//cout << "do\n";
		modified = false;
		tbb::parallel_for(0, (int)pairs_for_Ncmp.size(), 1, [&](int i) {
			//cout << "i = " << i << endl;
			if (koniec) return; // return from lambda, not from bc_cons_parallel()
			const int eq_num = pairs_for_Ncmp[i].first;
			const int var_num = pairs_for_Ncmp[i].second;
#if defined DISTR_MUTEX
			//mutex_bc_cons_type::scoped_lock lock;
			scoped_lock_of<mutex_bc_cons_type> lock;
			for (int ii = 1; ii <= VecLen(p.x0); ++ii) mutex_bc_cons_tab[ii].mut.lock_shared();
#elif defined NON_RW_MUTEX
			//mutex_bc_cons_type::scoped_lock lock (mutex_bc_cons);
			scoped_lock_of<mutex_bc_cons_type> lock{mutex_bc_cons};
#else
			mutex_bc_cons_type::scoped_lock lock (mutex_bc_cons, false);
#endif
			ivector x_loc = x;
#if defined DISTR_MUTEX
			for (int ii = VecLen(p.x0); ii >= 1; --ii) mutex_bc_cons_tab[ii].mut.unlock_shared();
#else
			lock.release();
#endif
			int result = bc3revise<thread_shared> (p, x_loc, interval(0.0), eq_num, var_num, eps, which, modified);
			++stat->num_bc3rev;
			//cout << "result = " << result << endl;
			if (result == -1) koniec = true;//return -1;
#if defined DISTR_MUTEX
			lock.acquire(mutex_bc_cons_tab[var_num].mut);//, true);
#else
			lock.acquire(mutex_bc_cons);//, true);
#endif
			x[var_num] = x_loc[var_num];
			//x[pairs_for_Ncmp[i].second] = x_loc[pairs_for_Ncmp[i].second];
/*#if defined DISTR_MUTEX
#else
			lock.release();
#endif*/
		});
		if (koniec) return -1;
	} while (modified);
	return 0;
}

