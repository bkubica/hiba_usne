//#include <interval.h>

int solve_quadratic_eq (const interval &x, const interval &a, const interval &b, const interval &c,
			interval &x1, interval &x2) {
	if (0.0 <= a) {
		//cout << "unapplicable: a = " << a << endl;
		return -2; //nothing changes; x1 and x2 not set
	}
	++stat->num_quadr;
	//cout << "solve_quadratic_eq - applicable\n";
	//cout << "x = " << x << endl;
	//cout << "a = " << a << ", b = " << b << ", c = " << c << endl;
	interval delta = sqr(b) - 4.0*a*c;
	//cout << "delta = " << delta << endl;
	if (Sup(delta) < 0.0) {
		//cout << "no solutions: delta negative, delta = " << delta << endl;
		++stat->num_del_quadr_delta_neg;
		return -1;
	}
	interval sqrt_delta = sqrt(delta & interval(0.0, Infinity));
	//cout << "sqrt_delta = " << sqrt_delta << endl;
	interval x1_ = -(b + sqrt_delta)/a/2;
	interval x2_ = (-b + sqrt_delta)/a/2;
	// consistency enforsing...
	if (!(0.0 <= x2_)) x1_ &= c/a/x2_;
	if (!(0.0 <= x1_)) x2_ &= c/a/x1_;
	// x1 and x2 should be consistent
	//cout << "x1_ = " << x1_ << endl;
	//cout << "x2_ = " << x2_ << endl;
	if (!Disjoint (x1_, x2_)) {
		x1 = x1_ | x2_;
		if (Disjoint (x1, x)) {
			//cout << "the single solution disjoint with the base\n";
			++stat->num_del_quadr_disjoint;
			return -1;
		}
		//even if (x1_ \cup x2_) \subset x, we can have more than one solution
		x1 &= x;
		return 0;
	}
	// x1_ and x2_ do not coincide
	if (Disjoint (x1_, x)) {
		if (Disjoint (x2_, x)) {
			//cout << "no solutions: both disjoint with the base\n";
			++stat->num_del_quadr_disjoint;
			return -1;
		}
		if (in(x2_, x)) {
			x1 = x2_;
			return 1;
		}
		x1 = x & x2_;
		return 0;
	}
	if (Disjoint (x2_, x)) {
		if (in(x1_, x)) {
			x1 = x1_;
			return 1;
		}
		x1 = x & x1_;
		return 0;
	}
	x1 = x & x1_;
	x2 = x & x2_;
	++stat->num_bis_quadr;
	return 3;
}


int use_quadratic (const problem_desc &p, int i, ivector &x, ivector &x2, intvector &which) {
	//cout << "use_quadratic\n";
	interval y, y0;
	ivector g, g0;
	imatrix H;
	ivector xc = ivector(Inf(x) + Sup(x))/2;
	p.compute_grad_f (xc, i, y0, g0); ++stat->num_grad_f;
	p.compute_hess_f (x, i, y, g, H); ++stat->num_hess_f;
	//cout << "H = " << H << endl;
	//cout << "g0 = " << g0 << endl;
	//cout << "y0 = " << y0 << endl;
	int n = VecLen(x);
	//x2 = x;
	ivector v = x - xc;
	which = 1;
	//int res = 1;
	for (int j = 1; j <= n; ++j) {
		//cout << "j = " << j << endl;
		interval a = H[j][j]/2;
		if (0.0 <= a) {
			//cout << "unapplicable: a = " << a << endl;
			//res = 0;
			continue;
			//return -2; //nothing changes; x1 and x2 not set
		}
		interval b = g0[j], c = y0;
		for (int ii = 1; ii <= n; ++ii) {
			if (ii != j) {
				b += H[ii][j]*v[ii];
				c += H[ii][ii]*sqr(v[ii])/2 + g0[ii]*v[ii];
				for (int jj = ii + 1; jj <= n; ++jj) {
					c += H[ii][jj]*v[ii]*v[jj];
				}
			}
			// czegos tu brakuje....................
		}
		//cout << "a = " << a << endl;
		//cout << "b = " << b << endl;
		//cout << "c = " << c << endl;
		//interval x1_, x2_;
		interval v1, v2;
		int result = solve_quadratic_eq (v[j], a, b, c, v1, v2); //++stat->num_quadr;
		//cout << "  result = " << result << endl;
		if (result == -2) {
			//res = 0;
			continue;
		}
		//res *= result;
		if (result == -1) return -1;
		x[j] = xc[j] + v1;
		if (result == 1) {
			which[j] = 0;
			return 1;
		}
		if (result == 3) {
			x2 = x;
			x2[j] = xc[j] + v2;
			return 3;
		}
		//cout << "currently, x = " << x << endl;
		//cout << "currently, x2 = " << x2 << endl;
	}
	return 0;//res;
}

