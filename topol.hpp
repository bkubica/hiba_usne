#if !defined __TOPOL_HPP__
#define __TOPOL_HPP__

int check_borsuk_for_i (const problem_desc &p, const cxsc::ivector &x1, const cxsc::ivector &x2) {
	//const int n = VecLen(x1);
	//assert (VecLen(x2) == n);
	cxsc::ivector y1(p.num_fun), y2(p.num_fun);
	p.compute_all_f(x1, y1);
	p.compute_all_f(x2, y2);
	{
		cxsc::imatrix Jac(p.num_fun, VecLen(x1));
		p.compute_all_grad_f(x1, y1, Jac);
		cxsc::ivector x1c { (Inf(x1) + Sup(x1))*0.5 };
		cxsc::ivector y1c;
		p.compute_all_f(x1c, y1c);
		y1 &= y1c + Jac*(x1 - x1c);
		;
		//cxsc::imatrix Jac(p.num_fun, VecLen(x2));
		p.compute_all_grad_f(x2, y2, Jac);
		cxsc::ivector x2c { (Inf(x2) + Sup(x2))*0.5 };
		cxsc::ivector y2c;
		p.compute_all_f(x2c, y2c);
		y2 &= y2c + Jac*(x2 - x2c);
	}
	//cout << "x1 = " << x1 << "\nx2 = " << x2 << "\n";
	//cout << "y1 = " << y1 << "\ny2 = " << y2 << "\n";
	cxsc::interval result (0.0, Infinity);
	for (int j = 1; j <= p.num_fun; ++j) {
		if (0.0 <= y2[j]) {
			xinterval division = y1[j] % y2[j];
			cxsc::ivector vec = result & division;
			if (vec[1] == EmptyIntval()) return 1;
			if (vec[2] == EmptyIntval()) result = vec[1];
			else result = vec[1] | vec[2];
			//return 0;
		}
		else {
			//cout << "y2 nonzero: j = " << j << "\n";
			cxsc::interval division = y1[j]/y2[j];
			if (Disjoint(result, division)) return 1;
			result &= division;
		}
		//cout << "result(" << j << ") = " << result << "\n";
	}
	return (Sup(result) <= 0.0);
	//return 0;
}


int check_borsuk (const problem_desc &p, const cxsc::ivector &x, const intvector &var_num) {
	//cout << "var_num = " << var_num << "\n";
	for (int it = 1; it <= p.num_fun; ++it) {
		const int i = var_num[it];
		cxsc::ivector x1{x};
		SetInf(x1[i], Sup(x1[i]));
		cxsc::ivector x2{x};
		SetSup(x2[i], Inf(x2[i]));
		//cout << "for i = " << i << " : " << check_borsuk_for_i(p, x1, x2) << "\n";
		if (!check_borsuk_for_i(p, x1, x2)) return 0;
	}
	return 1;
}


int check_borsuk_subdiv (const problem_desc &p, const cxsc::ivector &x, const intvector &var_num/*, const int num_sub*/) {
	//cout << "var_num = " << var_num << "\n";
	std::vector< std::pair<cxsc::ivector, cxsc::ivector> > subfacets;
	subfacets.clear();
	for (int it = 1; it <= p.num_fun; ++it) {
		const int i = var_num[it];
		cxsc::ivector x1{x};
		SetInf(x1[i], Sup(x1[i]));
		cxsc::ivector x2{x};
		SetSup(x2[i], Inf(x2[i]));
		//cout << "for i = " << i << " : " << check_borsuk_for_i(p, x1, x2) << "\n";
		if (!check_borsuk_for_i(p, x1, x2)) return 0;
	}
	return 1;
}

#endif

