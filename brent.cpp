
#include "solver.hpp"
#include "problem_desc_impl.hpp"

#include <string>

#define N 10
#define M N

// N - variables, M - equations

template <int level, SparsityLevel sparse_mode, int n, class T/* = cxsc::interval*/>
adhc_ari<level, sparse_mode, n, T> eq (const adhc_vector<level, sparse_mode, n, T> &x, const int num) {
	adhc_ari<level, sparse_mode, n, T> result;
	if (num == 1) result = 3*x[1]*(x[2] - 2*x[1])+sqr(x[2])*0.25;
	else if (num == N) result = 3*x[N]*(20.0 - 2*x[N] + x[N - 1]) + sqr(20.0 - x[N - 1])*0.25;
	else result = 3*x[num]*(x[num + 1] - 2*x[num] + x[num - 1]) + sqr(x[num + 1] - x[num - 1])*0.25;
	return result;
}


template <int level, SparsityLevel sparse_mode, int n, class T/* = cxsc::interval*/>
struct brent {
	adhc_ari<level, sparse_mode, n, T> operator() (const adhc_vector<level, sparse_mode, n, T> &x, const int i=0) const {
		return eq(x, i);
	};
};


//#define SPARSITY SparsityLevel::dense
//#define SPARSITY sparse
#define SPARSITY SparsityLevel::highly_sparse
//#define SPARSITY SparsityLevel::another_sparse


int main() {
#include "num_threads.hpp"
	opdotprec = 1;
	ivector x(N);
	x = interval (-1e8, 1e8);
	real eps = 1e-7;
	problem_desc_impl<SPARSITY, N, M, brent> p(x);
	solver s (num_threads);
	cout << "x0 = " << x << endl;
	create_func creat = Herbort_create_pair_list;
	//create_func creat = Goulard_create_pair_list;
	//create_func creat = GE_create_pair_list;
	//create_func creat = Goulard_GE_create_pair_list;
	s.branch_and_bound (p, eps, creat);
//#define SHOW
#if defined SHOW
	cout << "Possible: " << endl;
	for (auto &x : s.sol) {
		x.show();
	}
	cout << endl << "Guaranteed: " << endl;
	for (auto &x : s.guar_sol) {
		x.show();
	}
	std::cout << "========\n";
#endif
	s.show_results();
	s.stat_show();
	return 0;
}

