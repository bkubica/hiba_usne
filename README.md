**HIBA_USNE** = Heuristical Interval Branch-and-prune Algorithm for Underdetermined and well-determined Systems of Nonlinear Equations

1. Introduction.

HIBA_USNE is a C++ interval solver for nonlinear systems.


2. License.

The software is distributed under the GPLv2 free software license.
Please, consult the COPYING file for the details.


3. Requirements.

**HIBA_USNE** library depends on a few others:
 - C-XSC or survive-CXSC,
 - ADHC,
 - a BLAS implementation: OpenBLAS or MKL (or other ones, possibly).


C-XSC, that used to be available at: <http://www.xsc.de>
As this library has not been updated since 2014 (and it is, probably, not
compatible with the C++17 or newer standards), it has been forked by the
author. The survive-CXSC library is available at:
<https://gitlab.com/bkubica/survive-cxsc>
Using survive-CXSC is strongly recommended; the author gives no warranty that
ADHC will work with the older counterpart of the library, or even he gives a warranty, it will not.
For instance, the sqr() function will not work for cxsc::complex type, and
cxsc::l_real, cxsc::l_interval, ... types are *not* thread-safe, for the old C-XSC.

ADHC is an algorithmic differentiation C++ headers library, an it can be found at: <https://gitlab.com/bkubica/adhc>


In its current version, HIBA_USNE requires a C++ compiler compatible with at least
**C++14** standard (but using **C++17** is recommended).
The author has tested it on GCC, version 11.1.0, under the control of GNU/Linux operating system.


4. Compilation.

The compilation system for the examples and tests is a bit primitive, which
will hopefully be improved in the future versions.
Currently, there is a Makefile, in which you can set several variables, including:

(i) *CC* - the C++ compiler to be used (g++, icc, etc.).
(ii) *PLATFORM* - host or mic (not sure whether mic will work anymore).
(iii) *BLAS* - recommended values are openblas or mkl; other possible ones are non or atlas.
(iv) *USE_LP* = no (please do not change it at this development stage ;-) ).
(v) *ADHC_DIR* - as the name says, the directory, where ADHC header files are available.
(vi) *XSC_DIR* - the directory, where C-XSC or (preferably) survive-CXSC library
is installed. It is assumed that  this directory contains (at least) two
subdirectories: include/ and lib/.


5. Examples.

The example Makefile produces several examples, each in a distinct file:
- example
- broyden
- broy-trid
- almost
- puma
- rhein
- transistor
- hippopede
- n-R_planar
- n-R_planar-notrig
- diff_geom
- box3
- bratu
- kin1
- alexandre-under
- brent
- ext_freudenstein
- franek
- more-cosnard
- kinematics1
- neural-3-layer
- hopfield
- stationary-hopf 


6. The link to HIBA_USNE solver: <https://gitlab.com/bkubica/hiba_usne>

