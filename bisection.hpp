
int MaxSmear (const imatrix &J, const rvector w_x, int num_cols, int num_rows, int &i0) {
	// Sup(abs(x)) == mag(x) ???
	int i = 1, j, j0;
	//real s0 (0.0)/*= Sup(abs(J[1][1]))*w_x[1]*/, s;
	real s0 (0.0), s;
	//s0 = 0.0;
	do {
		j = 1;
		do {
			if ((s = Sup(abs(J[i][j]))*w_x[j]) > s0) {
				i0 = i;
				j0 = j;
				s0 = s;
			}
		} while (++j <= num_cols);
	} while (++i <= num_rows);
	return j0;
}


int MaxSmearDiam (const imatrix &J, const rvector w_x, int num_cols, int num_rows, int &i0) {
	int i = 1, j, j0;
	//real s0 (0.0)/*= diam(J[1][1])*w_x[1]*/, s;
	real s0 (0.0), s;
	//s0 = 0.0;
	do {
		j = 1;
		do {
			if ((s = diam(J[i][j])*w_x[j]) > s0) {
				i0 = i;
				j0 = j;
				s0 = s;
			}
		} while (++j <= num_cols);
	} while (++i <= num_rows);
	return j0;
}


int MaxSumMagnitude (const imatrix &J, const rvector w_x, int num_cols, int num_rows) {
	// Sup(abs(x)) == mag(x) ???
	int i, j = 1, j0 = 1;
	real mag (0.0), mag0 (0.0);
	do {
		i = 1;
		mag = 0.0;
		do mag += Sup(abs(J[i][j]))*w_x[j];
		while (++i <= num_rows);
		if (mag > mag0) {
			j0 = j;
			mag0 = mag;
		}
	} while (++j <= num_cols);
	return j0;
}


int SecondMaxSumMagnitude (const imatrix &J, const rvector w_x, int num_cols, int num_rows) {
	// Sup(abs(x)) == mag(x) ???
	int i, j = 1, j_max = 1, j_second_max = -1;
	real mag (0.0), max_mag (0.0), second_max_mag (0.0);
	do {
		i = 1;
		mag = 0.0;
		do mag += Sup(abs(J[i][j]))*w_x[j];
		while (++i <= num_rows);
		if (mag > max_mag) {
			j_second_max = j_max;
			second_max_mag = max_mag;
			j_max = j;
			max_mag = mag;
		}
		else if (mag > second_max_mag) {
			j_second_max = j;
			second_max_mag = mag;
		}
	} while (++j <= num_cols);
	return j_second_max;
}


int MinSumMagnitude (const imatrix &J, const rvector w_x, int num_cols, int num_rows) {
	// Sup(abs(x)) == mag(x) ???
	int i, j = 1, j0 = 1;
	real mag (0.0), mag0 (0.0);
	do {
		i = 1;
		mag = 0.0;
		do mag += Sup(abs(J[i][j]))*w_x[j];
		while (++i <= num_rows);
		if (mag < mag0) {
			j0 = j;
			mag0 = mag;
		}
	} while (++j <= num_cols);
	return j0;
}


int MinMaxRawJMagnitude (const imatrix &J, const rvector w_x, int num_cols, int num_rows) {
	// Sup(abs(x)) == mag(x) ???
	int i, j = 1, j0 = 1;
	real mag (0.0), mag0 (Infinity/*0.0*/), max_mag_i (0.0);
	do {
		i = 1;
		max_mag_i = 0.0;
		do {
			//mag = Sup(abs(J[i][j]))*w_x[j];
			mag = Sup(abs(J[i][j]));
			if (mag > max_mag_i) max_mag_i = mag;
		} while (++i <= num_rows);
		//if (diam(J[i][j]) < 1e-4) continue;
		//if (max_mag_i*w_x[j] < 1000.0) continue;
		//if (max_mag_i < 1.0) continue;
		if (max_mag_i < mag0) {
			j0 = j;
			mag0 = max_mag_i;
		}
	} while (++j <= num_cols);
	return j0;
}


int SecondMaxRawJMagnitude (const imatrix &J, const rvector w_x, int num_cols, int num_rows) {
	// Sup(abs(x)) == mag(x) ???
	int i, j = 1, j_max = 1, j_second_max = -1;
	real mag (0.0), max_mag (0.0), second_max_mag (0.0);
	do {
		i = 1;
		real max_mag_i = 0.0;
		do {
			mag = Sup(abs(J[i][j]));
			if (mag > max_mag_i) max_mag_i = mag;
		} while (++i <= num_rows);
		if (max_mag_i > max_mag) {
			j_second_max = j_max;
			second_max_mag = max_mag;
			j_max = j;
			max_mag = max_mag_i;
		}
		else if (max_mag_i > second_max_mag) {
			j_second_max = j;
			second_max_mag = max_mag_i;
		}
	} while (++j <= num_cols);
	return j_second_max;
}


int MaxMinRawJMagnitude (const imatrix &J, const rvector w_x, int num_cols, int num_rows) {
	// added 2014-06-02
	// Sup(abs(x)) == mag(x) ???
	int i, j = 1, j0 = 1;
	real mag (0.0), mag0 (Infinity/*0.0*/), min_mag_i (0.0);
	do {
		i = 1;
		min_mag_i = Infinity;
		do {
			//mag = Sup(abs(J[i][j]))*w_x[j];
			mag = Sup(abs(J[i][j]));
			if (mag <= min_mag_i) min_mag_i = mag;
		} while (++i <= num_rows);
		//if (diam(J[i][j]) < 1e-4) continue;
		//if (max_mag_i*w_x[j] < 1000.0) continue;
		//if (max_mag_i < 1.0) continue;
		if (min_mag_i > mag0) {
			j0 = j;
			mag0 = min_mag_i;
		}
	} while (++j <= num_cols);
	return j0;
}


int ZeroNearBound (const ivector &y, const imatrix &J, const rvector w_x, int num_cols, int num_rows) {
	// Sup(abs(x)) == mag(x) ???
	int i = 1, i0 = 1, j = 1, j0 = 1;
	real delta, min_delta = min (-Inf(y[1]), Sup(y[1]));
	while (++i <= num_rows) {
		if ((delta = min (-Inf(y[i]), Sup(y[i]))) < min_delta) {
			min_delta = delta;
			i0 = i;
		}
	}
	real s0 = Sup(abs(J[i0][1]))*w_x[1], s;
	while (++j <= num_cols) {
		if ((s = Sup(abs(J[i0][j]))*w_x[j]) > s0)  {
			j0 = j;
			s = s0;
		}
	}
	return j0;
}


int heuristic_sonic (const ivector &y, const imatrix &J, const rvector w_x, int num_cols, int num_rows, const real &eps) {
	int j = 1, j_max = 1;
	real w_avg = 0.0, w_max = 0.0;
	do {
		if (w_x[j] > w_max) {
			w_max = w_x[j];
			j_max = j;
		}
		w_avg += w_x[j];
	} while (++j <= num_cols);
	w_avg /= num_cols;
	if (w_max/w_avg > 20.0) return j_max;
	int i = 1, i0 = 1, j0 = 1;
	real delta, min_delta = min (-Inf(y[1]), Sup(y[1]));
	while (++i <= num_rows) {
		if ((delta = min (-Inf(y[i]), Sup(y[i]))) < min_delta) {
			min_delta = delta;
			i0 = i;
		}
	}
	real s0 = Sup(abs(J[i0][1]))*w_x[1], s;
	j = 1;
	while (++j <= num_cols) {
		if ((s = Sup(abs(J[i0][j]))*w_x[j]) > s0)  {
			j0 = j;
			s = s0;
		}
	}
	if ((diam(y[i0]) > 1e-2)&&(diam(y[i0]) > min_delta*3.0)) return j0;
	//...
	j0 = MaxSmearDiam (J, w_x, num_cols, num_rows, i0);
	if (Sup(abs(y[i0])) < diam(y[i0])) return j0;
	return MaxSmear (J, w_x, num_cols, num_rows, i0);
}


int heuristic (const imatrix &J, const rvector w_x, int num_cols, int num_rows, const real &eps) {
	int j = 1, j_min = 1, j_max = 1;
	real w_min = w_x[1], w_max = w_min;
	do if (w_x[j] > w_max) {
		w_max = w_x[j];
		j_max = j;
	}
	else if (w_x[j] < w_min) {
		w_min = w_x[j];
		j_min = j;
	} while (++j <= num_cols);
	//if (w_max/w_min > 2) return j_max;
	if (w_max > w_min*8) return j_max;
	else {
		//return MinSumMagnitude (J, w_x, num_cols, num_rows);
		//j = MinSumMagnitude (J, w_x, num_cols, num_rows);
		//j = MinMaxMagnitude (J, w_x, num_cols, num_rows);
		j = MinMaxRawJMagnitude (J, w_x, num_cols, num_rows);
		//return j;
		//if (w_x[j] > eps*4096) return j;
		//if (w_x[j] > eps*8192) return j;
		if (w_x[j] > eps*1024) return j;
		//if (w_x[j] > eps*16384) return j;
		//if (w_x[j] > eps*64) return j;
		else return j_max;
	} 
}


int heuristic_extended (const imatrix &J, const rvector w_x, const intvector &which,
			int num_cols, int num_rows, const real &eps) {
	int j = 1, j_min = 1, j_max = 1;
	bool Newt_unreduced = true;
	real w_min = w_x[1], w_max = w_min;
	do {
		if (!which[j]) Newt_unreduced = false;
		if (w_x[j] > w_max) {
			w_max = w_x[j];
			j_max = j;
		}
		else if (w_x[j] < w_min) {
			w_min = w_x[j];
			j_min = j;
		}
	} while (++j <= num_cols);
	//if (Newt_unreduced || (w_max/w_min > 2)) return j_max;
	if (Newt_unreduced || (w_max > w_min*8)) return j_max;
	//if (Newt_unreduced || (w_max > w_min*2)) return j_max;
	else {
		//j = MinSumMagnitude (J, w_x, num_cols, num_rows);
		//j = MinMaxMagnitude (J, w_x, num_cols, num_rows);
		j = MinMaxRawJMagnitude (J, w_x, num_cols, num_rows);
		//return j;
		//if (w_x[j] > eps*4096) return j;
		//if (w_x[j] > eps*8192) return j;
		//if (w_x[j] > eps*16384) return j;
		//if (w_x[j] > eps*2048) return j;
		if (w_x[j] > eps*1024) return j;
		//if (w_x[j] > eps*64) return j;
		//if (w_x[j] > eps) return j;
		else return j_max;
	}
}


int heuristic_norm (const imatrix &J, const rvector w_x, const intvector &which,
		    int num_cols, int num_rows, const real &eps) {
	// choose the widest component of x, that has NOT been reduced by Newton tests
	real w_max = 0.0;
	int j = 1, j_max;
	do if ((which[j])&&(w_x[j] > w_max)) {
		w_max = w_x[j];
		j_max = j;
	} while (++j <= num_cols);
	// assertion: at least one of the components of which is a nonzero (otherwise why bisecting?),
	// so j_max has been set
	return j_max;
}


int heuristic_norm_impr (const imatrix &J, const rvector w_x, const intvector &which,
		    int num_cols, int num_rows, const real &eps) {
	// choose the widest component of x, that has NOT been reduced by Newton tests
	real w_max = 0.0, w_max_unnarrowed = 0.0, w_min = w_x[1];
	int j = 1, j_max, j_max_unnarrowed, j_min = 1;
	bool Newt_unreduced = true;
	//do if ((which[j])&&(w_x[j] > w_max)) {
	do {
		if (w_x[j] > w_max) {
			w_max = w_x[j];
			j_max = j;

		}
		else if (w_x[j] < w_min) {
			w_min = w_x[j];
			j_min = j;
		}
		if ((which[j])&&(w_x[j] > w_max_unnarrowed)) {
			w_max_unnarrowed = w_x[j];
			j_max_unnarrowed = j;
		}
		if (!which[j]) Newt_unreduced = false;
	} while (++j <= num_cols);
	//return j_max;
	if (Newt_unreduced || (w_max > w_max_unnarrowed*1.5)) return j_max;
	//if (w_max > w_max_unnarrowed && w_max > w_min*8) return j_max;
	//if ((j_max == j_max_unnarrowed)||(w_max > w_max_unnarrowed*2)) return j_max;
	//if (w_max > w_max_unnarrowed*2) return j_max;
	if (w_max_unnarrowed > w_min*8) return j_max_unnarrowed;
	j = MinMaxRawJMagnitude (J, w_x, num_cols, num_rows);
	if (w_x[j] > eps*1024) return j;
	return j_max_unnarrowed;
}


int heuristic_norm_3 (const imatrix &J, const rvector w_x, const intvector &which,
		    int num_cols, int num_rows, const real &eps) {
	// choose the widest component of x, that has NOT been reduced by Newton tests
	real w_max = 0.0, w_max_unnarrowed = 0.0, w_min = w_x[1];
	int j = 1, j_max, j_max_unnarrowed, j_min = 1;
	bool Newt_unreduced = true;
	do {
		if (w_x[j] > w_max) {
			w_max = w_x[j];
			j_max = j;

		}
		else if (w_x[j] < w_min) {
			w_min = w_x[j];
			j_min = j;
		}
		if ((which[j])&&(w_x[j] > w_max_unnarrowed)) {
			w_max_unnarrowed = w_x[j];
			j_max_unnarrowed = j;
		}
		if (!which[j]) Newt_unreduced = false;
	} while (++j <= num_cols);
	//return j_max;
	if (Newt_unreduced || (w_max > w_max_unnarrowed*1.5)) return j_max;
	//if (w_max > w_max_unnarrowed && w_max > w_min*8) return j_max;
	//if ((j_max == j_max_unnarrowed)||(w_max > w_max_unnarrowed*2)) return j_max;
	//if (w_max > w_max_unnarrowed*2) return j_max;
	if (w_max_unnarrowed > w_min*8) return j_max_unnarrowed;
	j = MinMaxRawJMagnitude (J, w_x, num_cols, num_rows);
	if (w_x[j] >= 1e-1) return j;
	return j_max_unnarrowed;
}


int heuristic_norm_4 (const imatrix &J, const rvector w_x, const intvector &which,
		      int num_cols, int num_rows, const real &eps) {
	// choose the widest component of x, that has NOT been reduced by Newton tests
	real w_max = 0.0, w_max_unnarrowed = 0.0, w_min = w_x[1];
	int j = 1, j_max, j_max_unnarrowed, j_min = 1;
	//int below = 0;
	bool Newt_unreduced = true;
	do {
		//if (w_x[j] < 0.125) ++below;
		if (w_x[j] > w_max) {
			w_max = w_x[j];
			j_max = j;

		}
		else if (w_x[j] < w_min) {
			w_min = w_x[j];
			j_min = j;
		}
		if ((which[j])&&(w_x[j] > w_max_unnarrowed)) {
			w_max_unnarrowed = w_x[j];
			j_max_unnarrowed = j;
		}
		if (!which[j]) Newt_unreduced = false;
	} while (++j <= num_cols);
	int i0;
	//if (Newt_unreduced && below <= num_cols - num_rows/*w_max >= 0.5*/) return MaxSmear(J, w_x, num_cols, num_rows, i0);
	//if (Newt_unreduced || (w_max > w_max_unnarrowed*1.5)) return j_max;
	if (Newt_unreduced) {
		if (num_cols <= num_rows) {
			// not an underdetermined problem
			//int jms = MaxSmear(J, w_x, num_cols, num_rows, i0);
			int jms = MaxSumMagnitude(J, w_x, num_cols, num_rows);
			//return jms;
			//int jmsd = MaxSmearDiam(J, w_x, num_cols, num_rows, i0);
			//jmsd = jms;
			//if (jms == jmsd) return jms;
			//if (w_x[jms] > w_x[jmsd] && w_max < 8*w_x[jms]) return jms;
			//if (w_max < 8*w_x[jmsd]) return jmsd;
			if (w_max < 16*w_x[jms]) return jms;
			return j_max;
		}
		else {
			//j = MinMaxRawJMagnitude (J, w_x, num_cols, num_rows);
			//if (w_max < 2*w_x[j]) return j;
			return j_max;
		}
	}
	//if (w_max > w_max_unnarrowed*1.5) return j_max;
	if (w_max_unnarrowed > w_min*8) return j_max_unnarrowed;
	//if (false){//below <= num_cols - num_rows/2) {
	if (num_cols <= num_rows){
		//j = MaxSmearDiam(J, w_x, num_cols, num_rows, i0);
		//j = MaxSmear(J, w_x, num_cols, num_rows, i0);
		j = MaxSumMagnitude(J, w_x, num_cols, num_rows);
	}
	else {
		j = MinMaxRawJMagnitude (J, w_x, num_cols, num_rows);
	}
	if (w_x[j] >= 1e-1) return j;
	return j_max_unnarrowed;
}



int heuristic_norm_5 (const imatrix &J, const rvector w_x, const intvector &which,
		    int num_cols, int num_rows, const real &eps) {
	// choose the widest component of x, that has NOT been reduced by Newton tests
	real w_max = 0.0, w_max_unnarrowed = 0.0, w_min = w_x[1];
	int j = 1, j_max, j_max_unnarrowed, j_min = 1;
	bool Newt_unreduced = true;
	do {
		if (w_x[j] > w_max) {
			w_max = w_x[j];
			j_max = j;

		}
		else if (w_x[j] < w_min) {
			w_min = w_x[j];
			j_min = j;
		}
		if ((which[j])&&(w_x[j] > w_max_unnarrowed)) {
			w_max_unnarrowed = w_x[j];
			j_max_unnarrowed = j;
		}
		if (!which[j]) Newt_unreduced = false;
	} while (++j <= num_cols);
	//return j_max;
	if (Newt_unreduced || (w_max > w_max_unnarrowed*1.5)) {
		real sum (0.0);
		for (int i = 1; i <= num_rows; ++i) sum += diam(J[i][j_max]);
		if (sum > 0.5) return j_max;
		int i0;
		return MaxSmearDiam (J, w_x, num_cols, num_rows, i0);
	}
	//if (w_max > w_max_unnarrowed && w_max > w_min*8) return j_max;
	//if ((j_max == j_max_unnarrowed)||(w_max > w_max_unnarrowed*2)) return j_max;
	//if (w_max > w_max_unnarrowed*2) return j_max;
	if (w_max_unnarrowed > w_min*8) return j_max_unnarrowed;
	j = MinMaxRawJMagnitude (J, w_x, num_cols, num_rows);
	real sum (0.0);
	for (int i = 1; i <= num_rows; ++i) sum += diam(J[i][j]);
	if (w_x[j] >= 1e-1 && sum >= 0.5) return j;
	sum = 0.0;
	for (int i = 1; i <= num_rows; ++i) sum += diam(J[i][j_max_unnarrowed]);
	if (sum > 0.5 && w_x[j] < 2.0) return j_max_unnarrowed;
	int i0;
	return MaxSmearDiam (J, w_x, num_cols, num_rows, i0);
}


int heuristic_norm_6 (const imatrix &J, const rvector w_x, const intvector &which,
		      int num_cols, int num_rows, const real &eps) {
	// choose the widest component of x, that has NOT been reduced by Newton tests
	real w_max = 0.0, w_max_unnarrowed = 0.0, w_min = w_x[1];
	int j = 1, j_max, j_max_unnarrowed, j_min = 1;
	bool Newt_unreduced = true;
	do {
		if (w_x[j] > w_max) {
			w_max = w_x[j];
			j_max = j;

		}
		else if (w_x[j] < w_min) {
			w_min = w_x[j];
			j_min = j;
		}
		if ((which[j])&&(w_x[j] > w_max_unnarrowed)) {
			w_max_unnarrowed = w_x[j];
			j_max_unnarrowed = j;
		}
		if (!which[j]) Newt_unreduced = false;
	} while (++j <= num_cols);
	int i0;
	int i_J_max, j_sum_max;;
	real J_max, J_sum, J_sum_max (0.0);
	if (Newt_unreduced) {
		for (j = 1; j <= num_cols; ++j) {
			i_J_max = 1;
			J_max = Sup(abs(J[1][j]));
			for (int i = 2; i <= num_rows; ++i) {
				if (Sup(abs(J[i][j])) > J_max) {
					J_max = Sup(abs(J[i][j]));
					i_J_max = i;
				}
			}
			J_sum = 0.0;
			for (int i = 1; i <= num_rows; ++i) if (i != i_J_max) J_sum += Sup(abs(J[i][j]));
			if (J_sum > J_sum_max) {
				J_sum_max = J_sum;
				j_sum_max = j;
			}
		}
		if (w_max < 16*w_x[j_sum_max]) return j_sum_max;
		else return j_max;
		/*if (num_cols <= num_rows) {
			// not an underdetermined problem
			int jms = MaxSumMagnitude(J, w_x, num_cols, num_rows);
			if (w_max < 16*w_x[jms]) return jms;
			return j_max;
		}
		else {
			//j = MinMaxRawJMagnitude (J, w_x, num_cols, num_rows);
			//if (w_max < 2*w_x[j]) return j;
			return j_max;
		}*/
	}
	//if (w_max > w_max_unnarrowed*1.5) return j_max;
	if (w_max_unnarrowed > w_min*8) return j_max_unnarrowed;
	//if (false){//below <= num_cols - num_rows/2) {
	if (num_cols <= num_rows){
		//j = MaxSmearDiam(J, w_x, num_cols, num_rows, i0);
		//j = MaxSmear(J, w_x, num_cols, num_rows, i0);
		j = MaxSumMagnitude(J, w_x, num_cols, num_rows);
	}
	else {
		j = MinMaxRawJMagnitude (J, w_x, num_cols, num_rows);
	}
	if (w_x[j] >= 1e-1) return j;
	return j_max_unnarrowed;
}



int heuristic_norm_7 (const imatrix &J, const rvector w_x, const intvector &which,
		      int num_cols, int num_rows, const real &eps) {
	// choose the widest component of x, that has NOT been reduced by Newton tests
	real w_max = 0.0, w_max_unnarrowed = 0.0, w_min = w_x[1];
	int j = 1, j_max, j_max_unnarrowed;//, j_min = 1;
	//int below = 0;
	bool Newt_unreduced = true;
	do {
		//if (w_x[j] < 0.125) ++below;
		if (w_x[j] > w_max) {
			w_max = w_x[j];
			j_max = j;

		}
		else if (w_x[j] < w_min) {
			w_min = w_x[j];
			//j_min = j;
		}
		if ((which[j])&&(w_x[j] > w_max_unnarrowed)) {
			w_max_unnarrowed = w_x[j];
			j_max_unnarrowed = j;
		}
		if (!which[j]) Newt_unreduced = false;
	} while (++j <= num_cols);
	int i0;
	//if (Newt_unreduced && below <= num_cols - num_rows/*w_max >= 0.5*/) return MaxSmear(J, w_x, num_cols, num_rows, i0);
	//if (w_max > w_max_unnarrowed*2) return j_max;
	if (Newt_unreduced) {
		/*if (num_cols <= num_rows) {
			// not an underdetermined problem
			//int jms = MaxSmear(J, w_x, num_cols, num_rows, i0);
			int jms = MaxSumMagnitude(J, w_x, num_cols, num_rows);
			//return jms;
			//int jmsd = MaxSmearDiam(J, w_x, num_cols, num_rows, i0);
			//jmsd = jms;
			//if (jms == jmsd) return jms;
			//if (w_x[jms] > w_x[jmsd] && w_max < 8*w_x[jms]) return jms;
			//if (w_max < 8*w_x[jmsd]) return jmsd;
			if (w_max < 16*w_x[jms]) return jms;
			return j_max;
		}
		else {
			//j = MaxMinRawJMagnitude (J, w_x, num_cols, num_rows);
			//j = MinMaxRawJMagnitude (J, w_x, num_cols, num_rows);
			//if (w_max < 2*w_x[j]) return j;
			return j_max;
		}*/
		int j1 = SecondMaxSumMagnitude(J, w_x, num_cols, num_rows);
		int j2 = SecondMaxRawJMagnitude(J, w_x, num_cols, num_rows);
		j = (w_x[j1] > w_x[j2])?j1:j2;
		if (w_max < 2*w_x[j]) return j;
		return j_max;
	}
	//if (w_max > w_max_unnarrowed*1.5) return j_max;
	if (w_max_unnarrowed > w_min*2) return j_max_unnarrowed;
	//if (false){//below <= num_cols - num_rows/2) {
	/*if (num_cols <= num_rows){
		//j = MaxSmearDiam(J, w_x, num_cols, num_rows, i0);
		//j = MaxSmear(J, w_x, num_cols, num_rows, i0);
		j = MaxSumMagnitude(J, w_x, num_cols, num_rows);
	}
	else {
		j = MaxMinRawJMagnitude (J, w_x, num_cols, num_rows);
	}*/
	int j1 = SecondMaxSumMagnitude(J, w_x, num_cols, num_rows);
	int j2 = SecondMaxRawJMagnitude(J, w_x, num_cols, num_rows);
	j = (w_x[j1] > w_x[j2])?j1:j2;
	//j = SecondMaxSumMagnitude(J, w_x, num_cols, num_rows);
	if (w_x[j] >= 1e-1 || which[j]) return j;
	return j_max_unnarrowed;
}


int heuristic_norm_4a (const imatrix &J, const rvector w_x, const intvector &which,
		       int num_cols, int num_rows, const real &eps, const intvector &var_num) {
	// choose the widest component of x, that has NOT been reduced by Newton tests
	real w_max = 0.0, w_max_unnarrowed = 0.0, w_min = w_x[1];
	int j = 1, j_max, j_max_unnarrowed, j_min = 1;
	cout << "var_num = " << var_num << endl;
	//int below = 0;
	bool Newt_unreduced = true;
	do {
		//if (w_x[j] < 0.125) ++below;
		if (w_x[j] > w_max) {
			w_max = w_x[j];
			j_max = j;

		}
		else if (w_x[j] < w_min) {
			w_min = w_x[j];
			j_min = j;
		}
		if ((which[j])&&(w_x[j] > w_max_unnarrowed)) {
			w_max_unnarrowed = w_x[j];
			j_max_unnarrowed = j;
		}
		if (!which[j]) Newt_unreduced = false;
	} while (++j <= num_cols);
	int i0;
	//if (Newt_unreduced && below <= num_cols - num_rows/*w_max >= 0.5*/) return MaxSmear(J, w_x, num_cols, num_rows, i0);
	//if (Newt_unreduced || (w_max > w_max_unnarrowed*1.5)) return j_max;
	if (Newt_unreduced) {
		if (num_cols <= num_rows) {
			// not an underdetermined problem
			//int jms = MaxSmear(J, w_x, num_cols, num_rows, i0);
			int jms = MaxSumMagnitude(J, w_x, num_cols, num_rows);
			//return jms;
			//int jmsd = MaxSmearDiam(J, w_x, num_cols, num_rows, i0);
			//jmsd = jms;
			//if (jms == jmsd) return jms;
			//if (w_x[jms] > w_x[jmsd] && w_max < 8*w_x[jms]) return jms;
			//if (w_max < 8*w_x[jmsd]) return jmsd;
			if (w_max < 16*w_x[jms]) return jms;
			return j_max;
		}
		else {
			j = var_num[num_rows + 1];
			cout << "j = " << j << endl;
			//j = MinMaxRawJMagnitude (J, w_x, num_cols, num_rows);
			if (w_max < 2*w_x[j]) return j;
			return j_max;
		}
	}
	//if (w_max > w_max_unnarrowed*1.5) return j_max;
	if (w_max_unnarrowed > w_min*8) return j_max_unnarrowed;
	//if (false){//below <= num_cols - num_rows/2) {
	if (num_cols <= num_rows){
		//j = MaxSmearDiam(J, w_x, num_cols, num_rows, i0);
		//j = MaxSmear(J, w_x, num_cols, num_rows, i0);
		j = MaxSumMagnitude(J, w_x, num_cols, num_rows);
	}
	else {
		//j = MinMaxRawJMagnitude (J, w_x, num_cols, num_rows);
		j = var_num[num_rows + 1];
		cout << "\tj = " << j << endl;
	}
	if (w_x[j] >= 1e-1) return j;
	return j_max_unnarrowed;
}


int heuristic_norm_8 (const imatrix &J, const rvector w_x, const intvector &which,
		      int num_cols, int num_rows, const real &eps) {
	// choose the widest component of x, that has NOT been reduced by Newton tests
	real /*w_max = 0.0,*/ w_max_unnarrowed = 0.0, w_min = w_x[1];
	real max_mag_unnarrowed = 0.0;
	int j = 1, /*j_max,*/ j_max_unnarrowed, j_max_mag_unnarrowed, j_min = 1;
	bool Newt_unreduced = true;
	do {
		//if (w_x[j] < 0.125) ++below;
		/*if (w_x[j] > w_max) {
			w_max = w_x[j];
			j_max = j;

		}
		else */if (w_x[j] < w_min) {
			w_min = w_x[j];
			j_min = j;
		}
		if (which[j]){
			if(w_x[j] > w_max_unnarrowed) {
				w_max_unnarrowed = w_x[j];
				j_max_unnarrowed = j;
			}
			real mag = 0.0;
			int i = 1;
			do mag += Sup(abs(J[i][j]))*w_x[j];
			while (++i <= num_rows);
			if (mag > max_mag_unnarrowed) {
				j_max_mag_unnarrowed = j;
				max_mag_unnarrowed = mag;
			}
		}
		if (!which[j]) Newt_unreduced = false;
	} while (++j <= num_cols);
	if (Newt_unreduced) {
		if (num_cols <= num_rows) {
			// not an underdetermined problem
			//int jms = MaxSumMagnitude(J, w_x, num_cols, num_rows);
			int jms = j_max_mag_unnarrowed;
			if (w_max_unnarrowed < 16*w_x[jms]) return jms;
			return j_max_unnarrowed;
		}
		else {
			return j_max_unnarrowed;
		}
	}
	//if (w_max > w_max_unnarrowed*1.5) return j_max;
	//if (num_cols <= num_rows && w_max_unnarrowed < w_x[j_max_mag_unnarrowed]*8) return j_max_mag_unnarrowed;
	//if (w_max_unnarrowed > w_min*8 || w_x[j_max_mag_unnarrowed] < 0.1) return j_max_unnarrowed;
	//if (w_max_unnarrowed > w_min*8 || w_max_unnarrowed > w_x[j_max_mag_unnarrowed]*2) return j_max_unnarrowed;
	//if (num_cols > num_rows) return j_max_unnarrowed;
	if (w_x[j_max_mag_unnarrowed] < 0.1) return j_max_unnarrowed;
	return j_max_mag_unnarrowed;

	//if (w_max > w_max_unnarrowed*1.5) return j_max;
	/*if (w_max_unnarrowed > w_min*4 || w_max_unnarrowed > 2*w_x[j_max_mag_unnarrowed]) return j_max_unnarrowed;
	//j = j_max_mag_unnarrowed;
	//if (w_x[j] >= 1e-1) return j;
	//if (w_max_unnarrowed < 2*w_x[j_max_mag_unnarrowed]) return j_max_mag_unnarrowed;
	if (w_max > w_x[j_max_mag_unnarrowed]*4) return j_max;
	return j_max_mag_unnarrowed;*/
}

