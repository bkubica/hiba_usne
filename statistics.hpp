
#if !defined __STATISTICS_HPP__
#define __STATISTICS_HPP__

//#include <tbb/atomic.h>
#include <atomic>

#include "threaded_aux.hpp"

template <thread_privacy_t priv>
struct statistics {
	using stat_t = possibly_atomic<long int, priv>;

	stat_t num_f; stat_t num_grad_f; stat_t num_hess_f;
	stat_t num_ineq; stat_t num_grad_ineq; stat_t num_hess_ineq;
	stat_t num_del_ineq;
	stat_t num_bisec; stat_t num_bis_2; stat_t num_bis_3;
	stat_t num_precond; stat_t num_del_newt; stat_t num_bis_newt;
	stat_t num_bc3; stat_t num_bc3rev; stat_t num_del_bc;
	stat_t num_hc; stat_t num_hc_narrow; stat_t num_del_hc;
	stat_t num_bound_cons; stat_t num_bound_cons_recur; stat_t num_del_bound_cons;
	stat_t num_quadr; stat_t num_del_quadr_delta_neg; stat_t num_del_quadr_disjoint; stat_t num_bis_quadr;
	stat_t num_high_prec; stat_t num_del_high_prec; stat_t num_ver_high_prec;
	
	statistics() {
		num_f = 0L; num_grad_f = 0L; num_hess_f = 0L;
		num_ineq = 0L; num_grad_ineq = 0L; num_hess_ineq = 0L;
		num_del_ineq = 0L;
		num_bisec = 0L; num_bis_2 = 0L; num_bis_3 = 0L;
		num_precond = 0L; num_del_newt = 0L; num_bis_newt = 0L;
		num_bc3 = 0L; num_bc3rev = 0L; num_del_bc = 0L;
		num_hc = 0L; num_hc_narrow = 0L; num_del_hc = 0L;
		num_bound_cons = 0L; num_bound_cons_recur = 0L; num_del_bound_cons = 0L;
		num_quadr = 0L; num_del_quadr_delta_neg = 0L; num_del_quadr_disjoint = 0L; num_bis_quadr = 0L;
		num_high_prec = 0L; num_del_high_prec = 0L; num_ver_high_prec = 0L;
	};

	template <thread_privacy_t arg_priv>
	void add(const statistics<arg_priv> &arg) {
		num_f += arg.num_f; num_grad_f += arg.num_grad_f; num_hess_f += arg.num_hess_f;
		num_ineq += arg.num_ineq; num_grad_ineq += arg.num_grad_ineq; num_hess_ineq += arg.num_hess_ineq;
		num_del_ineq += arg.num_del_ineq;
		num_bisec += arg.num_bisec; num_bis_2 += arg.num_bis_2; num_bis_3 += arg.num_bis_3;
		num_precond += arg.num_precond; num_del_newt += arg.num_del_newt; num_bis_newt += arg.num_bis_newt;
		num_bc3 += arg.num_bc3; num_bc3rev += arg.num_bc3rev; num_del_bc += arg.num_del_bc;
		num_hc += arg.num_hc; num_hc_narrow += arg.num_hc_narrow; num_del_hc += arg.num_del_hc;
		num_bound_cons += arg.num_bound_cons; num_bound_cons_recur += arg.num_bound_cons_recur;
		num_del_bound_cons += arg.num_del_bound_cons;
		num_quadr += arg.num_quadr; num_del_quadr_delta_neg += arg.num_del_quadr_delta_neg;
		num_del_quadr_disjoint += arg.num_del_quadr_disjoint; num_bis_quadr += arg.num_bis_quadr;
		num_high_prec += arg.num_high_prec; num_del_high_prec += arg.num_del_high_prec;
		num_ver_high_prec += arg.num_ver_high_prec;
	};

	template <thread_privacy_t arg_priv>
	void add(statistics<arg_priv> &&arg) {
		num_f += arg.num_f; num_grad_f += arg.num_grad_f; num_hess_f += arg.num_hess_f;
		num_ineq += arg.num_ineq; num_grad_ineq += arg.num_grad_ineq; num_hess_ineq += arg.num_hess_ineq;
		num_del_ineq += arg.num_del_ineq;
		num_bisec += arg.num_bisec; num_bis_2 += arg.num_bis_2; num_bis_3 += arg.num_bis_3;
		num_precond += arg.num_precond; num_del_newt += arg.num_del_newt; num_bis_newt += arg.num_bis_newt;
		num_bc3 += arg.num_bc3; num_bc3rev += arg.num_bc3rev; num_del_bc += arg.num_del_bc;
		num_hc += arg.num_hc; num_hc_narrow += arg.num_hc_narrow; num_del_hc += arg.num_del_hc;
		num_bound_cons += arg.num_bound_cons; num_bound_cons_recur += arg.num_bound_cons_recur;
		num_del_bound_cons += arg.num_del_bound_cons;
		num_quadr += arg.num_quadr; num_del_quadr_delta_neg += arg.num_del_quadr_delta_neg;
		num_del_quadr_disjoint += arg.num_del_quadr_disjoint; num_bis_quadr += arg.num_bis_quadr;
		num_high_prec += arg.num_high_prec; num_del_high_prec += arg.num_del_high_prec;
		num_ver_high_prec += arg.num_ver_high_prec;
	};

	void show() const {
		cout << "num_f = " << num_f << endl;
		cout << "num_grad_f = " << num_grad_f << endl;
		cout << "num_hess_f = " << num_hess_f << endl;
		cout << "num_ineq = " << num_ineq << endl;
		cout << "num_grad_ineq = " << num_grad_ineq << endl;
		cout << "num_hess_ineq = " << num_hess_ineq << endl;
		cout << "num_del_ineq = " << num_del_ineq << endl;
		cout << "num_bisec = " << num_bisec << endl;
		cout << "num_bis_2 = " << num_bis_2 << endl;
		cout << "num_bis_3 = " << num_bis_3 << endl;
		cout << "num_precond = " << num_precond << endl;
		cout << "num_bis_newt = " << num_bis_newt << endl;
		cout << "num_del_newt = " << num_del_newt << endl;
		cout << "num_bc3 = " << num_bc3 << endl;
		cout << "num_bc3rev = " << num_bc3rev << endl;
		cout << "num_hc = " << num_hc << endl;
		cout << "num_hc_narrow = " << num_hc_narrow << endl;
		cout << "num_del_hc = " << num_del_hc << endl;
		cout << "num_bound_cons = " << num_bound_cons << endl;
		cout << "num_bound_cons_recur = " << num_bound_cons_recur << endl;
		cout << "num_del_bound_cons = " << num_del_bound_cons << endl;
		cout << "num_del_bc = " << num_del_bc << endl;
		cout << "num_quadr = " << num_quadr << endl;
		cout << "num_del_quadr_delta_neg = " << num_del_quadr_delta_neg << endl;
		cout << "num_del_quadr_disjoint = " << num_del_quadr_disjoint << endl;
		cout << "num_bis_quadr = " << num_bis_quadr << endl;
		cout << "num_high_prec = " << num_high_prec << endl;
		cout << "num_del_high_prec = " << num_del_high_prec << endl;
		cout << "num_ver_high_prec = " << num_ver_high_prec << endl;
	}
};

template <thread_privacy_t priv>
statistics<priv> operator+ (const statistics<priv> &arg1, const statistics<priv> &arg2) {
	statistics<priv> result;
	result = arg1;
	result.add(arg2);
	return result;
};

#endif

