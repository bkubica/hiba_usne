
#include "solver.hpp"
#include "problem_desc_impl.hpp"

#include <string>

//#define N 100
#define N 50
//#define N 31
//#define N 30
//#define N 8
//#define N 6
//#define N 4
//#define N 3
//#define N 2
//#define M 3
#define M N

// N - variables, M - equations

template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> eq1 (const adhc_vector<level, sparse_mode, n, T> &x) {
	adhc_ari<level, sparse_mode, n, T> result;
	result = x[1]*x[3] + x[2]*x[4];// + 1e-8;
	return result;
}

template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> eq2 (const adhc_vector<level, sparse_mode, n, T> &x) {
	adhc_ari<level, sparse_mode, n, T> result;
	result = x[2]*x[3] - x[1]*x[4];// + 1e-8;
	return result;
}

template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> eq3 (const adhc_vector<level, sparse_mode, n, T> &x) {
	adhc_ari<level, sparse_mode, n, T> result;
	result = sqr(x[1]) + sqr(x[2]) - sqr(x[3]) - sqr(x[4]);// + 1e-8;
	return result;
}


template<int level, SparsityLevel sparse_mode, int n, class T>
struct Franek {
	adhc_ari<level, sparse_mode, n, T> operator() (const adhc_vector<level, sparse_mode, n, T> &x, const int i) const {
		switch (i) {
			case 1: return eq1(x);
			case 2: return eq2(x);
			default: return eq3(x);
		}
	};
};


template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_vector<level, sparse_mode, n, T> f (const adhc_vector<level, sparse_mode, n, T> &x) {
	adhc_vector<level, sparse_mode, n, T> result;
	result[1] = power(x[1], 3) - 3.0*x[1]*sqr(x[2]);
	result[2] = 3.0*sqr(x[1])*x[2] - power(x[2], 3);
	result[3] = sqr(x[3]) - sqr(x[4]);
	result[4] = 2.0*x[3]*x[4];
	for (int ii = 5; ii <= n; ++ii) result[ii] = x[ii];
	return result;
}

template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> f (const adhc_vector<level, sparse_mode, n, T> &x, const int i) {
	adhc_ari<level, sparse_mode, n, T> result;
	switch(i) {
		case 1:	result = power(x[1], 3) - 3.0*x[1]*sqr(x[2]); break;
		case 2:	result = 3.0*sqr(x[1])*x[2] - power(x[2], 3); break;
		case 3: result = sqr(x[3]) - sqr(x[4]); break;
		case 4:	result = 2.0*x[3]*x[4]; break;
		default: result = x[i];//std::terminate();
	}
	return result;
}

template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_vector<level, sparse_mode, n, T> g (const adhc_vector<level, sparse_mode, n, T> &x) {
	//adhc_vector<level, sparse_mode, n, T> result;
	adhc_vector<level, sparse_mode, n, T> result_;
	adhc_vector<level, sparse_mode, n, T> &result = (level == -1)?(* new adhc_vector<level, sparse_mode, n, T> [n]):result_;
	result[1] = sqr(x[1]);// - sqr(x[2]) - sqr(x[3]) - sqr(x[4]);
	for (int ii = 2; ii <= n; ++ii) result[1] += (-sqr(x[ii]));
	for (int ii = 2; ii <= n; ++ii) result[ii] = 2.0*x[1]*x[ii];
	/*result[2] = 2.0*x[1]*x[2];
	result[3] = 2.0*x[1]*x[3];
	result[4] = 2.0*x[1]*x[4];*/
	return result;
}

template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> g (const adhc_vector<level, sparse_mode, n, T> &x, const int i) {
	//adhc_ari<level, sparse_mode, n, T> result;
	adhc_ari<level, sparse_mode, n, T> result_;
        adhc_ari<level, sparse_mode, n, T> &result = (level == -1)?(* new adhc_ari<level, sparse_mode, n, T>):result_;
	if (i == 1) {
		result = sqr(x[1]);
		for (int ii = 2; ii <= n; ++ii) result += (-sqr(x[ii]));
	}
	else {
		result = 2.0*x[1]*x[i];
	}
	/*switch(i) {
		case 1: result = sqr(x[1]) - sqr(x[2]) - sqr(x[3]) - sqr(x[4]); break;
		case 2: result = 2.0*x[1]*x[2]; break;
		case 3: result = 2.0*x[1]*x[3]; break;
		case 4: result = 2.0*x[1]*x[4]; break;
		default: std::terminate();
	}*/
	return result;
}

template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_vector<level, sparse_mode, n, T> h (const adhc_vector<level, sparse_mode, n, T> &x) {
	//adhc_vector<level, sparse_mode, n, T> result;
	adhc_vector<level, sparse_mode, n, T> result_;
	adhc_vector<level, sparse_mode, n, T> &result = (level == -1)?(* new adhc_vector<level, sparse_mode, n, T> [n]):result_;
	for (int i = 1; i <= n; ++i) {
		result[i] = cxsc::interval(0.0);
		for (int ii = 1; ii <= n; ++ii) if (ii != i) result[i] += x[ii];
	}
	/*result[1] = x[2] + x[3] + x[4];
	result[2] = x[1] + x[3] + x[4];
	result[3] = x[1] + x[2] + x[4];
	result[4] = x[1] + x[2] + x[3];*/
	return result;
}

template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> h (const adhc_vector<level, sparse_mode, n, T> &x, const int i) {
	/*cxsc::intmatrix A_int(4, 4);
	A_int = 1;
	A_int -= Id(A_int);*/
	//adhc_ari<level, sparse_mode, n, T> result;
	adhc_ari<level, sparse_mode, n, T> result_;
        adhc_ari<level, sparse_mode, n, T> &result = (level == -1)?(* new adhc_ari<level, sparse_mode, n, T>):result_;
	for (int ii = 1; ii <= n; ++ii) if (ii != i) result += x[ii];
	/*switch(i) {
		case 1: result = x[2] + x[3] + x[4]; break;
		case 2: result = x[1] + x[3] + x[4]; break;
		case 3: result = x[1] + x[2] + x[4]; break;
		case 4: result = x[1] + x[2] + x[3]; break;
		default: std::terminate();
	}*/
	//result = cxsc::interval(0.0);
	//for (int ii = 1; ii <= n; ++ii) if (ii != i) result += x[ii];
	//result = cxsc::imatrix(A_int)*x;
	return result;
}

template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> complex_power_2 (const adhc_vector<level, sparse_mode, n, T> &x, const int i) {
	adhc_ari<level, sparse_mode, n, T> result;
	switch(i) {
		case 1: result = sqr(x[1]) - sqr(x[2]); break;
		case 2: result = 2.0*x[1]*x[2]; break;
		default: std::terminate();
	}
	return result;
}

template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> complex_power_3 (const adhc_vector<level, sparse_mode, n, T> &x, const int i) {
	adhc_ari<level, sparse_mode, n, T> result;
	switch(i) {
		case 1: result = power(x[1], 3) - 3.0*x[1]*sqr(x[2]); break;
		case 2: result = 3.0*sqr(x[1])*x[2] - power(x[2], 3); break;
		default: std::terminate();
	}
	return result;
}

template<int level, SparsityLevel sparse_mode, int n, class T>
adhc_ari<level, sparse_mode, n, T> complex_power_4 (const adhc_vector<level, sparse_mode, n, T> &x, const int i) {
	adhc_ari<level, sparse_mode, n, T> result;
	switch(i) {
		case 1: result = power(x[1], 4) - 6.0*sqr(x[1])*sqr(x[2]) + power(x[2], 4); break;
		case 2: result = 4.0*(power(x[1], 3)*x[2] - x[1]*power(x[2], 3)); break;
		default: std::terminate();
	}
	return result;
}

template<int level, SparsityLevel sparse_mode, int n, class T>
struct Franek_new {
	adhc_ari<level, sparse_mode, n, T> operator() (const adhc_vector<level, sparse_mode, n, T> &x, const int i) const {
		//adhc_vector<level, sparse_mode, n, T> result = f(x);//h(g(f(x)));
		//adhc_vector<level, sparse_mode, n, T> result = g(f(x));
		//return result[i];
		adhc_ari<level, sparse_mode, n, T> result;
		//result = h(g(f(x)), i);
		//result = f(g(h(x)), i);
		//result = f(h(g(x)), i);
		//result = f(g(x), i);
		//result = h(f(g(x)), i);
		//result = h(x, i);
		//result = h(f(x), i);
		//result = g(f(x), i);
		result = g(x, i);
		//result = f(x, i);
		//result = complex_power_2(x, i);
		//result = complex_power_3(x, i);
		//result = complex_power_4(x, i);
		return result;
	};
};


//#define SPARSITY SparsityLevel::dense
//#define SPARSITY sparse
#define SPARSITY SparsityLevel::highly_sparse


int main() {
#include "num_threads.hpp"
	opdotprec = 1;
	ivector x(N);
	//x = interval (-1.0, 1.1);
	//----x = interval (-1.0, 1.0);
	//x = interval(-0.5, 0.5);
	x = interval (-0.5, 2.0);
	//x[1] = cxsc::interval(-1.0, 2.0); x[2] = cxsc::interval(3.0); x[3] = cxsc::interval(0.0, 1.0);
	//x = interval(-1.0, 2.0);
	real eps = 1e-7;
	eps = 1e-4;
	problem_desc_impl<SPARSITY, N, M, Franek_new> p(x);
	solver s (num_threads);
	cout << "x0 = " << x << endl;
	create_func creat = Herbort_create_pair_list;
	//create_func creat = Goulard_create_pair_list;
	//create_func creat = GE_create_pair_list;
	//create_func creat = Goulard_GE_create_pair_list;
	s.branch_and_bound (p, eps, creat);
//#define SHOW
#if defined SHOW
	cout << "Possible: " << endl;
	for (auto &x : s.sol) {
		x.show();
	}
	cout << endl << "Guaranteed: " << endl;
	for (auto &x : s.guar_sol) {
		x.show();
	}
	std::cout << "========\n";
#endif
	s.show_results();
	s.stat_show();
	return 0;
}

